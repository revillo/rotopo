var Backbone = require('backbone');
var _ = require('underscore');
var fs = require('fs');
var path = require('path');

var HIGH_INT = 99999999;

REWARDS = {
  'easy' : 25,
  'intermediate' : 50,
  'advanced' : 100,
  'insane' : 200,
}


var ContentManager = Backbone.Model.extend({
  setDirectory: function(directory) {
    this.directory = directory;
    this.cache = {};
    this.levels = {};
    this.refresh();
  },
  
  getAllItems: function() {
    return this.cache;
  },
  
  getLevelPack: function(name) {    
    if (this.cache.level_packs)
      return this.cache.level_packs[name];
  },
  
  prepareLevelHistoryFromRows: function(user_id, rows, purchases, shopManager) {
    var levels = {};
    var history = {};
    var missing = [];
    
    
    for (var r = 0; r < rows.length; r++) {
      var row = rows[r];
      history[row.level_id] = {completed: row.completed, retry_record: row.retry_record, time_record: row.time_record, turn_record: row.turn_record};
      
      var level = this.levels[row.level_id];
      levels[row.level_id] = level;
    }
    
    var packs = ["core"];
    var entries = [];
    
    for (var p in purchases) {
      var shopItem = shopManager.items[p];
      if (shopItem) {
        if (shopItem.contents.level_packs) {
          packs = _.union(shopItem.contents.level_packs, packs);
        } else if (shopItem.contents.level_ids) {
          for (var id = 0; id < shopItem.contents.level_ids.length; id++) {
          entries.push([user_id, shopItem.contents.level_ids[id], 0, HIGH_INT, HIGH_INT, HIGH_INT]);
          }
        }
      }
    }
        
    //Check if any levels are missing
    entries = this.prepareUserLevelPackEntries(user_id, packs, entries);
    for (var e = 0; e < entries.length; e++) {
      var level_id = entries[e][1];
      if (history[level_id] == undefined) {
        history[level_id] = {completed: 0, retry_record: HIGH_INT, time_record: HIGH_INT, turn_record: HIGH_INT};
        levels[level_id] = this.levels[level_id];
        missing.push(entries[e]);
      }
    }      
    
    return {levels: levels, history: history, missing: missing}
  },
  
  prepareLevelHistoryFromTrial: function(user_id, history) {
    var result = [];
    for (var level_id in history) {

      var h = history[level_id];
      if (!_.isObject(h)) continue;
      if (this.levels[level_id] && this.levels[level_id].directory == "core") {
        if (!(h.retry_record >= 0)) {
          h.retry_record = HIGH_INT;
        }
        if (!(h.time_record >= 0)) {
          h.time_record = HIGH_INT;
        }
        if (!(h.completed >= 0)) {
          h.completed = 0;
        }
        result.push([user_id, level_id, h.completed || 0, h.retry_record, h.time_record, h.turn_record]);
      }
    }
    
    return result;
  },
  
  prepareUserLevelPackEntries: function(user_id, packNames, previous) {
    var result = previous || [];
    for (var n = 0; n < packNames.length; n++) {
      var name = packNames[n];
      var pack = this.getLevelPack(name);
      for (var level in pack) {
        //uid, levelid, completed, retry_record, time_record, turn_record
        result.push([user_id, pack[level].level_id, 0, HIGH_INT, HIGH_INT, HIGH_INT]);
      }
    }
    return result;
  },
  
  getLevelCounts: function() {
    var result = {
      easy: 0,
      intermediate: 0,
      advanced: 0,
      insane: 0
    };
    
    for (var level in this.levels) {
      result[this.levels[level].difficulty]++;
    }
    
    return result;
  },
  
  loadFiles: function(files, directory, cacheObject) {
    var thiz = this
    for (var f = 0; f < files.length; f++) {
      var file = files[f];
      var filePath = path.resolve(directory, file);
      //If directory
      if (fs.lstatSync(filePath).isDirectory()) {
        cacheObject[file] = {};
        var newFiles = fs.readdirSync(filePath);
        thiz.loadFiles(newFiles, filePath, cacheObject[file]);
      } else {
        
        try {
          var data = fs.readFileSync(filePath);
          var parsed = JSON.parse(data);
          
          if (parsed.level_id) {
            if (this.newLevels[parsed.level_id]) {
              throw new Error("Two levels with the same ID:" + parsed.level_id + ": " + filePath + ", " + this.newLevels[parsed.level_id].name);
            }
            
            for (var p = 0; p < parsed.puzzles.length; p++) {
              var puzzle = parsed.puzzles[p];
              
              puzzle.reward = Math.min(p+1, 5) * REWARDS[parsed.difficulty];
              
              /*
              if (!puzzle.reward) {
                throw new Error("Puzzle without reward:" + parsed.level_id + ": " + p); 
              }*/
            }
            
            cacheObject[parsed.level_id] = parsed;
            this.newLevels[parsed.level_id] = parsed;
          } else {
            cacheObject[file] = parsed;
          }
        } catch(error) {
          console.log(error, filePath);
          throw new Error(error.message + " at " + file);
        }
        
      }
    }
  },
  
  refresh: function(res) {
    var newCache = {};
    var thiz = this;
    var dirPath = path.normalize(this.directory);
    
    try {
      var files = fs.readdirSync(dirPath);
      this.newLevels = {};
      this.loadFiles(files, dirPath, newCache);
      this.cache = newCache;
      this.levels = this.newLevels;
      return null;
    } catch (error) {
      console.log("Loading game content error: ", error);
      return error;
    }
    
  },
  
})

module.exports = ContentManager;
