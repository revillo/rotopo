var Backbone = require('backbone');
var _ = require('underscore');
var mysql = require('mysql');
var mysqldump = require('mysqldump');
var fs = require('fs');
var request = require('request');
var stripe; 
var braintree = require("braintree");
var cluster = require("cluster");

//https://developers.google.com/identity/gsi/web/guides/verify-google-id-token
const {OAuth2Client} = require('google-auth-library');

ACCOUNT_TYPE = {
  STANDARD : 0,
  FACEBOOK : 1
}

var USE_FIXTURES = false;

var SERVER_CONNECTED = false;

var Migrations = [
  { 
    version : 1,
    queries: [
      'CREATE SCHEMA gimble',
      'USE gimble',
      'CREATE TABLE auth_users (user_id int NOT NULL AUTO_INCREMENT, network VARCHAR(32) NOT NULL, network_id VARCHAR(50) NOT NULL, points int, email VARCHAR(80), PRIMARY KEY (user_id), UNIQUE KEY auth_uid (network, network_id), INDEX (email))',
      'CREATE TABLE level_history (user_id int NOT NULL, level_id int NOT NULL, completed int, UNIQUE KEY user_level (user_id, level_id), INDEX (user_id))',
      'CREATE TABLE purchases (user_id int NOT NULL, item_id int NOT NULL, status CHAR(16), INDEX (user_id), UNIQUE KEY user_item (user_id, item_id))',
      'CREATE TABLE info (version int)',
      'INSERT INTO info (version) VALUES (1)',
    ]
  },
  
  { 
    version : 2,
    queries: [
      'USE gimble',
      'ALTER TABLE purchases ADD transaction_date date',
      'ALTER TABLE purchases ADD transaction_id CHAR(16)',
      'UPDATE info SET version = 2 LIMIT 1',
    ]
  },
  
  { 
    version : 3,
    queries: [
      'USE gimble',
      'ALTER TABLE level_history ADD time_record int',
      'ALTER TABLE level_history ADD retry_record int',
      'UPDATE level_history SET time_record=99999999, retry_record=99999999',
      'UPDATE info SET version = 3 LIMIT 1',
    ]
  },
  
  {
    version : 4,
    queries: [
      'USE gimble',
      'ALTER TABLE auth_users ADD last_seen date',
      'UPDATE info set version = 4 LIMIT 1',
    ]
  },
  
  {
    version : 5,
    queries: [
      'USE gimble',
      'ALTER TABLE level_history ADD turn_record int',
      'UPDATE level_history SET turn_record=99999999',
      'UPDATE info set version = 5 LIMIT 1',
    ]
  },
]

var logError = function(error, important) {
  console.log(error);
}

var logPurchase = function(a,b,c,d,e) {
  console.log(a,b,c,d,e);
}

var UserManager = Backbone.Model.extend({
  initialize: function() {
    _.bindAll(this, 'checkVersion');
  },
  
  connect: function(APP_DATA, readyFunction) {
    var thiz = this;
    this.appData = APP_DATA;
    
    if (APP_DATA.APP_MODE == "placeholder") {
      readyFunction();
      return;
    }
    
    var a = this.appData;
    var db_config = {
      port: a.MYSQL_PORT,
      host : a.MYSQL_HOST,
      user : a.MYSQL_USER,
      password: a.MYSQL_PASSWORD
    }
    
    /*
    try {
      this.connection = mysql.createConnection(db_config);
      this.connection.connect(function(err) {
        if (err) {
          console.log(err);
          throw(err);
        }
      });
    } catch(err) {
      console.log("ERROR! No MYSQL CONNECTION", err);
      this.connection = undefined;
    }*/
    
    var firstConnection = true;
    
    var handleDisconnect = function() {
      if (thiz.shuttingDown) {
        return;
      }
      thiz.connection = mysql.createConnection(db_config);
      thiz.connection.connect(function(err) {
        if (err) {
          logError(err);
          setTimeout(handleDisconnect, 2000);
          return;
        }
        
        if (firstConnection) {
          firstConnection = false;
          thiz.checkDB(readyFunction);
        } else {
          thiz.connection.query('USE gimble', function(err) {
            if (err) {
              SERVER_CONNECTED = false;
            } else {
              SERVER_CONNECTED = true;
            }
          });
        }
      });
      
      thiz.connection.on('error', function(err) {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
          handleDisconnect();
        } else {
          throw err;
        }
      });
    }
    
    this.getFacebookAppToken(handleDisconnect);

    //stripe = require("stripe")(a.STRIPE_SECRET);
    
    this.braintreeGateway = braintree.connect({
      environment: a.BRAINTREE_ENVIRONMENT == "production" ? braintree.Environment.Production : braintree.Environment.Sandbox,
      merchantId: a.BRAINTREE_MERCHANT_ID,
      publicKey: a.BRAINTREE_PUBLIC_KEY,
      privateKey: a.BRAINTREE_PRIVATE_KEY
    });
  },
 
  
  setLogger: function(errorLog, importantLog, emailer, emailAddress, prefix) {  
    console.log(emailAddress);
    logPurchase = function(userInfo, item_ids, cash) {
       var mailOpts = {
        from: 'Rotopo',
        to: emailAddress,
        text: JSON.stringify(userInfo) + " - " + JSON.stringify(item_ids),
        subject: prefix + 'Rotopo Purchase - ' + cash
      }
      
      emailer.sendMail(mailOpts, function(error, info) {
        if (error) {
          console.log(error);
        }        
      });
    }
    
    logError = function(error, important, userInfo, data) {
      var log = errorLog;
      if (important)
        log = importantLog;
      
      data = data || {};
      
      console.log(error);
      
      var mailOpts = {
        from: 'Rotopo',
        to: emailAddress,
        text: JSON.stringify(userInfo) + "-" + JSON.stringify(data) + ":" + error,
        subject: prefix + (important ? 'Rotopo-Important' : 'Rotopo-Error')
      }
      
      emailer.sendMail(mailOpts, function(error, info) {
        if (error) {
          console.log(error);
        }        
      });
    
            
      if (_.isObject(error)) {
        log.error(error.message, " ", error.stack);
      } else {
        log.error(error);
      }
    }
  },
  
  applyMigration: function(migration, callback) {
    var thiz = this;
    var sendNextQuery = function(q) {
      if (!migration.queries[q]) {
        callback();
        return;
      }
      
      thiz.connection.query(migration.queries[q], function(err) {
        if (err) {
          console.log("MYSQL ERROR: ", err);
          throw err;
        } else {
          sendNextQuery(q+1);
        }
      });
      
    }
    sendNextQuery(0);
  },  
  
  migrateFrom: function(version, callback) {
    var vc = version;
    var thiz= this;
    
    var applyNextMigration = function(v) {
      if (!Migrations[v]) {
        SERVER_CONNECTED = true;
        callback();
        return;
      }
      
      thiz.applyMigration(Migrations[v], function() {
        applyNextMigration(v+1);
      })
    }
    applyNextMigration(vc); 
  },
  
  checkVersion: function(callback) {
    var thiz = this;
    
    this.connection.query('SELECT version FROM info LIMIT 1', function(err, rows, fields) {
      if (err) {
        console.log("MYSQL ERROR: ", err);
        throw err;
      }
      thiz.version = rows[0].version;   
      console.log("Mysql Found Version: ", thiz.version);
      if (thiz.version < Migrations.length) {
        console.log("Mysql migrating to: ", Migrations.length);
        thiz.migrateFrom(thiz.version, callback);
      } else {
        SERVER_CONNECTED = true;
        callback();
      } 
      
      /*else if (thiz.version == Migrations.length) {
        SERVER_CONNECTED = true;
        callback();
      } else {
        console.log("This code is too old for the database, quitting");
        callback('OUT_OF_DATE');
      }*/
      
    });
    
    
    /*
    this.connection.query("INSERT INTO Users (user_id, data) VALUES (?,?)", [0, JSON.stringify({a: "hello?", b: [1,2,'.']})], function(err) {
      if (err) throw err;
    });*/
  },
  
  checkDB: function(callback) {
    var thiz = this;
    this.connection.query('USE gimble', function(err) {
      if (err) {
        console.log("No schema found, creating database and tables");
        thiz.migrateFrom(0, callback);
      } else {
        thiz.checkVersion(callback);
      }
    });
  },
 
  getFacebookAppToken: function(callback) {
    var a = this.appData;
    var thiz = this;
    
    receiveFacebookAppToken = function(error, response, body) {
      if (error || !_.isString(body)) {
        console.log("Error: NO FACEBOOK APP ID!>>", error);
        logError(error);
        throw(error);
      }
            
      thiz.appData.FACEBOOK_APP_TOKEN = JSON.parse(body).access_token;
      console.log("Facebook token received");
      callback();
    }
  
    if (USE_FIXTURES) {
      receiveFacebookAppToken(null, null, "appToken=1010101");
      return;
    }
    request.get({
      url: 'https://graph.facebook.com/oauth/access_token', 
      qs:  {
          'client_id' : a.FACEBOOK_APP_ID,
          'client_secret' : a.FACEBOOK_APP_SECRET,
          'grant_type' : 'client_credentials'
        }
    }, receiveFacebookAppToken);
  },
  
  addLevelsToUser: function(user_id, entries, callback) {
    if (entries.length == 0) {
      callback();
      return;
    }
    
    this.connection.query("INSERT INTO level_history (user_id, level_id, completed, retry_record, time_record, turn_record) VALUES ? ON DUPLICATE KEY UPDATE level_id=level_id", [entries], function(error) {
      if (error) {
        logError(error);
        callback(error);
        return;
      }
      callback();
    });
  },
  
  purchaseItems: function(userInfo, item_ids, method, payed, opts, callback) {
    var thiz = this;
    
    this.getPurchases(userInfo.user_id, function(error, purchases) {
      if (error) {
        callback(error)
        return;
      }
      
      //TODO: If rejecting an item in cash mode, fail.
      
      
      
      item_ids = _.reject(item_ids, function(item_id) {
        return purchases[item_id];
      });
      
      if (item_ids.length == 0) {
        callback(null);
        return;
      }
      
      if (method == "CASH") {
        var cost = thiz.shopManager.getCost(item_ids, "CASH");
        if (cost < 100) {
          cost += 30;
        }
        
        if (cost != payed) {
          callback({error: "PRICE_ERROR", message: "A price mismatch was detected and the purchase was canceled. You have not been charged."});
          return;
        }
        
        /*
        if(!stripe) {
          callback("STRIPE_ERROR");
          return;
        }*/
        
        if (!thiz.braintreeGateway) {
          callback("BRAINTREE_ERROR");
          return;
        }
        
        var idPairs = thiz.shopManager.prepareUserItemEntries(userInfo.user_id, item_ids, "INITIATED");
        
        if (idPairs.length == 0) {
          callback(null);
          return;
        }
        
        thiz.connection.query("INSERT INTO purchases (user_id, item_id, status) VALUES ?", [idPairs], function(error) {
          if (error) {
            logError(error);
            callback(error);
            return;
          }
          
          //Checkout with braintree
          var handleBraintreeResponse = function(btError, response) {
            if (btError || (response && !response.success)) {
              thiz.connection.query("DELETE FROM purchases WHERE (user_id, item_id, status) IN ?", [[idPairs]], function(error) {
                if (error) {
                  console.log(error);
                  callback({error: "SERVER_ERROR", message: "There was a server error during this transaction. Your payment was not processed."});
                  return;
                }
                
                var text = "No Information.";
                if (response && response.transaction && response.transaction.processorResponseText) {
                  text = response.transaction.processorResponseText;
                }
                
                callback({error: "PAYMENT_FAILED", message: "There was an error processing your payment. You have not been charged. Processor: " + text});

                console.log("bterror", btError, response);
              })
              return;
            }            
            
            //Approved
            var transactionID = "";
            if (response.transaction && response.transaction.id) {
              transactionID = response.transaction.id;
            } else {
              console.log("no bt id: ", response.transaction);
            }
            
            var date = new Date();
            
            thiz.connection.query("UPDATE purchases SET status='CASH', transaction_date=?, transaction_id=? WHERE (user_id, item_id, status) IN ?", [date, transactionID, [idPairs]], function(error) {
              if (error) {                
                logError(error, true, userInfo, item_ids);
                callback({error: "SERVER_ERROR", message: "Your payment was submitted and the items were purchased. However, the server experienced an error. We have flagged this transaction for review. Sorry for any inconvenience."});
                return;
              }
              
              
              var handleLevelPass = false;
              for (var i = 0; i < item_ids.length; i++) {
                if (item_ids[i] == "999") {
                  handleLevelPass = true;
                }
              }

              // PROCESS LEVEL PASS
              if (handleLevelPass) {
                var handleError = function(error) {
                  logError(error, true, userInfo, item_ids);
                  callback({error: "SERVER_ERROR", message: "Your payment was submitted and the items were purchased. However, the server experienced an error. We have flagged this transaction for review. Sorry for any inconvenience."});
                }
                
                thiz.getUserData(userInfo, ['points'], function(err, row) {
                  if (err) {
                    handleError(err);
                    return;
                  }
                  
                  var points = row.points;
                  var pointRefundTotal = 0;
                  
                  for (var p in purchases) {
                    var item = thiz.shopManager.items[p];
                    if (item.contents && item.contents.level_ids) {
                      pointRefundTotal += item.point_price;
                    }
                  }
                  
                  if (pointRefundTotal > 0) {
                    
                    thiz.writeUserData(userInfo, {points: points + pointRefundTotal}, function(error) {
                      if (err) {
                        handleError(err);
                        return;
                      }
                      callback(null);
                      logPurchase(userInfo, item_ids, "$" + (cost / 100));         
                    });
                  } else {
                    callback(null);
                    logPurchase(userInfo, item_ids, "$" + (cost / 100));         
                  }                  
                });
                
                //END PROCESS LEVEL PASS
                
              } else {
                callback(null);              
                logPurchase(userInfo, item_ids, "$" + (cost / 100));         
              }
            });
          }
          
      
          
          var dollarAmount = cost / 100;
          var item_idsString = "" + item_ids;
          
          thiz.braintreeGateway.transaction.sale({
            amount: dollarAmount.toFixed(2),
            paymentMethodNonce: opts.nonce,
            orderId : userInfo.user_id + "#" + userInfo.network + ":" + userInfo.network_id,
            customer : {
              id : userInfo.user_id,
            },
            
            descriptor: {
              url: "rotopo.com"
            },
            
            options: {
              submitForSettlement: true
            }, 
            
            customFields: {
              user_id : userInfo.user_id,
              email: opts.details ? opts.details.email : "", 
              item_ids: item_idsString.length > 250 ? "Too Long" : item_idsString,
              network: userInfo.network,
              network_id: userInfo.network_id
            }
          }, handleBraintreeResponse);
          
        });
      } else { //POINTS
        thiz.getUserData(userInfo, ['points'], function(error, row) {
          if (error) {
            callback(error);
            return;
          }
          var points = row.points;

          var cost = thiz.shopManager.getCost(item_ids, "POINTS");
          
          if (cost != null && points > cost) {
            points -= cost;
          } else {
            callback("INSUFFICIENT_FUNDS");
            return;
          }
          
          var idPairs = thiz.shopManager.prepareUserItemEntries(userInfo.user_id, item_ids, "POINTS");
          
          if (idPairs.length == 0) {
            callback(null);
            return;
          }
        
          thiz.connection.query("INSERT INTO purchases (user_id, item_id, status) VALUES ?", [idPairs], function(error) {
            if (error) {
              logError(error);
              callback({error: "SERVER_ERROR"});
              return;
            }
            
            thiz.writeUserData(userInfo, {points: points}, function(error) {
              if (error) {
                callback(error);
                return;
              }
              callback(null);
            });
          });
          
          //Done with purchase
        });
      } //POINTS
    });
  },
  
  createUserEntry: function(credentials, data, callback) {
    var credentials = _.pick(credentials, 'network', 'network_id');
    var fields = _.clone(credentials);
    fields.points = 0;
    
    var points = Number(data.points);
    
    if (points > 0) {
      fields.points = Math.min(points, 10000);
    } else {
      points = 0;
    }

    var thiz = this;
    callback = callback || function(){};
    this.connection.query("INSERT INTO auth_users SET ?", fields, function(err, rows, fields) {
      if(err) {
        logError(err);
        callback(err);
        return;
      }
      
      thiz.getUserData(credentials, ['user_id'], function(error, row){
        if (error) {
          logError(err);
          callback(error);
        } else {
          //Add levelHistory from trial
          var user_id = row.user_id;
          if (!_.isEmpty(data.levelHistory)) {
            var entries = thiz.contentManager.prepareLevelHistoryFromTrial(user_id, data.levelHistory);
            thiz.addLevelsToUser(user_id, entries, function(err) {
              if (err) {
                logError(err);
                callback(err);
              } else {
                callback(null, user_id);
              }
            });
          } else {
            callback(null, user_id);
          }
        }
      }); 
    }); 
  },
  
  writeUserData: function(credentials, data, callback) {
    callback = callback || function(){};
    var thiz = this;
    
    var handler = function(err) {
      if(err) {
        logError(err);
        callback(err);
      } else {
        callback(null);
      }
    }
    
    if (credentials.user_id) {
      this.connection.query("UPDATE auth_users SET ? WHERE user_id=?", [data, credentials.user_id], handler);
    } else {
      this.connection.query("UPDATE auth_users SET ? WHERE network_id=? AND network=?", [data, credentials.network_id, credentials.network], handler);
    }
  },
  
  getUserData: function(credentials, columns, callback) {
    callback = callback || function(){};
    
    var handler = function(err, rows, fields) {
      if(err) {
        logError(err);
        callback(err);
        return;
      }
      if (rows.length > 0)
        callback(null, rows[0]);
      else
        callback('NO_USER');
    };
    
    //TODO
    if (credentials.user_id) {
      this.connection.query("SELECT ?? FROM auth_users WHERE user_id = ? LIMIT 1", [columns, credentials.user_id], handler);
    } else {
      this.connection.query("SELECT ?? FROM auth_users WHERE network_id=? AND network=?", [columns, credentials.network_id, credentials.network], handler);
    }
  },
  
  getPurchases: function(user_id, callback) {
    this.connection.query("SELECT * FROM purchases WHERE user_id = ?", [user_id], function(err, rows, fields) {
      if (err) {
        logError(err);
        callback(err);
        return;
      }
      
      var result = {};
      
      for (var r = 0; r < rows.length; r++) {
        result[rows[r].item_id] = 1;
      }
      
      callback(null, result);
    });
  },
  
  getLevelHistory: function(user_id, callback) {
    this.connection.query("SELECT * FROM level_history WHERE user_id = ?", [user_id], function(err, rows, fields) {
      if (err) {
        logError(err);
        callback(err);
        return;
      }
      callback(null, rows);
    });
  },
  
  getAccountContent: function(user_id, callback) {
    var thiz = this;
    var result = {};
    var userInfo = {user_id: user_id};
    
    this.getUserData({user_id:user_id}, ['network', 'network_id', 'points', 'last_seen'], function(error, row) {
      if (error) {
        callback(error);
        return;
      }
      
      result = {
        points: row.points, 
        network: row.network, 
        network_id: row.network_id
      };
      
      
      var lastSeen = row.last_seen;
      
      thiz.getLevelHistory(user_id, function(err, history) {
        if (err) {
          callback(err);
          return;
        }
                        
        thiz.getPurchases(user_id, function(err, purchases) {
          if (err) {
            callback(err);
            return;
          }
          
          var levelData = thiz.contentManager.prepareLevelHistoryFromRows(user_id, history, purchases, thiz.shopManager);
          result.levels = levelData.levels;
          result.levelHistory = levelData.history;
          result.purchases = purchases;
          purchaseData = thiz.shopManager.prepareContentFromRows(purchases, thiz.contentManager);
          result.avatars = purchaseData.avatars;
          result.other = {};
          result.user_id = user_id;
          
          var date = new Date();
          var dayDiff = 0;
          
          if (lastSeen) {
            dayDiff = Math.floor((date - lastSeen) / (24 * 60 * 60 * 1000));
            
            if (dayDiff > 0) {
              result.other.login_bonus = dayDiff;
            }
          }
          
          if (!lastSeen || dayDiff > 0) {
            result.other.login_days = 1;
            thiz.writeUserData(userInfo, {last_seen: date}, function() {
              
            });
          }
          
          if (levelData.missing.length > 0) {
            thiz.addLevelsToUser(user_id, levelData.missing, function(error) {
              if (error) {
                callback(error);
                return;
              }
              callback(null, result);
            });
          } else {
            callback(null, result);
          }
        });
      });
      

      
    });
    
    
  },
  
  saveUserData: function(credentials, data, callback) {
    var thiz = this;
    
    this.getUserData(credentials, ['user_id', 'points'], function(error, row) {
      
      if (error) {
        logError(error);
        callback("Database error S0");
        return;
      }
      
      var user_id = row.user_id;
      var points = row.points;
      
      if (data.level_id) {
        data.puzzle_index = Number(data.puzzle_index);
        var multiplier = Number(data.multiplier);
        var level_id = Number(data.level_id);
        var level = thiz.contentManager.levels[level_id];
        
        if (!level || (!multiplier || multiplier > 8.0 || multiplier < 0.25)) {
          callback('User submitted invalid data');
          return;
        }
        
        
        
        var puzzle = level.puzzles[data.puzzle_index];
        
        //If last puzzle is beaten, add time and death records. TODO: check bounds
        if(data.time_record) {
          data.time_record = Number(data.time_record);
        }
        
        if (data.retry_record != undefined) {
          data.retry_record = Number(data.retry_record);
        }
        
        if (data.turn_record != undefined) {
          data.turn_record = Number(data.turn_record);
        }
        
        var lastSave = {level_id: data.level_id, puzzle_index: data.puzzle_index, winloss: data.winloss, time: Date.now()};
        
        if (data.puzzle_index < level.puzzles.length-1) {
          lastSave.time_record = data.time_record;
          lastSave.retry_record = data.retry_record;
          lastSave.turn_record = data.turn_record;
          //Only save these when last puzzle is finished
          delete data.time_record;
          delete data.retry_record;
          delete data.turn_record;
        }
      
        
        if(puzzle) {
          var reward = Math.floor((puzzle.reward || 0) * multiplier);
          points += reward;
          
         thiz.saveUserPuzzleCompleted(user_id, data, function(err) {
            if (err) {
              logError(err);
              callback("Database error S2");
              return;
            }
            
            thiz.connection.query("UPDATE auth_users SET points = ? WHERE user_id = ?", [points, user_id], function(err) {
              if(err) {
                logError(err);
                callback("Database error S1");
                return;
              }
                      
              callback(null, points, lastSave);
            });
          });
        } else {
          callback("User submitted invalid data");
        }
      } else {
        callback("User submitted invalid data");
      }
    });
  },

  saveUserPuzzleCompleted: function(user_id, data, callback) {
    var dbResponse = function(err, info) {
      if (err) {
        logError(err);
        callback(err);
        return;
      }
      
      if (info.affectedRows < 1) {
        callback("puzzle not owned");
      } else {
        callback();
      }
    };
    var thiz = this;
    this.connection.query("SELECT * FROM level_history WHERE user_id = ? AND level_ID = ?", [user_id, data.level_id], function(error, rows) {
      
      if (error || rows.length == 0) {
        callback("puzzle not owned");
        return;
      }
      
      
      
      if (data.retry_record != undefined) {
        if(rows[0].completed && data.puzzle_index >= rows[0].completed) {
          thiz.connection.query("UPDATE level_history SET completed = ?, retry_record = ?, time_record = ?, turn_record = ? WHERE user_id = ? AND level_id = ?", [data.puzzle_index+1, data.retry_record, data.time_record, data.turn_record, user_id, data.level_id], dbResponse);
        } else {  
          thiz.connection.query("UPDATE level_history SET completed = GREATEST(?, completed), retry_record = LEAST(?, retry_record), time_record = LEAST(?, time_record), turn_record = LEAST(?, turn_record) WHERE user_id = ? AND level_id = ?", [data.puzzle_index+1, data.retry_record, data.time_record, data.turn_record, user_id, data.level_id], dbResponse);
        }
      } else {
        thiz.connection.query("UPDATE level_history SET completed = GREATEST(?, completed) WHERE user_id = ? AND level_id = ?", [data.puzzle_index+1, user_id, data.level_id], dbResponse);
      }
    
    });
    
        

  },
  
  /*
  checkGoogleCredentialsAndUpdateCookie: function(credentials, data, res) {
  
    
  },
  
  checkFacebookCredentialsAndUpdateCookie: function(credentials, data, res) {
    var thiz = this;
    var updateCookieFromCredentials = function(error, response, body) {
      if (error || !body) {
        res.send({success: 0, error: (error == "INVALID_CREDENTIALS" ? error: "FACEBOOK_ERROR"), message: "Failed to log in to Facebook"});
        return;
      }
      
      var network_id;
      try {
        var fbResponse = JSON.parse(body);
        network_id = fbResponse.data.user_id;
      } catch (e) {
        logError(e);
        res.send({success: 0, error: "FACEBOOK_ERROR", message: "Failed to log in to Facebook"});
        return;
      }
      
      if (!network_id) {
        res.send({success: 0, error: "FACEBOOK_ERROR", message: "Failed to log in to Facebook"});
        return;
      }
      
      var networkInfo = {};
      
      
    }
    
    if (USE_FIXTURES) 
      updateCookieFromCredentials(null, null, '{"data": {"user_id": "11111"}}');
    else
      this.checkCredentials(credentials, updateCookieFromCredentials);
  },*/
    
  checkCredentialsAndUpdateCookie: function(credentials, data, res) {
    var returnInvalid = function() {
      res.send({success: 0, error: "INVALID_CREDENTIALS", message: "Failed to login"});
    }
    
    var thiz = this;
    
    var networkInfo = {};
    
    var findOrCreateUser = function() {
      var network_id = networkInfo.network_id;
      var network = networkInfo.network;
      
      res.cookie('network', network, {signed: true});
      res.cookie('network_id', network_id, {signed: true});
      
      //Lookup network info in users
      thiz.connection.query('SELECT user_id FROM auth_users WHERE network_id=? AND network=? LIMIT 1', 
        [network_id, network], function(err, rows, fields) {
          if (err) {
            logError(err);
            res.send({success: 0, error: "DATABASE_ERROR", message: "Could not access game server. Please try again later."});
            return;
          }
          
          //If user is found, update cookie with user_id
          if (rows.length > 0) {
            var user_id = rows[0].user_id;
            res.cookie('user_id', user_id, {signed:true});
            thiz.getAccountContent(user_id, function(err, content) {
              if (err) {
                res.send({success: 0, error: "DATABASE_ERROR",  message: "Could not access game server. Please try again later."});
                return;
              }
              
              res.send({success: 1, content: content});
            });
          //Otherwise create a new user
          } else {
            thiz.createUserEntry(networkInfo, data, function(err, user_id) {
              if (err) {
                res.send({success: 0, error: "DATABASE_ERROR", message: "Could not access game server. Please try again later."});
                return;
              }
              res.cookie('user_id', user_id, {signed: true});
              
              
              thiz.getAccountContent(user_id, function(err, content) {
                if (err) {
                  res.send({success: 0, error: "DATABASE_ERROR",  message: "Could not access game server. Please try again later."});
                  return;
                }
                
                res.send({success: 1, content: content});
              });
            });
          }
      });
    };
    
    /*
    var handleGoogleResponse = function(error, response, body) {
      if (body) {
        try {
          body = JSON.parse(body);
        } catch (error) {
          logError(error);
          body = {error: 1}
        }
      }
      
      if (error || body && body.error) {
        returnInvalid();
        return;
      }
      
      if (!body.sub) {
        returnInvalid();
        return;
      }
      
      networkInfo.network = 'google';
      networkInfo.network_id = body.sub;
   
      findOrCreateUser();
    };*/
    
    handleFacebookResponse = function(error, response, body) {
      if (error || !body) {
        returnInvalid();
        return;
      }
      
      var network_id;
            
      try {
        var fbResponse = JSON.parse(body);
        network_id = fbResponse.data.user_id;
      } catch (e) {
        logError(e);
        returnInvalid();
        return;
      }
      
      if (!network_id) {
        returnInvalid();
        return;
      }
            
      networkInfo.network_id = network_id;
      networkInfo.network = 'facebook';
      findOrCreateUser();
    };

    if (credentials.network == 'facebook' && _.isObject(credentials.network_data) && credentials.network_data.accessToken) {
      request.get({
        url: 'https://graph.facebook.com/debug_token', 
        qs:  {
          'input_token' : credentials.network_data.accessToken,
          'access_token' : this.appData.FACEBOOK_APP_TOKEN,
        }
      }, handleFacebookResponse);
    } else if (credentials.network == "google" && _.isObject(credentials.network_data) && credentials.network_data.accessToken) {
     
      let thiz = this;
      let idToken = credentials.network_data.accessToken;
      const client = new OAuth2Client(thiz.appData.GOOGLE_CLIENT_ID);
      async function verify() {
        const ticket = await client.verifyIdToken({
            idToken: idToken,
            audience: thiz.appData.GOOGLE_CLIENT_ID
        });
        const payload = ticket.getPayload();
        networkInfo.network = 'google';
        networkInfo.network_id = payload['sub'];
        findOrCreateUser();
      }
      verify().catch(function(err) {
        returnInvalid();
      });

      /*
      request.get({
        url: 'https://www.googleapis.com/oauth2/v3/userinfo', 
        qs:  {
          'access_token' : credentials.network_data.accessToken,
        }
      }, handleGoogleResponse);
      */
    } else {
      returnInvalid();
    }
  },
  
  disconnect: function() {
    this.shuttingDown = true;
    this.connection.end();
  },
  
  getDump: function(response) {
    var file = '../rotopo_backup/data.sql';
    if (fs.existsSync(file)) {
      var stream = fs.createReadStream(file, {bufferSize: 64 * 1024});
      response.setHeader('Content-disposition', 'attachment; filename=rotopo_dump.sql');
      stream.pipe(response);
    } else {
      response.send({success: 0, message: "no file"});
    }
  },
  
  dumpDatabase: function(callback) {
    
    var dir =  '../rotopo_backup/'
    
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }
    
    mysqldump({
      host: this.appData.MYSQL_HOST,
      user: this.appData.MYSQL_USER,
      password: this.appData.MYSQL_PASSWORD,
      port: this.appData.MYSQL_PORT,
      database: 'gimble',
      dest: dir + 'data.sql' 
    }, function(err) {
      callback(err);
    });
  },  
  
});



module.exports = UserManager;
