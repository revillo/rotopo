var Backbone = require('backbone');
var _ = require('underscore');
var mysql = require('mysql');
var request = require('request');
var fs = require('fs');
var path = require('path');

var stripe; 

var POINT_PRICES = {
  'easy' : 200,
  'intermediate' : 400,
  'advanced' : 800,
  'insane' : 1600
}

var CASH_PRICES = {
  'easy' : 2,
  'intermediate' : 4,
  'advanced' : 6,
  'insane': 8
}


var ShopManager = Backbone.Model.extend({
  setDirectory: function(directory) {
    this.directory = directory;
    this.cache = {};
    this.items = {};
    this.refresh();
  },
  
  getAllItems: function() {
    return this.cache;
  },
  
  getCost: function(item_ids, method) {
    var sum = 0;
    for (var i = 0; i < item_ids.length; i++) {
      var item_id = item_ids[i];
      var item = this.items[item_id];
      if (!item) return null;
      
      if (method == "POINTS") {
        if (!item.point_price) return null;
        sum += item.point_price;
      } else {
        if (!item.cash_price) return null;
        sum += item.cash_price;
      }
    }
    
    return sum;
  },
  
  prepareContentFromRows: function(purchases, contentManager) {
    var avatars = {};
    for (var p in purchases) {
      if (!this.items[p]) continue;
      var avs = this.items[p].contents.avatars;
      if (avs) {
        for (var av = 0; av < avs.length; av++) {
          var avatar = contentManager.cache.avatars[avs[av]];
          avatars[avatar.name] = avatar;
        }
      }
    }
    
    return {avatars: avatars}
  },
  
  prepareUserItemEntries: function(user_id, item_ids, status) {
    var result = [];
    for (var i = 0; i < item_ids.length; i++) {
      var item_id = item_ids[i];
      if (this.items[item_id]) {
        if(status)
          result.push([user_id, item_id, status]);
        else
          result.push([user_id, item_id]);
      }
    }
    return result;
  },
  
  setConnection: function(connection) {
    
  },
  
  loadFiles: function(files, directory, cacheObject) {
    var thiz = this
    for (var f = 0; f < files.length; f++) {
      var file = files[f];
      var filePath = path.resolve(directory, file);
      //If directory
      if (fs.lstatSync(filePath).isDirectory()) {
        cacheObject[file] = {};
        var newFiles = fs.readdirSync(filePath);
        thiz.loadFiles(newFiles, filePath, cacheObject[file]);
      } else {
        var data = fs.readFileSync(filePath);
        var parsed;
        
        try { 
         parsed = JSON.parse(data);
        } catch(error) {
          throw new Error("Error in " + filePath + ":" + error);
        }
       
         if (parsed.disabled) continue;
       
        cacheObject[file] = parsed;//JSON.stringify(parsed);
        
        if (parsed.item_id) {
          if (this.newItems[parsed.item_id]) {
            throw new Error("Two levels with the same ID:" + parsed.item_id);
          } else {
            this.newItems[parsed.item_id] = cacheObject[file];
          }
        }
        
        if (parsed.contents && parsed.contents.level_ids) {
          var level_ids = parsed.contents.level_ids;
          var numPuzzles = 0;
          var point_price = 0;
          var cash_price = 0;
          
          for (var l = 0; l < level_ids.length; l++) {
            var level = this.contentManager.levels[level_ids[l]];
            numPuzzles += level.puzzles.length;
            point_price += level.puzzles.length * POINT_PRICES[parsed.difficulty];
            cash_price += level.puzzles.length * CASH_PRICES[parsed.difficulty];
          }
          parsed.numPuzzles = numPuzzles;
          parsed.point_price = point_price;
          parsed.cash_price = cash_price;
          
          if (level_ids.length == 1) {
            if (parsed.name != this.contentManager.levels[level_ids[0]].name){
              throw new Error("Inconsistent naming: " + parsed.name);
            }
          }
          
          if (parsed.premium) {
            parsed.cash_price = 35;
            parsed.point_price *= 5.0;
          } else {
            parsed.cash_price = 0;
          }
        } 
        
        if (parsed.contents && parsed.contents.avatars) {
          if (!parsed.cash_price)
            parsed.cash_price = parsed.point_price * 0.005;
        }
        
        if (parsed.contents && parsed.contents.level_packs) {
          var numPuzzles = 0;
          
          for (var p = 0; p < parsed.contents.level_packs.length; p++) {
            var packName = parsed.contents.level_packs[p];
            var levels = this.contentManager.getLevelPack(packName);
            for (var l in levels) {
              numPuzzles += levels[l].puzzles.length;
            }
          }
          parsed.numPuzzles = numPuzzles;
        }
        

      }
    }
  },
  
  refresh: function(res) {
    var newCache = {};
    var thiz = this;
    var dirPath = path.normalize(this.directory);
    
    try {
      var files = fs.readdirSync(dirPath);
      this.newItems = {};
      this.loadFiles(files, dirPath, newCache);
      this.items = this.newItems;
      this.cache = newCache;
      return null;
    } catch (error) {
      console.log("Loading shop item error: ", error);
      return error;
    }
    
  },
  
})

module.exports = ShopManager;
