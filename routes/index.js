var express = require('express');
var router = express.Router();
var _ = require('underscore');
var UserManager = require('../backend/user.js');
var ShopManager = require('../backend/shop.js');
var ContentManager = require('../backend/content.js');
var nodemailer = require ('nodemailer');
var minifier = require('minifier');

var fs = require('fs');

/* GET home page. */

var APP_INFO;

var TITLE = 'roTopo';


var API_STATUS = "online";

try {
  var text = fs.readFileSync('config.json');
  APP_INFO = JSON.parse(text);
} catch(error) {
  console.log(error, "NO CONFIG FILE. App will not run properly!");
}

if (APP_INFO.APP_MODE == "placeholder") {
  router.APP_INFO = APP_INFO;
  router.get('/', function(req, res) {
    res.render('placeholder', {title: TITLE});
  });    
} else {

var userManager = new UserManager();
var shopManager = new ShopManager();
var contentManager = new ContentManager();
contentManager.setDirectory("game_content");

if (APP_INFO) {
  userManager.contentManager = contentManager;
  userManager.shopManager = shopManager;
} else {
  APP_INFO = {};
}

router.APP_INFO = APP_INFO;
router.userManager = userManager;
router.shopManager = router.shopManager;

var emailer = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: APP_INFO.EMAIL,
    pass: APP_INFO.EMAIL_PASSWORD
  }
});

router.emailer = emailer;



var getAccountInfo = function(cookie) {
  if (!cookie) {
    return {status: "none"};
  } else if (cookie && 
    _.isString(cookie.network_id) && 
    cookie.network_id.length > 0 && 
    cookie.user_id !== undefined &&
    _.isString(cookie.network)) {
    
    var credentials = {
      network : cookie.network,
      network_id : cookie.network_id,
      status: 'oauth'
    }
    
    if (cookie.last_save) {
      credentials.lastSave = cookie.last_save;
    }
    
    /*
    if (cookie.settings)
      credentials.settings = cookie.settings;
    
    if (cookie.avatar)
      credentials.avatar = cookie.avatar;
    */ 
     
    if (cookie.user_id !== undefined) {
      var id = Number(cookie.user_id);
      if (!isNaN(id) && id >= 1) {
        credentials.user_id = id;
        credentials.status = 'user';
      }
    }
    
    if (credentials.user_id)
      return credentials;
  }
  
  return {status: "none"}
}


userManager.connect(APP_INFO, function(error) {

if (error) {
  userManager.logError(error);
  return;
}

shopManager.setConnection(userManager.connection);
shopManager.contentManager = contentManager;
shopManager.setDirectory("shop_items");


minifier.on('error', function(error) {
  console.log("minifier", error);
})


var minifyJS = function() {
  var jsdir = __dirname + "/../public/javascripts/";

  /*
  minifier.minify([
      jsdir + "libs/jquery-1.9.0.min.js",
      jsdir + "libs/underscore-min.js",
      jsdir + "libs/backbone-min.js",
      jsdir + "libs/handlebars.js", 
      jsdir + "libs/jquery.easing.1.3.js", 
      jsdir + "libs/jquery.mousewheel.js",
      jsdir + "libs/three/three.r74.min.js",
      jsdir + "libs/three/stats.min.js",
      jsdir + "libs/three/ColladaLoader2.js",    
    ], {
      output: __dirname + "/../public/javascripts/libs.js",
      clean: true,
  });
  */
  
  minifier.minify([
    jsdir + "game/utils.js",
    jsdir + "game/game.js",
    jsdir + "game/gfx.js",
    jsdir + "player/overlay.js", 
    jsdir + "player/camera.js", 
    jsdir + "player/puzzle.js", 
    jsdir + "player/avatar-effects.js", 
    jsdir + "player/visuals.js"], {
      output: __dirname + "/../public/javascripts/player.js",
      clean: true,
  });
}
//minifyJS();

router.get('/placeholder', function(req, res) {
  res.render('placeholder', {title: TITLE});
});   
 
var BASIC_CONTENT = {
  points : 0,
  levels : contentManager.getLevelPack('core'),
  levelHistory : {},
  purchases : {},
  avatars: {},
  signedIn: 0
} 

var NETWORK_KEYS = JSON.stringify({
  facebookAppID: APP_INFO.FACEBOOK_APP_ID,
  googleClientID: APP_INFO.GOOGLE_CLIENT_ID,
  googleAppID: APP_INFO.GOOGLE_APP_ID
});

var GAME_INFO = JSON.stringify({
  numAvatars : _.size(shopManager.cache.avatars),
  numLevels : _.size(contentManager.levels),
  levelCounts : contentManager.getLevelCounts()
});

var BASIC_STRING = JSON.stringify(BASIC_CONTENT);

var mainPageTemplate;
var mainPageBasic;
 
router.get('/early-access', function(req, res) {
  res.redirect('/');
});
 
router.get('/', function(req, res) {
  if (API_STATUS == 'disabled') {
    res.send("The rotopo server is going to bed for a while. Please check again later.");
    return;
  }
  
  var accountInfo = getAccountInfo(req.signedCookies);
  var content = BASIC_CONTENT;
      
  var renderMainPage = function() {
    if (accountInfo.user_id) {
     res.render('index', { 
        title: TITLE, 
        mode: APP_INFO.APP_MODE, 
        //content: JSON.stringify(content), 
        content: BASIC_STRING, 
        networkKeys: NETWORK_KEYS,
        gameInfo: GAME_INFO,
        loggedIn: true
      });
    } else {
      if (!mainPageBasic) {
        //mainPageBasic = mainPageTemplate.replace("##contentreplace##", JSON.stringify(BASIC_CONTENT));
      }
      
      //res.send(mainPageBasic);
      res.render('index', { 
        title: TITLE, 
        mode: APP_INFO.APP_MODE,
        content: BASIC_STRING, 
        networkKeys: NETWORK_KEYS,
        gameInfo: GAME_INFO,
        loggedIn: false
      });
    }
  }
  
  /*
  mainPageTemplate = null;
  if (!mainPageTemplate) {
    req.app.render('index', { title: TITLE, mode: APP_INFO.APP_MODE, facebookAppID: APP_INFO.FACEBOOK_APP_ID, content: "##contentreplace##"}, function(err, html) {
      console.log("rendering ", Date.now() - tick);
      mainPageTemplate = html;
      renderMainPage();
      
    });
  } else  {
    renderMainPage();
  }
  */
  
  renderMainPage();
});

if (APP_INFO.APP_MODE == "develop") {
  router.get('/editor', function(req, res) {
    res.render('editor', { title: TITLE + " editor"});
  });
}

router.get('/edit-logout', function(req, res) {
  res.clearCookie('admin');
  res.redirect('/admin87');
});

router.post('/edit-verify', function(req, res) {
  var password = req.param("pass");
  if (password == APP_INFO.EDIT_PASSWORD) {
    res.cookie('admin', 'authorized', {signed: true});
  } 
  res.redirect('/admin87');
});

router.get('/admin87', function(req, res) {
  if (req.signedCookies && req.signedCookies.admin == 'authorized') {
    res.render('admin', {
      title: 'Epigon Admin Panel', 
    });
  } else {
    res.send('Password: <form method = "POST" action = "edit-verify"><input type="text" name="pass"><input type = "submit"></form>');
  }
});

router.get('/change/status', function(req, res) {
  if (req.signedCookies && req.signedCookies.admin == 'authorized') {
    var status = req.param('status');
    API_STATUS = status;
    res.send({success: 1, status: status});
  } else {
    res.send({success: 0, message: "Not authorized"});
  }
});

router.get('/content/refresh', function(req, res) {
  if (req.signedCookies && req.signedCookies.admin == 'authorized') {
    var cref = contentManager.refresh()
    var sref = shopManager.refresh();
    BASIC_CONTENT.levels = contentManager.getLevelPack('core');
    BASIC_STRING = JSON.stringify(BASIC_CONTENT);
    
    if (cref || sref) 
      res.send({success: 0, cref: (cref? cref.message: 0), sref: (sref? sref.message : 0)});
    else
      res.send({success: 1});
  } else {
    res.send({success: 0, message: "Not authorized"});
  }
});

router.get('/content/minify', function(req, res) {
  if (req.signedCookies && req.signedCookies.admin == 'authorized') {
    minifyJS();
    res.send({success: 1, message: "minified"});
  } else {
    res.send({success: 0, message: "Not authorized"});
  }
});

router.get('/admin/dumpsql', function(req, res) {
  if (req.signedCookies && req.signedCookies.admin == 'authorized') {
  
    userManager.dumpDatabase(function(error) {
      if (error) {
        res.send({success:0, error: error});
        return;
      }
      res.send({success: 1, message: "DB dumped"});
    });

  } else {
    res.send({success: 0, message: "Not authorized"});
  }
});

router.get('/admin/getdump', function(req, res) {
  if (req.signedCookies && req.signedCookies.admin == 'authorized') {
  
    userManager.getDump(res);

  } else {
    res.send({success: 0, message: "Not authorized"});
  }
});

router.get('/shop', function(req, res) {
  res.redirect('/');
});

var API = function(route, method, handle) {
  router[method]('/api/' + route, function(req, res) {
    if (API_STATUS == "disabled") {
      res.send({success: 0, error: "CONNECTION_ERROR", message: "Epigon is currently offline"});
    } else {
      handle(req, res);
    }
  });
}  

API('get_braintree_client_token', 'get', function (req, res) {
  userManager.braintreeGateway.clientToken.generate({}, function (err, response) {
    if (err) {
      console.log(err);
      res.send({success: 0});
      return;
    }
    res.send({success: 1, token: response.clientToken});
  });
});

API('logout', 'get', function(req, res) {
  res.clearCookie('network');
  res.clearCookie('network_id');
  res.clearCookie('user_id');
  res.clearCookie('last_save');
  res.send({success: 1});
});


API('login', 'post', function(req, res) {
  var userInfo = getAccountInfo(req.signedCookies) || {}; 
  
  if (userInfo.user_id) {
    userManager.getAccountContent(userInfo.user_id, function(error, content) {
      
      if (error) {
        res.send({success: 0, error: error});
        return;
      }
      
      if (userInfo.lastSave) {
        content.lastSave = userInfo.lastSave;
      }
      
      res.send({success: 1, content: content}); 
    });
    return;
  }
  
  var credentials = req.param('credentials');
  var data = req.param('data') || {};
  
  if (data.levelHistory) {
    data.levelHistory = JSON.parse(data.levelHistory);
  }
  
  if (_.isObject(credentials) && credentials.network) {
    userManager.checkCredentialsAndUpdateCookie(credentials, data, res);
  } else {
    res.send({success: 0, message: "Invalid login credentials.", error: "INVALID_CREDENTIALS"});
  }
});

API('purchase_items', 'post', function(req, res) {
  var userInfo = getAccountInfo(req.signedCookies);
  var item_ids = req.param('item_ids');
  var method = req.param('method');
  var braintreeData = req.param('data');
  var payed = req.param('amount_payed');
  
  if (!_.isArray(item_ids) || (method != 'POINTS' && method != 'CASH')) {
    res.send({success: 0, error: "INVALID"});
    return;
  }
  
  if (method == 'CASH' && !braintreeData) {
    res.send({success: 0, error: "INVALID"});
    return;
  }
  
  if (method == 'CASH' && API_STATUS == 'payments_disabled') {
    res.send({success: 0, error: "PAYMENTS_DISABLED", message: "Cash purchases have been temporarily disabled. Sorry."});
    return;
  }
  
  if (userInfo.status == "none") {
    res.send({success: 0, error: "NOT_SIGNED_IN"});
    return;
  }
  
  userManager.purchaseItems(userInfo, item_ids, method, payed, braintreeData, function(error) {
    if (error) {
      console.log(error);
      if (_.isObject(error)) {
        res.send({success: 0, error: error.error, message: error.message});
      } else {
        res.send({success: 0, error: error});
      }
    } else {
      userManager.getAccountContent(userInfo.user_id, function(error, content) {
        if (error) {
          console.log(error);
          res.send({success: 0, error: "SERVER_ERROR", message: "Items were purchased, however there has been an error retrieving the new game content. Please check again later."});
        } else {
          res.send({success: 1, content: content}); 
        }
      });
    }
  });
});


API('contact_us', 'post', function(req, res) {
  var userInfo = getAccountInfo(req.signedCookies);
  var message = req.param('message');
  var email = req.param('email');
  var subject = req.param('subject');
  
  /*
  delete userInfo.settings;
  delete userInfo.avatar;
  */
  
  userInfo.email = email;
  
  try {
    var mailOpts = {
      from: 'Rotopo - Contact',
      to: 'barkandfester@gmail.com',
      text: JSON.stringify(userInfo) + " " + message,
      subject: 'Rotopo:: ' + subject,
    }
    
    emailer.sendMail(mailOpts, function(error, info) {
      if (error) {
        console.log(error);
        res.send({success: 0, error: "MAIL_ERROR", message: "There was an error processing your message."});
      }
      
      res.send({success: 1});
    });
  } catch(error) {
    console.log(error);
    res.send({success: 0, error: "PROCESSING_ERROR", message: "There was an error processing your message."});
  }
  
});

API('get_shop_items', 'get', function(req, res) {
  res.send({
    items: shopManager.getAllItems()
  });
});

/*
API('save_settings', 'post', function(req, res) {
  var data = req.param('data');
  
  if (!_.isObject(data) || !data.settings) {
    res.send({success: 0, error: "INVALID"});
    return;
  }
  
  try {
    res.cookie('settings', JSON.stringify(data.settings), {signed: true});
    res.cookie('avatar', data.avatar, {signed: true});
    res.send({success: 1});
  } catch(error) {
    res.send({success: 0, error: "INVALID"});
  }
});
*/

var _hs = function(a) {
  var hs = 0;
  if (a.length == 0) return hs;
  for (i = 0; i < a.length; i++) {
    char = a.charCodeAt(i);
    hs = ((hs<<5)-hs)+char;
    hs = hs & hs;
  }
  return hs;
};

API('save', 'post', function(req, res) {
  var userInfo = getAccountInfo(req.signedCookies);
  var data = req.param('data');
  var type = req.param('type');
  var clientVersion = req.param('client_version');
  if (!clientVersion) {
    res.send({success: 0, error: "UPDATE_REQUIRED", message: "Please reload this page to update to the newest version."});
  }
  
  
  if (userInfo.status == "none") {
    res.send({success: 0, error: "NOT_SIGNED_IN"});
    return;
  }
  
  if (!_.isObject(data)) {
    res.send({success: 0, error: "INVALID"});
    return;
  }
  if (type != 'fail' && type != 'pass') {
    res.send({success: 0, error: "INVALID"});
    return;
  }
  
  if (type == 'pass' && APP_INFO.APP_MODE == 'release' && userInfo.lastSave) {
    if (Date.now() - Number(userInfo.lastSave.time) < 1200) {
      res.send({success: 0, error: "OVERLOAD_ERROR", message: "Rapid API calls have tripped a bot detector on the server"});
      return;
    }
  }
 
  if (data.winloss) {
    data.winloss = Number(data.winloss);
    if (data.winloss) {
      data.winloss = data.winloss.toFixed('2');
    }
  }
  
  if (type == 'fail') {
    res.cookie('last_save', {
      level_id: Number(data.level_id), 
      puzzle_index: Number(data.puzzle_index)-1,
      time: Date.now(), 
      retry_record : Number(data.retry_record),
      time_record: Number(data.time_record),
      turn_record: Number(data.turn_record),
      winloss: data.winloss
    }, {signed: true});
    
    res.send({success: 1});
    return;
  } else if (type == 'pass') {
    var hs = data.ts;    
    if (hs != _hs(userInfo.user_id + '+' + data.level_id + '+' + data.puzzle_index)) {
      res.send({success: 0, error: "INVALID"});
      return;
    }
    
    userManager.saveUserData(userInfo, data, function(error, newpoints, lastSave) {
      
      if (error) {
        res.send({sucess: 0, error: "API_ERROR", message: error});
      } else {
        res.cookie('last_save', lastSave, {signed: true});
        res.send({success: 1, points: newpoints});
      }
    });
  }
});

});

}
module.exports = router;
