var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var busboy = require('connect-busboy');

var routes = require('./routes/index');
var lessMiddleware = require('less-middleware');
var fs = require('fs');
var simpleLogger = require('simple-node-logger');

var eLog = simpleLogger.createRollingFileLogger({
  logDirectory:'./logs',
  fileNamePattern:'<DATE>-error.log',
  dateFormat:'YYYY.MM.DD'
});

var importantLog = simpleLogger.createRollingFileLogger({
  logDirectory:'./logs',
  fileNamePattern:'<DATE>-important.log',
  dateFormat:'YYYY.MM.DD'
});

var app = express();

if (routes.userManager)
  routes.userManager.setLogger(eLog, importantLog, routes.emailer, routes.APP_INFO.EMAIL, routes.APP_INFO.APP_MODE == "release" ? "" : "TESTING: ");


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

if (routes.APP_INFO.APP_MODE == "release") {
  var protectPath = function(regex) {
    return function(req, res, next) {
      if (!regex.test(req.url)) { return next(); }
      res.render('404');
    };
  }

  app.use(protectPath(/^\/javascripts\/player\/.*$/));
}

app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(favicon());


if (app.get('env') === 'development') {
  app.use(logger('dev'));
} 


app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(busboy());
app.use(cookieParser(routes.APP_INFO.COOKIE_SECRET));


if (app.get('env') === 'development') {
  app.use(express.static(path.join(__dirname, 'public')));
} else {
  app.use(express.static(path.join(__dirname, 'public'), {maxAge: 3600 * 1000}));
} 

app.use('/', routes);

process.env.PORT = routes.APP_INFO.APP_PORT;

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    res.status(404);
    res.render('404');
});

/// error handlers


process.on('uncaughtException', function(err) {
  console.log(err, err.stack);
  //importantLog.error(err.message, " ", err.stack);
  var d = new Date;
  var twoDigs = function(n) {
    if (n < 10) return "0" + n;
    else return n;
  }
  
  fs.appendFileSync("logs/" + d.getFullYear() + "." + twoDigs(d.getMonth() + 1) + "." + twoDigs(d.getDate()) + ".uncaughtException.log", d.toLocaleString() + "\n" + err.message + " " + err.stack);
  process.exit(1);
});


console.log("environment", app.get('env'));


var gracefulShutdown = function() {
  app.get('server').close();
  eLog.error("Shutdown signal received");
  console.log("Closing connections");
  
  setTimeout(function() {
    console.log("Disconnect and shutdown");
    routes.userManager.disconnect();
    process.exit(0);
  }, 1500); 
}

process.on('message', function(msg) {
  if (msg == "shutdown") {
    gracefulShutdown();
  }
});

process.on('SIGINT', gracefulShutdown);



// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    eLog.error(err.message, " ", err.stack); 
    console.log(err, err.stack);

    res.render('error', {
        message: err.message,
        error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  eLog.error(err.message, " ", err.stack); 

  res.send(500, 'error', {
      message: "An error has occurred, or the server is going offline.",
      error: {}
  });
});

module.exports = app;
