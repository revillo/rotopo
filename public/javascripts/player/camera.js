var Camera = function(WB) {
  'use strict'

  var GFX = GAME.GFX;
  
  WB.GimbleCamera = GAME.Models.Model.extend({
    setup: function() {
      this.tempMat4 = new THREE.Matrix4();
    },
    
    saveRemovingState: function() {
      this.removeCameraPos = _.clone(this.cameraPos);
      
      this.removeCameraUpBasic = _.clone(this.cameraUp);
      this.removeCameraUp = this.getRealUp();
      var camera = this.controller.scene._camera;
      this.removeCameraTarget = this.cameraTarget;
      this.removeCameraDistance = Vec.distance(this.cameraPos, this.cameraTarget);
      this.removingQuat = camera.quaternion.clone();
      
      /*
      camera.updateMatrixWorld();
      console.log(camera.matrixWorld);
      this.removingQuat = new THREE.Quaternion();
      this.removingQuat.setFromRotationMatrix(camera.matrixWorld);
      this.removingQuat.normalize();
      console.log(this.removingQuat);
      console.log(camera.quaternion);*/
    },
    
    getRealUp: function() {
      var dir = Vec.sub([0,0,0], this.cameraPos);
      var right = Vec.perpendicular(dir, this.cameraUp);
      
      return Vec.perpendicular(right, dir);
    },
    
    saveVictoryState: function() {
      this.victoryCameraPos = _.clone(this.cameraPos);
      this.victoryCameraUp = this.getRealUp();   
      this.victoryCameraTarget = _.clone(this.cameraTarget);
      var camera = this.controller.scene._camera;
      this.victoryQuat = camera.quaternion.clone();
    },
    
    updateVictory: function() {
      var controller = this.controller;
      var playerPos = controller.player.getPosition();
      var calcTarget = playerPos;
      var calcUp = Vec.invert(controller.playerIn);
      var vTheta = controller.activePuzzle.victoryTime;
      var calcPos = Vec.add(playerPos, Vec.setLength(Vec.lerp(calcUp, controller.playerDir, 0.5), WB.CAMERA_DIST));
      var amount = Math.inOut(vTheta / (WB.VICTORY_DUR), 1.5);        
      var camera = this.controller.scene._camera;
      controller.scene.setCamera(calcPos, calcTarget, calcUp);        
      camera.quaternion.slerp(this.victoryQuat, 1.0-amount);
      var dir = camera.getWorldDirection();
      var target = new THREE.Vector3();
      target.fromArray(Vec.lerp(this.victoryCameraTarget, calcTarget, amount));
      var dist = Math.lerp(Vec.distance(this.victoryCameraPos, this.victoryCameraTarget), Vec.distance(calcPos, calcTarget), amount);
      camera.position.copy(target.add(dir.setLength(-dist)));
      this.cameraPos = calcPos;
      this.cameraTarget = calcTarget;
      this.cameraUp = calcUp;
      //controller.scene.setFOV(65);
      return;
    },
    
    getPosition: function() {
      return GFX.V3toArray(WB.gamePlayer.scene._camera.position);
    },
    
    updateRemoving: function(calcPos, calcTarget, calcUp, elapsedTime) {
      var controller = this.controller;
      var amount = Math.inOut(controller.removingAmount, 2.0);
      var camera = controller.scene._camera;
      controller.scene.setCamera(calcPos, calcTarget, calcUp);      
      
      var target = new THREE.Vector3();
      target.fromArray(Vec.lerp(this.removeCameraTarget, calcTarget, amount));
      
      camera.quaternion.slerp(this.removingQuat, 1.0-amount);

      var dir = camera.getWorldDirection();
      
      var dist = Math.lerp(this.removeCameraDistance, Vec.distance(calcPos, calcTarget), amount);
       
      camera.position.copy(target.add(dir.setLength(-dist)));
      
      this.cameraPos = calcPos;
      this.cameraUp = calcUp;
      this.cameraTarget = calcTarget;
      controller.scene.setFOV(65);
    },
    
    gameUpdate: function(elapsedTime) {
      var controller = this.controller; //WB.gamePlayer     
   
      if (controller.screenSaver || !controller.activePuzzle) {
        this.cameraPos = [0, 0.0, -2.5];
        this.cameraUp = [0, 1.0, 0];
        this.cameraTarget = [0.0, 0.0, 0.0];
        this.scene.setCamera(this.cameraPos, this.cameraTarget, this.cameraUp);
        this.scene.setFOV(90);
        return;
      } else if (controller.activePuzzle.state == WB.PUZZLE_STATE.INSPECT) {
        this.cameraPos = [0, 0.0, -controller.activePuzzle.inspectDistance];
        this.cameraUp = [0, 1.0, 0];
        this.cameraTarget = [0.0, 0.0, 0.0];
        this.scene.setCamera(this.cameraPos, this.cameraTarget, this.cameraUp);
        this.scene.setFOV(80);
        return;
      }
      
      var puzzleState = controller.activePuzzle.state;
      
      var playerPos = controller.player.getPosition();
      
      if (false && WB.gamePlayer.playerCharacterUp) {
      //View avatar
        this.cameraPos = Vec.displace(playerPos, WB.gamePlayer.playerDir, 1.0);
        this.cameraUp = WB.gamePlayer.playerCharacterUp;
        this.cameraTarget = playerPos;
        this.scene.setCamera(this.cameraPos, this.cameraTarget, this.cameraUp);
        this.scene.setFOV(65);
        return;
      }
      
      if (controller.activity == WB.GAME_ACTIVITY.REMOVING) {
        playerPos = WB.gamePlayer.activePuzzle.startPos
      }

      var spaceDir = Vec.normalize(playerPos);
      
      var cameraType = "float";
            
      var normalDir = Vec.invert(controller.playerIn);
      var forwardDir = Vec.clone(controller.playerForward);
      //var normalDir = GFX.slerpVec(controller.playerCharacterUp, Vec.invert(controller.playerIn), 0.5);
      
      cameraType = controller.activePuzzle.cameraType || "float";

      var targetScale = controller.activePuzzle.targetScale || 0.5;
      //var targetScale = 0.5;

      
      //targetScale = Math.clamp(Vec.length(playerPos) / (controller.activePuzzle.sideLength * 7), 0.0, 1.0);
      //targetScale = Math.clamp(Vec.length(playerPos) / (controller.activePuzzle.sideLength * 7), 0.5, 1.0);
      
      var cameraAngle = 90;
      var follow = 0;
      var track = 0;

      if (cameraType == "float") {
        cameraAngle = controller.activePuzzle.cameraAngle || 90;
      } else if (cameraType == "follow") {
        cameraType = "float";
        targetScale = 1.0;
        follow = 1;
        cameraAngle = controller.activePuzzle.cameraAngle || 45;
      } else if (cameraType == "orbit") {
        targetScale = 0.0;
      } else if (cameraType == "track") {
        track = 1;
        cameraType = "float";
        targetScale = 1.0;
        cameraAngle = controller.activePuzzle.cameraAngle || 45;
      } 
      
      if (WB.overlay.settings.camera == "follow") {
        targetScale = 1.0;
        cameraType = "float";
      }

      //look ahead stuff
      if (cameraType == "float") {
        var playerTile = controller.playerTile;
        var posDir = Vec.sub(playerPos, playerTile.center);
        var tieDir = controller.playerDir;
       
        var s = playerTile.sideLength * 0.5;
        var nbrs = playerTile.neighbors;
        var playerStr = Vec.axisString(Vec.toAxis(controller.playerDir));
        
        var dirScale = 0.35; // 0.6;
        var offScale = 0.0; //  0.3;
        //normalDir = playerTile.normal;

        var dirs = ['x', 'y', 'z'];
        
        for (var i = 0; i < dirs.length; i++) {
          var ps = 'p' + dirs[i];
          var ns = 'n' + dirs[i];
          
          if (playerStr == ps && nbrs[ps] && (!track || nbrs[ps].cameraData.movecam))  {
            var scale = (posDir[i]+s) * dirScale / s;
            //(posDir[i]+s) * dirScale / s)
            //normalDir = Vec.add(normalDir, Vec.setLength(nbrs[ps].normal, scale));
            normalDir = GFX.slerpVec2(normalDir, playerTile.normal, nbrs[ps].normal, scale, tieDir);
            forwardDir = GFX.slerpVec2(forwardDir, playerTile.normal, nbrs[ps].normal, scale, tieDir);
            continue;
          } 
          
          if (playerStr == ns && nbrs[ns] && (!track || nbrs[ns].cameraData.movecam)) {
            var scale = -(posDir[i]-s) * dirScale / s;
            //-(posDir[i]-s) * dirScale / s)
            //normalDir = Vec.add(normalDir, Vec.setLength(nbrs[ns].normal, scale));
            normalDir = GFX.slerpVec2(normalDir, playerTile.normal, nbrs[ns].normal, scale, tieDir);
            forwardDir = GFX.slerpVec2(forwardDir, playerTile.normal, nbrs[ns].normal, scale, tieDir);
            continue;
          }           
        }
      }
      
      
      normalDir = Vec.normalize(normalDir);
      
      var theta = Vec.angleBetween(normalDir, spaceDir);
            
      var cameraFight = theta / Math.PI;
      
      
      if (theta > Math.PI / 2) {
        cameraFight = 0.85;
      }
      
      
      var cameraFactor = cameraFight;

      //Orbit
      var cameraDir = GFX.slerpVec(spaceDir, normalDir, cameraFactor);      
      var calcTarget = [0,0,0];
      
      
      if (cameraType == "float") {
        //cameraDir = GFX.slerpVec(spaceDir, normalDir, 1.0);     
        
        
        cameraDir = normalDir;   
        calcTarget = Vec.scale(playerPos, targetScale);
        
        
        if (cameraAngle < 90 && !WB.gamePlayer.playerTile.cameraData.movecam) {
          cameraDir = Vec.add(
            Vec.scale(normalDir, Math.sin(Math.radians(cameraAngle))),
            Vec.scale(forwardDir, -Math.cos(Math.radians(cameraAngle)))
          );
        }
      }
      
      
      
      /*
      if (track) {
        cameraDir = Vec.add(
          Vec.scale(controller.activePuzzle.cameraUp, Math.sin(Math.radians(cameraAngle))),
          Vec.scale(controller.activePuzzle.cameraDir, -Math.cos(Math.radians(cameraAngle)))
        );
        
      } else */ 
            
      var calcUp = forwardDir; 
      
      if (follow) {
        calcUp = normalDir;
      } 
      
      if (track) {
        if (WB.gamePlayer.playerTile.cameraData.movecam) {
          track = 0;
          follow = 1;
          cameraAngle = 90;
          calcUp = controller.playerForward;
        } else {
          calcUp = controller.activePuzzle.cameraUp;
        }
      }

      if (track) {
        cameraDir = Vec.add(
          Vec.scale(controller.activePuzzle.cameraUp, Math.sin(Math.radians(cameraAngle))),
          Vec.scale(controller.activePuzzle.cameraDir, -Math.cos(Math.radians(cameraAngle)))
        );
      } 
      
      if (follow) {
        cameraDir = Vec.add(
          Vec.scale(normalDir, Math.sin(Math.radians(cameraAngle))),
          Vec.scale(forwardDir, -Math.cos(Math.radians(cameraAngle)))
        );
        
        
        //cameraDir = normalDir;
        
        //cameraDir = GFX.slerpVec(cameraDir, Vec.invert(controller.playerForward), 0.5);
      }
      
      var ar = WB.state.get('window').ar;
      var camDistFactor = 1.0;
      
      
      var cameraDist = controller.activePuzzle.cameraDist || WB.CAMERA_DIST;
      
      if (ar < 1.0) {
        cameraDist *= 1.0 / ar;
      }
      
      var calcOffset = Vec.setLength(cameraDir, cameraDist);

      var calcPos = Vec.add(playerPos, calcOffset);
      
      this.cameraUp = this.cameraUp || controller.playerForward;
      this.cameraPos = this.cameraPos || calcPos;
      this.cameraTarget = this.cameraTarget || calcTarget;
      this.cameraOffset = this.cameraOffset || Vec.sub(calcPos, playerPos);

      
      var fov = 65;
      var cameraSpeed = WB.gamePlayer.activePuzzle.cameraSpeed || 1.0;
      
      
      //Update for victory
      if (controller.activity == WB.GAME_ACTIVITY.REMOVING) {
        this.updateRemoving(calcPos, calcTarget, calcUp, elapsedTime);
        this.cameraOffset = calcOffset;
      } else if (puzzleState == WB.PUZZLE_STATE.VICTORY) {
        this.updateVictory();
      } else if (puzzleState == WB.PUZZLE_STATE.PLAY || puzzleState == WB.PUZZLE_STATE.VICTORY) { 
          if (WB.gamePlayer.avatarData.frozen) {
            return;
          }
        
        this.cameraUp = GFX.slerpVec(this.cameraUp, calcUp, Math.clamp(elapsedTime * 2.5 * cameraSpeed, 0.0, 1.0), elapsedTime * 0.5, undefined, controller.playerDir);
        this.cameraOffset = GFX.slerpVec(this.cameraOffset, calcOffset, Math.clamp(elapsedTime * 2.5 * cameraSpeed, 0.0, 1.0), elapsedTime * 0.5, undefined, controller.playerDir);
                
        this.cameraPos = Vec.add(playerPos, this.cameraOffset);
        this.cameraTarget = calcTarget;
        controller.scene.setCamera(this.cameraPos, this.cameraTarget, this.cameraUp);
        controller.scene.setFOV(fov);
        var camera = this.controller.scene._camera;
        
        
        //Quat Slerp

        /*
        var camera = this.controller.scene._camera;
        var oldQuaternion = camera.quaternion.clone();
        controller.scene.setCamera(calcPos, calcTarget, calcUp);
        
        var amount = Math.clamp(elapsedTime * 1.9, 0.0, 1.0);
        if (follow) {
          amount = Math.clamp(elapsedTime * 4.0, 0.0, 1.0);
        }
                
        camera.quaternion.slerp(oldQuaternion, 1.0 - amount);
        var dir = camera.getWorldDirection();
        var target = new THREE.Vector3();
        target.fromArray(calcTarget);
        var dist = Math.lerp(Vec.distance(this.cameraPos, this.cameraTarget), Vec.distance(calcPos, calcTarget), amount * 2.0);
        
        camera.position.copy(target.add(dir.setLength(-dist)));
        var up = new THREE.Vector3(0, 1, 0);
        camera.localToWorld(up);
        
        this.cameraPos = GFX.V3toArray(camera.position);
        this.cameraUp = GFX.V3toArray(up);
        this.cameraTarget = calcTarget;
        this.cameraOffset = Vec.sub(this.cameraPos, playerPos); 
        controller.scene.setFOV(fov);
        */
      }
    }
  });
}