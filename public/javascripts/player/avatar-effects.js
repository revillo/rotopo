var AvatarEffects = function(WB) {
  'use strict'
  var GFX = GAME.GFX;
  
  WB.BasicMaterial = new THREE.ShaderMaterial({
    uniforms: {
      color: {type: 'c', value: new THREE.Color(0x453048)},
      ambient: {type: 'f', value: 0.8},
      gloss: {type: 'f', value: 0},
      specularColor: {type: 'v3', value: new THREE.Vector3(0, 0, 0)},
      backfaceColor: {type: 'c', value: new THREE.Color(0x453048)},
      dissolve: {type: 'f', value: 0.0},
      aliasScale : {type: 'f', value: 1.0},
      opacity: {type: 'f', value: 1.0}
    },
    vertexShader: $('#avatarVertexShader').text(),
    fragmentShader: $('#avatarFragmentShader').text(),
    shading: THREE.FlatShading,
    transparent: true
  });
  
  WB.BasicMaterial.sharedAsset = true;
  WB.MiniMeMaterial = WB.BasicMaterial.clone();
  WB.MiniMeMaterial.side = THREE.DoubleSide;
  WB.MiniMeMaterial.sharedAsset = true;
  
  WB.urchinMaterial = WB.BasicMaterial.clone();
  //urchinMaterial.uniforms.color.value.fromArray([0.95, 0.5, 0.5]);
	//urchinMaterial.uniforms.color.value.fromArray([0.15, 0.1, 0.1]);
	WB.urchinMaterial.uniforms.color.value.fromArray([.9, 0.9, 0.9]);
  //urchinMaterial.uniforms.color.value.fromArray([0.3, 0.1, 0.1]);
  WB.urchinMaterial.uniforms.specularColor.value.fromArray([0.5, 0.5, 0.6]);
  WB.urchinMaterial.uniforms.gloss.value = 10;
  WB.urchinMaterial.uniforms.ambient.value = 0.8;
  WB.urchinMaterial.uniforms.dissolve.value = 0.0;
  WB.urchinMaterial.sharedAsset = true;
  
  WB.urchinGeometry = new THREE.IcosahedronGeometry(0.02);
  WB.urchinGeometry.sharedAsset = true;
  
  var urchinGeometry = WB.urchinGeometry;
  var urchinMaterial = WB.urchinMaterial;
  
  var tris = urchinGeometry.faces.length;
  
  for (var t = 0; t < tris; t++) {
    var face = urchinGeometry.faces[t];
    if (face.normal.z >= 0)
      GFX.extrudeFaceToPoint(urchinGeometry, t, 0.075); 
  }
  
  urchinGeometry.computeFaceNormals();
  urchinGeometry.faceVertexUvs[0] = [];
  
  WB.Urchin = GFX.Object3D.extend({
    geometry: urchinGeometry,
    radius: 0.075,
    material: urchinMaterial,
    
    wiggle: function() {
      this.wiggleTime = 0;
      WB.audioManager.playSynth({
        synthType1: 'square',
        synthType2: 'sawtooth',
        frequency: 1680,
        volume: WB.VOLUME * 9.0
      });
    },
    
    update: function(elapsedTime) {
      if (this.wiggleTime != undefined) {
        this.wiggleTime += elapsedTime;
        var wiggleScale = Math.sin(Math.pow(this.wiggleTime, 0.5) * 25.0) * (0.6 - this.wiggleTime * 0.2);
        if (wiggleScale < 0.0) {
          wiggleScale *= 0.5;
        }
        this.setScale(1.0 + wiggleScale);
      }
    }
  });
  
  var bulletMaterial = WB.BasicMaterial.clone();
  //bulletMaterial.uniforms.color.value.fromArray([0.616, 0.392, 0.102]);
  bulletMaterial.uniforms.color.value.fromArray([0.33, 0.30, 0.30]);
  bulletMaterial.uniforms.ambient.value = 0.6;
  bulletMaterial.uniforms.specularColor.value.fromArray([0.5, 0.5, 0.4]);
  bulletMaterial.uniforms.gloss.value = 10;
  
  var bulletHitMaterial = WB.BasicMaterial.clone();
  bulletHitMaterial.uniforms.color.value.fromArray([0.80, 0.23, 0.16]);
  
  var bulletGeometry = GFX.makeSphereGeometry(0.03, 1);
  GFX.transformGeometry({
    geometry: bulletGeometry,
    translate: [0.0, 0.0, 0.15]
  })
  
  WB.Bullet = GFX.Object3D.extend({
    material: bulletMaterial,
    geometry: bulletGeometry,
    radius: 0.03, 
    lift: 0.15,
    active: true,
    
    update: function(elapsedTime) {
      if (this.fadeTime > 2.0) {
        return;
      }
      this.puzzle.moveAlong(this.state, elapsedTime);
      this.orient();
      
      if (!this.active) {
        this.fadeTime += elapsedTime;
        this.material.uniforms.dissolve.value += this.fadeTime * 0.5;
      }
    },
    
    orient: function() {
      this.setPosition(this.state.position);
      this.faceDirection(this.state.animUp, this.state.direction);
    },
    
    hit: function() {
      this.glObject.material = bulletHitMaterial;
    },
    
    fadeOut: function() {
      this.active = false;
      this.fadeTime = 0;
      this.material = this.material.clone();
      this.glObject.material = this.material;
    }
  });
  
  WB.bombBlast = GFX.ParticleSystem.extend({
    particleCount: 40,
    particleSize: 0.2,
    particleOpacity: 1.0,
    particleColor: 0xffffff,
    
    
    initParticles: function() {
      this.velocities = [];
      this.glObject.geometry.vertices.length = this.particleCount;
      this.velocities.length = this.particleCount;
      
      for (var i = 0; i < this.particleCount; i++) {
        this.velocities[i] = new THREE.Vector3();
        this.velocities[i].fromArray(Vec.randomAngle());
        this.glObject.geometry.vertices[i] = new THREE.Vector3();
      }
    },
    
    update: function(elapsedTime) {
      GFX.ParticleSystem.prototype.update.call(this, elapsedTime);
    
      var vs = this.glObject.geometry.vertices;
      for (var i = 0, l = vs.length; i < l; i++) {
        vs[i].addScaledVector(this.velocities[i], elapsedTime * 1.5);
        this.glObject.geometry.verticesNeedUpdate = true;
      }
      
      this.material.opacity -= elapsedTime * 2.0;
      if (this.material.opacity < 0) this.material.opacity = 0.0;
    }
  });
  
  
  WB.jetpackFireMaterial = WB.BasicMaterial.clone();
  WB.jetpackFireMaterial.uniforms.color.value.fromArray(Color255([47, 199, 255]));
  WB.jetpackFireMaterial.uniforms.ambient.value = 1.0;

  WB.GradientMaterial = new THREE.ShaderMaterial({
    vertexShader: $('#oceanVertexShader').text(),
    fragmentShader: $('#gradientFragmentShader').text(),
    side: THREE.BackSide,
    uniforms : {
      loColor: {type: 'c', value: new THREE.Color()},
      hiColor: {type: 'c', value: new THREE.Color()}
    }
  });
  
  WB.OceanMaterial = new THREE.ShaderMaterial({
    vertexShader: $('#oceanVertexShader').text(),
    fragmentShader: $('#oceanFragmentShader').text(),
    side: THREE.BackSide,
    shading: THREE.FlatShading,
    transparent: true
  });
  
  WB.CoinGeometry = new THREE.IcosahedronGeometry(0.05, 0);
  WB.CoinMaterial = WB.BasicMaterial.clone();
  WB.CoinMaterial.uniforms.color.value.setHex(0xe6bc13);
  WB.CoinMaterial.uniforms.gloss.value = 2.0;
  WB.CoinMaterial.uniforms.specularColor.value.set(0.2, 0.2, 0.2);
  
  WB.Coin = GFX.Group.extend({
    height: 0.15,
    radius: 0.05,
    
    setup: function() {
      this.glObject = new THREE.Object3D();
      this.coin = new GFX.Object3D({
        type: 'collada',
        material: WB.CoinMaterial,
        geometry: this.geometryLoader.getGeometry('coin')
      });
      
      this.add(this.coin);
      this.coin.setPosition([0, 0, this.height]);
      this.coin.setScale(0.07);
      this.clock = 0;
    },
    
    update: function(elapsedTime) {
      this.clock += elapsedTime;
      this.coin.setPosition([0, 0, this.height + Math.sin(this.clock * 5.0) * this.radius]);
      this.coin.setEuler([0,0,this.clock * 5.0]);
    }
  });
  
  WB.Bubbles = GFX.ParticleSystem.extend({
    particleCount: 15,
    particleSize: 0.05,
    particleOpacity: 0.8,
    particleColor: 0xf3f3ff,
    
    initParticles: function() {
      this.velocities = [];
      this.glObject.geometry.vertices.length = this.particleCount;
      this.velocities.length = this.particleCount;
      
      for (var i = 0; i < this.particleCount; i++) {
        this.velocities[i] = new THREE.Vector3();
        this.velocities[i].fromArray([0,1,0]);
        this.glObject.geometry.vertices[i] = this.getStartingPoint();
      }
    },

    getStartingPoint: function(v3) {
      v3 = v3 || new THREE.Vector3();
      v3.fromArray([0, 0, 0.05]);
      
      /*
      if (Vec.length(this.object.getPosition()) < 0.001) {
        v3.fromArray([5, 5, 0]);
        return v3;
      }*/
      
      if (this.object.glObject.matrixWorld)
        v3.applyMatrix4(this.object.glObject.matrixWorld); 
      return v3;
    },

    update: function(elapsedTime) {
      //if (this.object.visible)
      GFX.ParticleSystem.prototype.update.call(this, elapsedTime);

      var vs = this.glObject.geometry.vertices;
      for (var i = 0, l = vs.length; i < l; i++) {
        //vs[i].setLength(vs[i].length() + elapsedTime * this.speed);

        vs[i].addScaledVector(this.velocities[i], elapsedTime * 0.6);
        this.glObject.geometry.verticesNeedUpdate = true;
        if (Math.random() < elapsedTime * 0.1) {
          this.getStartingPoint(vs[i]);
        }
      }
    }
  });
  
  WB.jetpackSmoke = GFX.ParticleSystem.extend({
    particleCount: 30,
    particleSize: 0.02,
    decayRate: 0.0,
    
    initParticles: function() {
      this.velocities = [];
      this.glObject.geometry.vertices.length = this.particleCount;
      this.velocities.length = this.particleCount;
      
      for (var i = 0; i < this.particleCount; i++) {
        this.velocities[i] = new THREE.Vector3();
        this.velocities[i].fromArray(this.particleDir);
        this.glObject.geometry.vertices[i] = this.getStartingPoint();
      }

    },

    getStartingPoint: function(v3) {
      //var v = Math.floor(Math.random() * this.object.geometry.vertices.length);
      v3 = v3 || new THREE.Vector3();
      //console.log(this.object.geometry);
      v3.x = this.particleDir[0];
      v3.y = this.particleDir[1];
      v3.z = this.particleDir[2];
      //v3.copy(this.object.geometry.vertices[v]);
      v3.applyMatrix4(this.object.matrixWorld);      
      GFX.perturb(v3, 0.025);
      return v3;
    },

    update: function(elapsedTime) {
      //if (this.object.visible)
      GFX.ParticleSystem.prototype.update.call(this, elapsedTime);

      var vs = this.glObject.geometry.vertices;
      for (var i = 0, l = vs.length; i < l; i++) {
        //vs[i].setLength(vs[i].length() + elapsedTime * this.speed);

        vs[i].addScaledVector(this.velocities[i], elapsedTime * 2.0);
        this.glObject.geometry.verticesNeedUpdate = true;
        if (Math.random() < 0.5) {
          this.getStartingPoint(vs[i]);
          var vel = Vec.copy(this.particleDir);
          GFX.perturb(vel, 0.1);
          this.velocities[i].fromArray(vel);
        }
      }
    }
  });
  
  // Themes
  WB.themes = {}
  var tileGeometries = [];
  var decoShader = WB.SceneryMaterial.clone();
  //decoShader.side = THREE.DoubleSide;
  
  WB.FlameMaterial = new THREE.ShaderMaterial({
    vertexShader: $('#particleVertexShader').text(),
    fragmentShader: $('#particleFragmentShader').text(),
    transparent: true,
    blending: THREE.AdditiveBlending,
    uniforms : {
      scale : {type: 'f', value: 15.0},
      playerPos : {type: 'v3', value: new THREE.Vector3()}
    }
  });
  
  WB.CloudMaterial = new THREE.ShaderMaterial({
    vertexShader: $('#cloudVertexShader').text(),
    fragmentShader: $('#cloudFragmentShader').text(),
    uniforms : {
      scale : {type: 'f', value: 10000.0}
    },
    transparent: true,
    depthTest: false,
    //blending: THREE.AdditiveBlending
  });
  
  WB.CloudMaterial.sharedAsset = true;
  
  
  WB.CloudSystem = GFX.ParticleSystem.extend({
    dynamic: false,
    material: WB.CloudMaterial,
    particleSize: 10000,
    
    initParticles: function() {
      var geo = this.glObject.geometry;
      
      for (var c = 0; c < 30; c++) {
        var distance = Math.randomReal(60.0, 200.0);
        var radius = Math.randomReal(1.0, 2.0) * distance;
        var theta = Math.randomReal(0, 2 * Math.PI);
        var u = Math.randomReal(-1, 1);

        var center = [
          Math.sqrt(1-(u*u)) * Math.cos(theta) * radius,
          Math.sqrt(1-(u*u)) * Math.sin(theta) * radius,
          u * radius]
        
        for (var b = 0; b < 10; b++) {
          
          var bcenter = Vec.vary(center, 10.0);
          
          for (var i = 0; i < 10; i++) {
            var p = new THREE.Vector3();
            p.fromArray(Vec.vary(bcenter, 5.0));
            geo.vertices.push(p);
          }
        }
      }
    },
  });
  
  WB.handleTileStepForAvatar = function(tile) {
    if (this.avatar && this.avatar.slots) {
      if (this.slotArray.length > 2) {
        this.slotArray = [];
      }
      
      this.slotArray.push(tile.slotType);

      if (this.slotArray.length > 2) {
        WB.overlay.factors['slots'] = WB.overlay.factors['slots'] || {reason: "Slots", factor: 1.0};
        var factor = WB.overlay.factors['slots'].factor;

        if (this.slotArray[0] == this.slotArray[1] == this.slotArray[2]) {
          WB.overlay.factors['slots'].factor = Math.clamp(factor + 0.25, 0.5, 5/3);
        } else {
          WB.overlay.factors['slots'].factor = Math.clamp(factor - 0.1, 0.5, 5/3);
        }
        
        WB.overlay.updateMultiplier();
        
      }
    }
  },

  WB.themes.addHazards = function() {
    var avatar = this.avatar;
    if (!avatar) return;
    
    this.avatarData = {};
    
    if (avatar.slots) {
      this.slotArray = [];
      this.avatarData.slots = [];
      for (var t = 0; t < this.tiles.length; t++) {
        this.tiles[t].slotType = t % 3; 
      }
    }
    
    if (avatar.coins) {
      this.avatarData.coins = [];
      this.avatarData.numCoins = 0;
      this.coinMap = {};
      
      if(WB.overlay)
        WB.overlay.setFactor('coins', {type: 'add', factor: 0, reason: "gold coins"});
      
      for (var t = 0; t < this.tiles.length; t++) {
        if (Math.randomReal(0, 1) < 0.8) {
          continue;
        }
        
        this.avatarData.numCoins++;
        
        var tile = this.tiles[t];
        var pos = Vec.displace(tile.center, tile.normal, tile.sideLength * 0.5);
        var s = tile.sideLength * 0.5;
        var hash = Math.round(pos[0]/s) + "," + Math.round(pos[1]/s) + "," + Math.round(pos[2]/s);
        if (this.coinMap[hash]) continue;
        
        var coin = new WB.Coin({
          geometryLoader: this.avatar.theme.sceneryGeometries
        });
        
        coin.setPosition(this.tiles[t].center);
        coin.faceDirection(this.tiles[t].normal, Vec.swapAxes(this.tiles[t].normal));
        coin.tile = this.tiles[t];
        coin.center = Vec.displace(this.tiles[t].center, this.tiles[t].normal, coin.height);
        this.add(coin);
        this.avatarData.coins.push(coin);
        this.coinMap[hash] = coin;
      }
    }
  
    if (avatar.bullets) {
      this.avatarData.bullets = [];
      for (var t = 0; t < this.tiles.length; t++) {
        if (t == this.startTile) continue;
        
        var tile = this.tiles[t];
        if (Math.randomReal(0,1) < 0.1) {
          var bullet = new WB.Bullet({
            type: 'collada',
            geometry: avatar.theme.bulletGeometry.getGeometry('bullet')
          });
          
          bullet.setScale(0.04);
          var dirString = _.randomKey(tile.neighbors);
          
          bullet.puzzle = this;
          
          bullet.state = {
            tile : tile,
            up : tile.normal,
            animUp : tile.normal, 
            direction: Vec.STRING_AXIS[dirString],
            speed: 1.0,
            position: tile.center
          }
          
          bullet.orient();
          
          this.avatarData.bullets.push(bullet);
          this.add(bullet);
        }
        
      }
    }
    
    
    if (avatar.urchins) {
      this.avatarData.urchins = [];
      for (var t = 0; t < this.tiles.length; t++) {
        if (Math.randomReal(0, 1.0) < 0.25 && t != this.startTile) {
          var urchin = new WB.Urchin();
          this.add(urchin);
          urchin.tile = this.tiles[t];
          var s = urchin.tile.sideLength * 0.8;
          var planeVec = Vec.sub([1,1,1], Vec.abs(urchin.tile.normal));
          var displace = [0,0,0];
          if (planeVec[0]) displace[0] = (0.5 - Math.randomReal(0, 1.0)) * s;
          if (planeVec[1]) displace[1] = (0.5 - Math.randomReal(0, 1.0)) * s;
          if (planeVec[2]) displace[2] = (0.5 - Math.randomReal(0, 1.0)) * s;
          displace = Vec.add(displace, Vec.setLength(urchin.tile.normal, 0.04 * s));
          urchin.originalPosition = Vec.add(urchin.tile.center, displace);
          urchin.setPosition(urchin.originalPosition);
          
          urchin.lookAt(Vec.add(urchin.originalPosition, urchin.tile.normal), [0,1,0]);
          //urchin.faceDirection(urchin.tile.normal, planeVec);
          this.avatarData.urchins.push(urchin);
        }
      }
    }
  };
  
  
  
  WB.themes.cleanUpHazards = function() {
    var avatar = this.avatar;
    if (!this.avatarData) return;
    function remove(object3D) {
      this.remove(object3D);
    }
    
    _.each(this.avatarData.urchins, remove, this);
    _.each(this.avatarData.bullets, remove, this);
    _.each(this.avatarData.coins, remove, this);
  },
  
  WB.themes.explodeHazards = function(amount) {
    if (!this.avatarData) return;
    
    if (this.avatarData.urchins) {
      var urchins = this.avatarData.urchins;
      for (var u = 0; u < urchins.length; u++) {
        var tile = urchins[u].tile;
        urchins[u].setPosition(Vec.add(urchins[u].originalPosition, Vec.scale(tile.normal, 8.0 * amount))); 
      }  
    }
    
    if (this.avatarData.bullets) {
      var bullets = this.avatarData.bullets;
      for (var b = 0; b < bullets.length; b++) {
        var tile = bullets[b].state.tile;
        bullets[b].setPosition(Vec.add(tile.center, Vec.scale(tile.normal, 8.0 * amount))); 
      }  
    }
    
    if (this.avatarData.coins) {
      var coins = this.avatarData.coins;
      for (var c = 0; c < coins.length; c++) {
        var tile = coins[c].tile;
        coins[c].setPosition(Vec.add(tile.center, Vec.scale(tile.normal, 8.0 * amount)));
      }
    }
  },
  
  WB.themes.updateHazards = function(elapsedTime, player) {
    //console.log(player.getPosition(), WB.gamePlayer.playerCharacterUp || WB.gamePlayer.playerIn || [0,1,0], WB.gamePlayer.human.height * 0.65);
    var avatar = this.avatar;
    if (!avatar) return;
    
    if (_.isEmpty(this.avatarData)) return;
    
    var playerCenter = Vec.displace(player.getPosition(), WB.gamePlayer.playerCharacterUp || WB.gamePlayer.playerIn || [0,1,0], WB.gamePlayer.human.height * 0.65);
    
    if (!WB.gamePlayer.human.getBoundingBox) {
      return;
    }
    
    var playerBox = WB.gamePlayer.human.getBoundingBox();
    
    if (this.state == WB.PUZZLE_STATE.PLAY) {
      if (avatar.bullets) {
        var bullets = this.avatarData.bullets;
        for (var b = 0; b < bullets.length; b++) {
          
          var bullet = bullets[b];
          if(!bullet.active) continue;
          
          var bulletCenter = Vec.displace(bullet.getPosition(), bullet.state.animUp, bullet.lift);
          var bulletLocal = player.getLocalPosition(bulletCenter);
          if (!bullet.state.invertFlag && playerBox.distanceToPoint(GFX.ArraytoV3(bulletLocal)) < bullet.radius) {

            
            /*
            var result = player.intersectRay(bulletCenter, Vec.sub(playerCenter, bulletCenter));
            if (result) {
              this.die();
            }*/
            
            WB.audioManager.playSynth({
              volume: WB.VOLUME * 12.0,
              frequency: 100,
              duration: 0.5
            });
            bullet.hit();
            this.die();
          } 
          
          if (bullet.state.tile.cleared) {
            //this.remove(bullet);
            bullets[b].fadeOut();
          }
          
        }
      }
      
      if (this.avatarData.coins) {
        var coins = this.avatarData.coins;
        for (var c = 0; c < coins.length; c++) {
          var coin = coins[c];
          var coinCenter = coin.center;
          var coinLocal = player.getLocalPosition(coinCenter);
          if (playerBox.distanceToPoint(GFX.ArraytoV3(coinLocal)) < coin.radius) {
            WB.overlay.factors.coins.factor += 1.0 / this.avatarData.numCoins;
            WB.overlay.updateMultiplier();
            var thiz = this;
            WB.audioManager.playSynth({
              frequency: this.baseNotes[4] * this.noteShift * 8.0,
              duration: 0.25,
              prepare: function(synth, start) {
                synth.duration = 0.25;
                synth.osc.frequency.setTargetAtTime(thiz.baseNotes[6] * thiz.noteShift * 8, start + 0.1, 4);
                synth.osc2.frequency.setTargetAtTime(thiz.baseNotes[6] * thiz.noteShift * 8, start + 0.1, 4);
                synth.humpTime = 0;
              },
              volume: WB.VOLUME * 8.0
            });
            
            this.remove(coin);
            this.avatarData.coins = _.without(coins, coin);
            return;
          }
        }
      }
          
  
      if (this.avatarData.urchins) {
        var urchins = this.avatarData.urchins;
        for (var u = 0; u < urchins.length; u++) {
          var urchin = urchins[u];
          if (player.distanceTo(urchin) < urchin.radius) {
            urchin.wiggle();
            this.die();
          }
        }
      }
    }
  };
  
  WB.themes.applyAvatar = function() {
    if (WB.overlay)
      delete WB.overlay.factors.slots;
    WB.themes.addHazards.call(this);
    WB.themes.addTileScenery.call(this, this.avatar.theme);
  },
  
  //Called on puzzle
  WB.themes.addTileScenery = function(theme) {
    
    if (!theme || !theme.scenery) {
      return;
    }
    
    this.sceneryGroup = new GFX.Group();
    this.add(this.sceneryGroup);
    
    var mode = WB.TILE_MODE || "mix";
    
    this.sceneryMaterial = WB.SceneryMaterial.clone();
    
    this.sceneryMaterial.uniforms.nearGradient.value.setHex(this.originalShader.uniforms.nearGradient.value.getHex());    
    this.sceneryMaterial.uniforms.farGradient.value.setHex(this.originalShader.uniforms.farGradient.value.getHex());   
    this.sceneryMaterial.uniforms.cameraDist.value = this.cameraDist;
    
    
    this.tileSceneries = [];
    
    var numGeos = theme.scenery.count;
    
    for (var t = 0; t < this.tiles.length; t++) {
      var tile = this.tiles[t];
      var topFlag = 0;
      
      if (tile.normal[1] < 0.5) {
        if (mode == 'mixTop')
          continue;
        if(mode == 'top')
          topFlag = -1;
      } else if (mode == 'top') {
        topFlag = 1;
      }
      
      var td;
      
      if ((topFlag==1) || (topFlag == 0 && Math.randomReal(0, 1.0) < 0.35)) {
        td = new GFX.Object3D({
          type: "collada",
          geometry: theme.sceneryGeometries.getGeometry("tile" + (t%numGeos)),
          material: this.sceneryMaterial
        });
      } else {
        td = new GFX.Object3D({
          type: "collada",
          geometry: theme.sceneryGeometries.getGeometry("disp" + (t%numGeos)),
          material: this.sceneryMaterial
        });
      }
            
      this.tileSceneries.push(td);
      
      tile.scenery = td;
      
      this.changeTileColor(tile, 0x000000);
      
      td.setPosition(tile.center);
      td.originalPosition = td.getPosition();
      
      var upAxis = Vec.multiply(
        Vec.sub([1,1,1], Vec.abs(tile.normal)), 
        [Math.randomReal(-1,1), Math.randomReal(-1,1), Math.randomReal(-1,1)]
      );
      
      //td.lookAt(Vec.add(tile.center, tile.normal), Vec.toAxis(Vec.cross(tile.normal,  [1,1,1])));
      td.lookAt(Vec.add(tile.center, tile.normal), Vec.toAxis(upAxis));
      //td.lookAt(Vec.add(tile.center, tile.normal), Vec.toAxis([Math.randomReal(-1,1), 0, Math.randomReal(-1,1)]));
      td.setScale(this.sideLength * 0.5);
      
      this.sceneryGroup.add(td);
    }
  }
  
  WB.trail = GFX.ParticleSystem.extend({ 
  
    particleCount: 100,
    particleSize: 0.02,
    particleOpacity: 0.6,
    particleColor: 0xff00ff,
    decayRate: 1,
    speed : 0.1,
    spread: 0.05,
    
    initParticles: function() {
      this.velocities = [];
      this.glObject.geometry.vertices.length = this.particleCount;
      this.velocities.length = this.particleCount;
      var pos = WB.gamePlayer.player.getWorldPosition();
      
      for (var i = 0; i < this.particleCount; i++) {
        this.velocities[i] = new THREE.Vector3();
        this.velocities[i].fromArray(Vec.scale(Vec.randomAngle(), this.speed));
        this.glObject.geometry.vertices[i] = new THREE.Vector3();
        this.glObject.geometry.vertices[i].fromArray(pos);
      }      
      
      this.setVisibility(false);
    },
    
    
    update: function(elapsedTime) {
      GFX.ParticleSystem.prototype.update.call(this, elapsedTime);

      var puz = WB.gamePlayer.activePuzzle;
      var pcUp = WB.gamePlayer.playerCharacterUp || [0,1,0];
      var spawning = 0;
      var playerPos = WB.gamePlayer.player.getWorldPosition();
      //if (puz.state == WB.PUZZLE_STATE.PLAY || puz.state == WB.PUZZLE_STATE.VICTORY) {
        spawning = 1;
        this.setVisibility(true);
      //}
      
      var vs = this.glObject.geometry.vertices;
      for (var i = 0, l = vs.length; i < l; i++) {
        vs[i].addScaledVector(this.velocities[i], elapsedTime * 1.5);
        if (spawning && Math.random() < elapsedTime * this.decayRate) {
          var p = Vec.displace(Vec.vary(playerPos, this.spread), pcUp, Math.random() * WB.gamePlayer.human.height * 0.5);
          vs[i].fromArray(p);
          if (this.goUp) {
            this.velocities[i].fromArray(Vec.scale(Vec.add(Vec.randomAngle(), pcUp), this.speed * 0.5));
          }
        }
        this.glObject.geometry.verticesNeedUpdate = true;
      }
      
      if (this.material && this.material.uniforms && this.material.uniforms.playerPos) {
        this.material.uniforms.playerPos.value.fromArray(WB.gamePlayer.player.getPosition());
      }
    }
  });
  
  
  WB.themes.applyTheme = function(avatar) {
    var theme = avatar.theme;
    if (WB.overlay) {
      WB.overlay.deleteFactor('coins');
    }
    
    if (avatar.bullets) {
      theme.bulletGeometry = theme.bulletGeometry || new GFX.GeometryLoader({
        src: avatar.bullets.src
      });
    }
    
    if (avatar.trail) {
      this.themeObjects.trail = new WB.trail(avatar.trail);
      this.effectsGroup.add(this.themeObjects.trail);
    }
    
    if (avatar.scatter) {
      var sctr = avatar.scatter;
      var material = WB.BasicMaterial.clone();
      material.uniforms.color.value.setHex(0xffffff);
      material.uniforms.ambient.value = 0.9;
      
      theme.scatterGeometries = theme.scatterGeometries || new GFX.GeometryLoader({
        src: sctr.src,
        geoRotate: [Math.PI * 0.5, 0, 0]
      });
      
      var sct = 0;
      var groupIndex = 0;
      var materials = [material];
      var groups = sctr.groups || [];
      
      for (var g = 0; g < groups.length; g++) {
        materials[g] = WB.BasicMaterial.clone();
        materials[g].uniforms.color.value.setHex(groups[g].color);
        materials[g].uniforms.gloss.value = sctr.gloss;
        materials[g].uniforms.specularColor.value.fromArray(sctr.specular);
        materials[g].uniforms.ambient.value = 0.9;
      }
      
      for (var s = 0; s < sctr.number; s++) {
        if (sctr.groups && sctr.groups[groupIndex]) {
          if (sctr.groups[groupIndex].number < s) {
            groupIndex++;
          }
        }
        
        var c = this.themeObjects["cloud"+s] = new GFX.Object3D({
          type: "collada",
          geometry: theme.scatterGeometries.getGeometry("cloud"+(s%sctr.count)),
          material: materials[groupIndex],
        });
      
        var pos = Vec.randomSphere(10.0, 20.0, sctr.bounds || 1.0);
        var ty = pos[1];
        pos[1] = pos[2];
        pos[2] = ty;     
        
        c.setPosition(pos);
        c.lookAt([0,0,0], [0,1,0]);
        c.setScale(sctr.scale || 1.0);
        this.themeGroup.add(c);
      }
    }

    if (avatar.scape) {
      var sctr = avatar.scape;
      var material = WB.BasicMaterial.clone();
      material.uniforms.color.value.setHex(0xdddddd);
      material.uniforms.ambient.value = 0.9;
      
      theme.scatterGeometries = theme.scatterGeometries || new GFX.GeometryLoader({
        src: sctr.src,
        //geoRotate: [Math.PI * 0.5, 0, 0]
      });
      
      var scape = this.themeObjects.scape = new GFX.Object3D({
        type: "collada",
        geometry: theme.scatterGeometries.getGeometry("cloud0"),
        material: material
      });
      scape.faceDirection([0, -1, 0], [1, 0, 0]);
      scape.setScale(1.2);
      this.themeGroup.add(scape);
 
    }
    
    if (avatar.flame) {
      this.themeObjects.flame = new WB.trail({
        particleCount: 500,
        particleSize: 15,
        decayRate: 13.0,
        spread: 0.03,
        speed: 0.2,
        goUp: 1,
				particleColor: 0xff0000,
        material : WB.FlameMaterial
      });
      this.effectsGroup.add(this.themeObjects.flame);

    }
    
    if (theme.colors && theme.colors.background2) {
      this.themeObjects.gradient = new GFX.Object3D({
        type: "icosahedron",
        shapeRadius: 1000,
        shapeSubdivisions: 1,
        material: WB.GradientMaterial.clone()
      });
      
      
      this.themeObjects.gradient.glObject.material.uniforms.loColor.value.fromArray(theme.colors.background);
      this.themeObjects.gradient.glObject.material.uniforms.hiColor.value.fromArray(theme.colors.background2);
            
      this.themeGroup.add(this.themeObjects.gradient);
    }
    
    if (theme.synthType1)
      WB.audioManager.synthType1 = theme.synthType1;
    if (theme.synthType2)
      WB.audioManager.synthType2 = theme.synthType2;
    if (theme.detune)
      WB.audioManager.synthDetune = theme.detune;
    if (theme.synth2Gain)
      WB.audioManager.synth2Gain = theme.synth2Gain;

    
    var thiz = this;
    
    if (theme.scenery && !theme.sceneryGeometries) {
      theme.sceneryGeometries = new GFX.GeometryLoader({
        src: theme.scenery.src
      });
    }
    
    
    if (theme.starfield) {
          
      thiz.themeObjects.starField = new GFX.StarField({
        color: 0x888888,
        opacity: 1.0,
        milkyway: 1,
        twinkle: 1,
        distance: 800,
        numParticles: 1000
      });
      
      thiz.themeObjects.starField2 = new GFX.StarField({
        color: 0xffffff,
        opacity: 1.0,
        milkyway: 1,
        twinkle: 1,
        distance: 400,
        numParticles: 50
      });
      
      thiz.themeGroup.add(thiz.themeObjects.starField);
      thiz.themeGroup.add(thiz.themeObjects.starField2);
    }

    if (theme.clouds) {
      thiz.themeObjects.cloudField = new WB.CloudSystem();
      thiz.themeObjects.cloudField.setEuler([0, 0.2, 0]);
      thiz.themeGroup.add(thiz.themeObjects.cloudField);
    }
    
    if (theme.ocean) {
      
      thiz.themeObjects.oceanBox = new GFX.Object3D({
        type: "box",
        shapeDimensions: [1000, 20, 1000],
        material: WB.OceanMaterial 
      });
       
      thiz.themeGroup.add(thiz.themeObjects.oceanBox);
      
      thiz.themeObjects.bubbles = new WB.Bubbles({
        object: thiz.player
      });
      
      thiz.effectsGroup.add(thiz.themeObjects.bubbles);
    }
  }
        
  WB.themes.addNoise = function(themeObjects, themeGroup) {
    //return;
    /*
    this.scene.skyBox = new GFX.Object3D({
      type: "box",
      shapeDimensions: [1000, 20, 1000],
      material: WB.OceanMaterial 
    });
    */
    
    var size = 0.8;
    
    //dbd5d5
    themeObjects.noiseField = new GFX.StarField({
      color: 0xdbdad5,
      opacity: 1.0,
      size: size,
      twinkle: 1,
      distance: 30,
      numParticles: 1000
    });
    
     themeObjects.noiseField2 = new GFX.StarField({
      color: 0xd4cdc6,
      opacity: 1.0,
      size: size * 2.0,
      twinkle: 2,
      distance: 100,
      numParticles: 2000
    });
    
     themeObjects.noiseField3 = new GFX.StarField({
      color: 0xcac4bd,
      opacity: 1.0,
      size: size * 5.0,
      twinkle: 3,
      distance: 300,
      numParticles: 2000
    });
    
    themeGroup.add(themeObjects.noiseField3);
    themeGroup.add(themeObjects.noiseField2);
    themeGroup.add(themeObjects.noiseField);
  }

  var tailWagTime = 0;
  WB.updateGameForAvatar = function(elapsedTime) {
    if (this.avatarData.jetpackTime != undefined) {
      this.updateJetpack(elapsedTime);
    }
    
    if (this.avatarData.bombTime != undefined) {
      this.updateBomb(elapsedTime);
    }
    
    if (this.avatarData.dashStartPos) {
      this.updateDash(elapsedTime);
    }
    
    var avatar = WB.user.get('activeAvatar');
    
    if (avatar.tail) {
      var et = elapsedTime * 8.0;
      if (WB.gamePlayer.activePuzzle.victory) {
        et *= 3.0;
      }
      
      tailWagTime += et;
      if (this.avatarData.tail) {
        this.avatarData.tail.rotation.set(0, 0, Math.sin(tailWagTime) * 0.5);
      }
    }
  }
  
}