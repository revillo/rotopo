var Visuals = function(WB) {
  'use strict'

  WB.CLIENT_VERSION = 1; 
  
  var GFX = GAME.GFX;
  var HIGH_INT = 99999999;

  var PLAYER_RADIUS = 0.002;
  var GAME_RATE = 1.0;
  var START_PUZZLE_INDEX = 0;
  
  WB.REMOVING_DUR = 2.2;
  WB.DEATH_DUR = 1.0;
  WB.VICTORY_DUR = 3.0;
  WB.POSE_DUR = 3.0;
  WB.CAMERA_DIST = 2.0;
   
  WB.JOIN_TILES = 1; // 1 
  WB.TEST_SHAPE = 0; // 0
  var SPEED = 0.9; // 0.9;
  
  var VOLUME = 0.01 // 0.01;
  WB.VOLUME = VOLUME;
  
  WB.GAME_ACTIVITY = Enum([
    'DEFAULT',
    'REMOVING',
  ]);
  
  WB.INVERTED_DIRECTIONS = {
    'UP' : 'DOWN',
    'DOWN' : 'UP',
    'LEFT' : 'RIGHT',
    'RIGHT' : 'LEFT'
  }
  
  WB.ACHIEVEMENTS = {
    'COMPLETIONIST' : {
      'ALL_EASY' : { text: "COMPLETE ALL EASY LEVELS"},
      'ALL_INTERMEDIATE' : { text: "COMPLETE ALL INTERMEDIATE LEVELS"},
      'ALL_ADVANCED' : { text: "COMPLETE ALL ADVANCED LEVELS"},
      'ALL_INSANE' : { text: "COMPLETE ALL INSANE LEVELS"},
      'ALL_AVATARS' : { text: "UNLOCK ALL AVATARS"},

      'PERFECT_EASY' : { text: "SET PERFECT RETRY RECORD ON ALL EASY LEVELS"},
      'PERFECT_INTERMEDIATE' : { text: "SET PERFECT RETRY RECORD ON ALL INTERMEDIATE LEVELS"},
      'PERFECT_ADVANCED' : { text: "SET PERFECT RETRY RECORD ON ALL ADVANCED LEVELS"},
      'PERFECT_INSANE' : { text: "SET PERFECT RETRY RECORD ON ALL INSANE LEVELS"},
      
      'MANY_GIMBLES' : { text : "GET 500,000 GIMBLES" }
    }
  }
  
  $(document).on('visibilitychange', function() {
    if(document.visibilityState == 'hidden') {
      if(WB.audioManager) {
        WB.audioManager.stopSynth();
        if (WB.gamePlayer) {
          var fcs = WB.state.get('focus');
        
          if (fcs == "menu") {
            WB.gamePlayer.pause();
          } else if (fcs == "gamePlayer"){
            WB.overlay.togglePauseMenu(true);
          }
        }
      }
    } else {
     if (WB.gamePlayer) {
      var fcs = WB.state.get('focus');
    
      if (fcs == "menu" && WB.gamePlayer && WB.gamePlayer.readyToGo && !WB.overlay) {
        WB.gamePlayer.unpause();
      } else if (fcs == "shop") {
        WB.overlay.playShopMusic();
      }
    }
    }
  });
 
  $(window).blur(function() {
    if (WB.gamePlayer) {
      var fcs = WB.state.get('focus');
      if (fcs == "gamePlayer"){
        WB.overlay.togglePauseMenu(true);
      }
    }
  });
  
  var ADS = {
    wrappers : [
      {
        wrapper_side: "/images/menu/amzn/wrapper_zippy_left.png",
        wrapper_top: "/images/menu/amzn/wrapper_zippy_top.png",
        label: "zippy_trexo recommends"
      },
      {
        wrapper_side: "/images/menu/amzn/wrapper_orby_left.png",
        wrapper_top: "/images/menu/amzn/wrapper_orby_top.png",
        label: "orby recommends"
      },
      {
        wrapper_side: "/images/menu/amzn/wrapper_andi_left.png",
        wrapper_top: "/images/menu/amzn/wrapper_andi_top.png",
        label: "andi_oxturn recommends"
      },
      {
        wrapper_side: "/images/menu/amzn/wrapper_queen_left.png",
        wrapper_top: "/images/menu/amzn/wrapper_queen_top.png",
        label: "queen_azelfi recommends"
      },
      {
        wrapper_side: "/images/menu/amzn/wrapper_duncan_left.png",
        wrapper_top: "/images/menu/amzn/wrapper_duncan_top.png",
        label: "duncan_plunder recommends"
      },
      {
        wrapper_side: "/images/menu/amzn/wrapper_pawlo_left.png",
        wrapper_top: "/images/menu/amzn/wrapper_pawlo_top.png",
        label: "pawlo recommends"
      }     
    ],
    
    amzn: [
      "//rcm-na.amazon-adsystem.com/e/cm?o=1&p=48&l=ur1&category=primemain&banner=097TDSG20MH99VMHNX82&f=ifr&lt1=_new&linkID=ca64ae43ee40bcd796b58f5306e2eedd&t=baanfell-20&tracking_id=baanfell-20",
      "//rcm-na.amazon-adsystem.com/e/cm?o=1&p=48&l=ur1&category=kuft&banner=19P4Y3M8FNN5JYX3PJ82&f=ifr&lt1=_new&linkID=db8e53f07fa7e6c70a67c2b5e966028d&t=baanfell-20&tracking_id=baanfell-20",
      "//rcm-na.amazon-adsystem.com/e/cm?o=1&p=48&l=ur1&category=primegift&banner=0DHKFFV9MZ6GTWBFZK82&f=ifr&lt1=_new&linkID=8b0a250281560e7ce5406d6bc7a34852&t=baanfell-20&tracking_id=baanfell-20",
      "//rcm-na.amazon-adsystem.com/e/cm?o=1&p=48&l=ur1&category=primemain&banner=1PREMK5A0BD4VB6F8Y02&f=ifr&lt1=_new&linkID=654d9d81d09a3b0355b28c89d98b7293&t=baanfell-20&tracking_id=baanfell-20",
      "//rcm-na.amazon-adsystem.com/e/cm?o=1&p=48&l=ur1&category=audible&banner=0JYEMDNC49A58GM3J902&f=ifr&lt1=_new&linkID=1b9de5658e1abb01406327932c411240&t=baanfell-20&tracking_id=baanfell-20",
      "//rcm-na.amazon-adsystem.com/e/cm?o=1&p=48&l=ur1&category=primeent&banner=13E898KKBGT2Q3TD6G02&f=ifr&lt1=_new&linkID=ff72e76c65f5b0345722fda1f84c7a73&t=baanfell-20&tracking_id=baanfell-20",
      "//rcm-na.amazon-adsystem.com/e/cm?o=1&p=48&l=ur1&category=student_usa&banner=0S7P6QRT4G5FFQ3HVX02&f=ifr&lt1=_new&linkID=4f2478010179d89daa2506ff519bf050&t=baanfell-20&tracking_id=baanfell-20"
    ]
  }
  
  WB.TEST_LEVEL = {
    "name" : "testpuzzle",
    "level_id" : -1,
    "directory" : "testdir",
    
    "puzzles" : [    
    
   { 
      "name": "dip",
      "s" : 0.7,
      "path" : "fs rd fs rs fu fd fu fs ls rs fu fd fu fs fs fu rd fd ld ru fs ru ls ls fs ru fs fu ld rd fd ld ru fu fs ls fs rs fs fu ls fs rs fs fu fd fu fs fs ls ru fs fu ld rd fd ld ru",
      "reward" : 30,
      "opts" : {
        "startTile" : 0,
        "startDir" : [0, 0, -1],
        "cameraType" : "follow",
        "playerSpeed" : 2.0,
        "cameraAngle" : 20
      }
   },
    ]
  };
    
  var audioManager = new GAME.AudioManager();
  WB.SAID_ROTOPO = 0;
  
  //Adjust master volume elsewhere...
  
  WB.audioManager = audioManager;
  audioManager.loadSounds({
      rotopo: {src: "/assets/sounds/rotopo", volume: 33 * VOLUME, callback: function() {
        if (WB.SAID_ROTOPO == -1)
          WB.audioManager.playSound('rotopo');
      }},
      new_record: {src: "/assets/sounds/new_record", volume: 33 * VOLUME},
  });

  
  WB.User = GAME.User.extend({
    //Anything?
  });
  
  WB.HumanObject = GFX.Object3D.extend({
    setArmsOut: function() {},
    setStanding: function() {}
  });
  
  WB.Menu = GAME.Views.View.extend({
    clientVersion: WB.CLIENT_VERSION,
    
    setup: function() {
            
      Handlebars.registerPartial('bknfr-footer', function() {
        return GAME.templates['bknfr-footer']();
      }());
      
      console.log(GAME.templates['console']());
      
      if (GAME.SYSTEM.MOBILE) {
        $('#view').addClass('mobile');
      }
      
      $(document).on('focus', '.button', function(e) {
        WB.$focusedButton = $($(this)[0]);
        //console.log($(this), e);
      });
      
      $('#view').on('mouseover', '.button', function() {
        /*
        WB.audioManager.playSynth({
          frequency: WB.audioManager.midiFreq(74),
          duration: 0.3,
          volume : WB.VOLUME * 5.0
        });
        */
      });
      
      $(window).resize(this.resizeBoxes);
      
      this.user = new GAME.User();
      WB.menu = this;
      
      WB.state = new GAME.State();
      WB.state.setFocus('menu');
      WB.user = this.user;      
      
      if (!GAME.GFX.WEBGL) {
        this.displayAlert({error: "NO_WEBGL"});
        return;
      }
      
      this.gamePlayer = new WB.GamePlayer({
        el: this.$el
      });
      
      WB.gamePlayer = this.gamePlayer;
  
      
      this.initUserData(this.content);
      
      if (!document.hasFocus()) {
        this.gamePlayer.pause();
      }
      
      var thiz = this;
      
      this.on('api-error', function() {
        thiz.displayAlert({error: "CONNECTION_ERROR"});
      });
    
      if (WB.TEST_SHAPE){  
        this.createMenu();
        this.play();
        this.gamePlayer.activity = WB.GAME_ACTIVITY.DEFAULT;
        this.gamePlayer.unpause();
        this.gamePlayer.readyToGo = 1;
        return;
      }
      
      WB.gamePlayer.screenSaver = true;
      var bgPuzzle = new WB.Puzzle({level: WB.TEST_LEVEL});
      bgPuzzle.makePuzzle(0);
      this.gamePlayer.showScreenSaver(bgPuzzle); 
      this.gamePlayer.unpause();
      this.gamePlayer.readyToGo = 1;

      if (this.signedIn) {
        this.createMenu();
        this.swapOutTryButton();
      } else {
        this.user.on('status-change', this.handleNetworkStatus);
        this.createMenu();
        thiz.$('.button.fblogin').css('opacity', 0.5);
        
        this.user.connectFacebook(this.networkKeys.facebookAppID, function(error) {
          if (error) {
            thiz.displayAlert(error);
            return;
          }
          
          thiz.$('.button.fblogin').css('opacity', 1.0);
        });
        
        this.user.connectGoogle(thiz.networkKeys.googleAppID, thiz.networkKeys.googleClientID, thiz.$('.button.gglogin')[0], function(error) {
          if (error) {
            thiz.displayAlert(error);
            return;
          }
          
          //thiz.$('.button.gglogin').css('opacity', 1.0);
        });
                
      }
      
      if(WB.audioManager.hasSound('rotopo').buffer) {
        WB.SAID_ROTOPO = 1;
        WB.audioManager.playSound('rotopo');
      } else {
        WB.SAID_ROTOPO = -1;
      }
    },
    
    buttonTabbing: function() {
      $('.button').attr('tabindex', 0);
    },
    
    showBadges: function() {
      var allAvatars = this.gameInfo.numAvatars + 1 == _.size(WB.user.get('avatars'));
      var manyGimbles = WB.user.get('gimbles') >= 500000;
      var levelHistory = WB.user.get('levelHistory');
      var levels = WB.user.get('levels');
      
      var completed = {
        easy: 0,
        intermediate: 0,
        advanced: 0,
        insane: 0
      }
      
      var perfected = _.clone(completed);
      
      for (var level_id in levelHistory) {
        var level = levels[level_id];
        if (levelHistory[level_id].completed >= level.puzzles.length) {
          completed[level.difficulty]++;
        }
        
        if (levelHistory[level_id].retry_record == 0) {
          perfected[level.difficulty]++;
        }
      }
      
      this.displayAlert({type: "ACHIEVEMENTS"});
      
      var counts = this.gameInfo.levelCounts;
      
      if(allAvatars) $('.badge.ALL_AVATARS').addClass('done');
      if(completed.easy == counts.easy) $('.badge.ALL_EASY').addClass('done');
      if(completed.intermediate == counts.intermediate) $('.badge.ALL_INTERMEDIATE').addClass('done');
      if(completed.advanced == counts.advanced) $('.badge.ALL_ADVANCED').addClass('done');
      if(completed.insane == counts.advanced) $('.badge.ALL_INSANE').addClass('done');            
      if(manyGimbles) $(".badge.MANY_GIMBLES").addClass('done');
    },
    
    reloadMenu: function() {
      WB.overlay.togglePauseMenu(true);
      WB.state.pushFocus('menu');
      
      this.$menu.show();
      this.createMenu();
            
      if (this.signedIn) {
        this.swapOutTryButton();
      }
      
      $('#overlay').hide();
      this.$('.button.try').html("continue roTopo");
      this.$('.button.try').unbind();
      var thiz = this;
      this.$('.button.try').click(function() {
        WB.state.popFocus();
        thiz.$menu.hide();
        $('#overlay').show();
      });

    },
    
    swapOutTryButton: function() {
      this.$('#menu .button.try').html("continue roTopo");
      this.$('#menu .button.back').html("continue roTopo");
      this.$('#menu .auth-buttons').remove();
      this.$('#menu .button.logout').show();
      this.$('.button.try').unbind();
      this.$('.button.try').click(this.play);
      this.$('#menu .info').html("<user>User: " + WB.user.get('displayName') + " </user>");
      
      this.resizeBoxes();
    },
   
    handleNetworkStatus: function() {
      if (this.user.network) {
        this.login();
      }
    },
    
    logout: function() {
      var thiz = this;
      
      /*
      var clearFBCookie = function() {
        function delete_cookie(name) {
          document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
        }

        var cookies = document.cookie.split(";");
        for (var i = 0; i < cookies.length; i++)
        {
          if(cookies[i].split("=")[0].indexOf("fblo_") != -1)
            delete_cookie(cookies[i].split("=")[0]);
        }
      }*/
      
      var restart = function(url) {
        thiz.getAPI('/api/logout', {}, function(data) {
          if(url) location = url;
          else
            location.reload();
        })
      }
      
      if(WB.menu.content.network == 'facebook') {
        this.user.connectFacebook(this.networkKeys.facebookAppID, function() {
          
          FB.getLoginStatus(function(response) {
            if (response.status != "connected") {
              restart();
              return;
            }
            
            FB.logout(function(response2) {
              restart("https://www.facebook.com/logout.php?next=" + location.origin + "&access_token=" + response2.authResponse.accessToken);
            })
          })
          
        });
      } else if (WB.menu.content.network == 'google') {
        restart();
      }

    },
    
    login: function() {
      var loginData = this.user.getCredentials();
      var data = {};
      
      if (WB.user.get('points')) {
        data.points = WB.user.get('points');
      }
      
      if (WB.user.get('levelHistory')) {
        data.levelHistory = JSON.stringify(WB.user.get('levelHistory'));
      }
      
      this.postAPI('/api/login', {credentials: loginData, data: data}, this.handleLoginResponse);
    },
    
    handleLoginResponse: function(response) {
      if (response.success) {
        this.initUserData(response.content || {});
        this.swapOutTryButton();
        this.signedIn = true;

        //this.$('.button.play').show();
        //this.$('.button.play').click(this.play);
      } else {
        this.displayAlert(response);
      }
    },
    
    
    initUserData: function(content, shopFlag) {
      this.content = content;
      var user_id = content.user_id || -1;
      
      if (user_id < 0) {
        var storedPoints = Number(GAME.storage.getItem("points_" + user_id));
        
        if (storedPoints) {
          content.points = storedPoints;
        }
        
        var storedHistory = GAME.storage.getItem(user_id + "_levelHistory");
        if (storedHistory) {
          content.levelHistory = JSON.parse(storedHistory);
        }
      }
      
      try {
        this.storedSettings = JSON.parse(GAME.storage.getItem(user_id + '_settings'));
        this.storedAvatarName = GAME.storage.getItem(user_id + '_avatar');
      } catch(e) {}
      
      
      if (content.signedIn)
        this.signedIn = content.signedIn;
      
      if (content.network && content.network_id) {
        var network = content.network;
        network = network[0].toUpperCase() + network.slice(1);
        this.user.set('displayName', network + "(****" + content.network_id.substr(content.network_id.length - 4) + ")");
      }
      this.user.set('levels', content.levels || this.content.levels);
      this.user.set('levelHistory', content.levelHistory || {});
      this.user.set('purchases', content.purchases || {});
      this.user.set('avatars', content.avatars || {});
      this.user.set('user_id', user_id);
      this.user.set('points', content.points || 0);

      var avatars = this.user.get('avatars');
      //avatars['default'] = {name:'default', color: [0.4196, 0.61568, 0.7372]}
      //avatars['default'] = {name:'default', color: Color255([69, 48, 72])}
      avatars['sorghum'] = {name:'sorghum', type: 'default', theme: {defaultNoise: 1}, material: { color: Color255([69, 48, 72]) }};
      //avatars['default'] = {name:'default', color: Color255([255, 255, 255])}
      
      if(!shopFlag) {
        if (this.storedAvatarName && avatars[this.storedAvatarName])
          WB.gamePlayer.changeAvatar(avatars[this.storedAvatarName]);
        else 
          WB.gamePlayer.changeAvatar(avatars['sorghum']);
      }
      
      if (this.storedSettings) {
        var toggle = this.storedSettings.music;
        if (toggle == 'on') {
          WB.audioManager.enableSounds();
        } else if(toggle == 'off') {
          WB.audioManager.disableSounds();
        }
      }
      
    },
    

    displayAlert: function(_alert) {
      var $alert;
      
      if(WB.gamePlayer && WB.gamePlayer.hasFocus()) {
        WB.overlay.togglePauseMenu(true);
      }
      
      if (!_alert.type) {
        _alert.type == 'ERROR - ' + _alert.error;
      }
      
      if (_alert.type == "CONFIRM_RESTART") {
        $alert = $(GAME.templates['confirm-restart']());
      } else if (_alert.type == "TUTORIAL") { 
        var n = _alert.number;
        $('.glow').removeClass('glow');
        
        if (n == 0) 
          WB.audioManager.loadSounds({
            hello: {src: "/assets/sounds/hello", volume: 33 * VOLUME, callback: function() {WB.audioManager.playSound('hello')}},
          });
        else if (n == 1) 
          $('.game.line.levels').addClass('glow');
        else if (n == 2)
          $('#level-menu').addClass('glow');
        else if (n == 3) 
          $('#multiplier').addClass('glow');
        else if (n == 4)
          $('.tasks').addClass('glow');
        else if (n == 5) {
          $('.game.line.gimbles .gimble-count').addClass('glow');
          $('.game.line.gimbles .button.shop').addClass('glow');
        }
        
        if (n == 7) {
          return;
        }
        
        $alert = $(GAME.templates['tutorial-' + _alert.number]());
     
      } else if (_alert.type == "MANUAL") {
        $alert = $(GAME.templates['manual']());
      }else if (_alert.type == "SHOP_REMINDER") {
        $alert = $(GAME.templates['remind-shop']());
      } else if (_alert.type == 'CONTACT') {
        $alert = $(GAME.templates["contact"]());
      } else if (_alert.type == "SETTINGS") {
        $alert = $(GAME.templates["box-settings"]());
        WB.overlay.drawOptions($alert);
      } else if (_alert.type == "LEVEL_OVERVIEW") {
        $alert = $(GAME.templates["level-overview"](_alert.level));
      } else if (_alert.type == "ACHIEVEMENTS") {
        $alert = $(GAME.templates["achievements"](WB.ACHIEVEMENTS));
      } else {
        switch (_alert.error) { 
          case "NETWORK_ERROR":
            $alert = $(GAME.templates["alert-network"](_alert));
          break;
          case "INVALID_CREDENTIALS":
            $alert = $(GAME.templates["alert-invalid"](_alert));
          break;
          case "CONNECTION_ERROR":
            $alert = $(GAME.templates["alert-connection"](_alert));
          break;
          case "INSUFFICIENT_FUNDS":
            $alert = $(GAME.templates["alert-insufficient-funds"](_alert));
          break;
          case "NO_WEBGL":
            $alert = $(GAME.templates["alert-no-webgl"](_alert));
          break;
          default:
            $alert = $(GAME.templates["alert-other"](_alert));
          break;
          case "BRAINTREE_OFFLINE":
            $alert = $(GAME.templates["alert-braintree"](_alert));
          break;
        }
      }
      
      var windo = WB.state.get('window') || {
        height: $(window).height(),
        width: $(window).width()
      }
      
      if (windo.height > windo.width) {
        $alert.addClass('vertical');
      } else {
        $alert.addClass('horizontal');
      }
      
      $('#alerter').append($alert);
      
      if (_alert.type == "LEVEL_OVERVIEW" && _alert.level.video) {
        WB.overlay.loadYoutubeAPI();
      }
      
      WB.state.pushFocus(_alert.type);
            
      $('#alerter .button.reload').click(function() {
        location.reload();
      });
        
      var removeAlert = function() {
        $alert.remove();
        if (WB.overlay)
          WB.overlay.stopAdBonus();
        if ($('.alert').length == 0) {
          $('#alerter').hide();
          WB.state.popFocus();
        }
      }
      
      if (_alert.type == "TUTORIAL") {
        $('#alerter .button.next').click(function() {
          removeAlert();
          _alert.number++;
          WB.menu.displayAlert(_alert);
        });
        
        
        $('#alerter .button.shop').click(function() {
          removeAlert();
          WB.overlay.loadShop(function() {
            _alert.number++;
            WB.menu.displayAlert(_alert);
          });
        });
      }
      
      $('#alerter .button.menu-return').click(function() {
        removeAlert();
        WB.menu.reloadMenu();
      });
      
      /*
      $('#alerter .button.shop').click(function() {
        removeAlert();
        WB.overlay.loadShop();
      });
      */

      $('#alerter .play-level').click(function() {
        removeAlert();
        var level_id = $(this).attr("level_id");
        WB.overlay.playLevel(level_id);
      });
      
      $('#alerter .button.ok').click(function() {
        removeAlert();
      });
      
      $('#alerter .button.restart').click(function() {
        removeAlert();
        WB.gamePlayer.restartLevel();
      });
      
      $('#alerter .button').attr('tabindex', 0);
      $('#alerter .button').addClass('mob-btn', 0);
      
      $('#alerter').show();
      this.resizeBoxes();

      return $alert;
    },
    
    play: function() {
      var settings = {
        antialiasing: 'low',
        music: 'on',
        camera: 'auto',
        fullscreen: 'off',
        controls: 'default'
      };
      
      if (this.storedSettings) {
        try {
          for (var key in this.storedSettings) {
            settings[key] = this.storedSettings[key];
          }
        } catch(error) {
          
        }
      }
      var thiz = this;
      
      if (!this.overlay) {
        this.overlay = new WB.Overlay({
          el: this.$('#overlay'),
          settings: settings
        });
        
        WB.overlay = this.overlay;
        
        this.overlay.on('api-error', function() {
          thiz.displayAlert({error: "CONNECTION_ERROR"});
        });
      } else {
        $('#overlay').show();
        this.overlay.updateSettings(settings);
        this.overlay.togglePauseMenu(false);
      }
      
      this.$menu.hide();
      
      WB.state.setFocus('gamePlayer');
      WB.gamePlayer.unpause();
      
      
      if (WB.TEST_SHAPE) {
        this.gamePlayer.playLevel(WB.TEST_LEVEL);
        this.gamePlayer.activePuzzle.state = WB.PUZZLE_STATE.PLAY;
      } else {
        //this.gamePlayer.removeActivePuzzle('pass');
        var firstLevelID = "1";
        var firstPuzzleID = 0;
        var data = {};
        
        if(this.content.lastSave) {
          var save = this.content.lastSave;
          var level = this.content.levels[save.level_id];
          var puzzleIndex = Number(save.puzzle_index) + 1;
          
          if (save.winloss) {
            WB.overlay.factors.winloss.factor = Number(save.winloss);
          }
          
          if (puzzleIndex >= level.puzzles.length) {
            WB.overlay.shuffle();
            return;
          }
          
          if (level) {
            firstLevelID = save.level_id;
            firstPuzzleID = puzzleIndex;
            data = {
              retries: save.retry_record,
              time: save.time_record,
              turns: save.turn_record
            }
          }
        } 
        
        this.gamePlayer.playLevel(this.content.levels[firstLevelID], firstPuzzleID, data);

        this.overlay.shuffleList = _.reject(this.overlay.shuffleList, function(level) {
          return level.level_id == firstLevelID || level.level_id == Number(firstLevelID);
        });
      }
    },
    
    showLoginMenu: function() {
      this.$menu.html(
        GAME.templates['login-menu']()
      );
      this.$menu.show();
      this.$menu.css({
        marginTop: ''
      });
      var thiz = this;
      $('#overlay .fade').show();

      this.$('.button.back').click(function() {
        thiz.$menu.hide();
        $('#overlay .fade').hide();
      });
      
      this.$('.button.fblogin').click(function() {
        thiz.user.loginToFacebook();
      });
      
      /*
      this.$('.button.gglogin').click(function() {
        thiz.user.loginToGoogle(thiz.networkKeys.googleClientID);
      });*/
      
      this.resizeBoxes();
    },
    
    resizeBoxes: function() {
      var windowWidth = $(window).width();
      var windowHeight = $(window).height();
      
      var fontSize = 1.0;
      
      if (windowHeight > windowWidth) {
      
        fontSize = windowHeight / 1080;
        fontSize = Math.min(fontSize, windowWidth / 721);
        
        if (fontSize > 0.85 && fontSize < 1.1) {
          fontSize = 1.0;
        }
        
        
        if (fontSize > 1.0) {
          fontSize = 1.0 + (0.8 * (fontSize - 1.0));
        }
              
      } else {
        fontSize = windowWidth / 1900;
        fontSize = Math.min(fontSize, windowHeight / 300);
        if (fontSize > 0.85 && fontSize < 1.1) {
          fontSize = 1.0;
        }
        
        if (fontSize < 1.0) {
          
          fontSize = 1.0 - (0.6 * (1.0-fontSize))
        }
      }
        
      WB.state.set('window', {fontSize: fontSize, width: windowWidth, height: windowHeight, ar: windowWidth/windowHeight});

      
      $('#view').css('font-size', (fontSize*12) + "pt");
      
      var $youtube = $('.youtube');
      
      $youtube.attr('width', $youtube.parent().width());
      $youtube.attr('height', $youtube.parent().height());
      
      return;
      /*
      var boxes = $('.box');
      
      for (var b = 0; b < boxes.length; b++) {
        var $box = $(boxes[b]);
        $box.css({
          'margin-top': $box.outerHeight() * -0.5
        });
      }
       */
    },
    
    createMenu: function(error) {
      $('.loading-text').remove();
      this.$menu = this.$('#menu');
      
      this.$menu.html(
        GAME.templates['menu'](GAME.SYSTEM)
      );
      
      this.$menu.css({
        marginTop: '-17em'
      });
      
      var thiz = this;
      
      //this.showAd();
      
      this.$('.button.try').click(this.play);
      
      this.$('.button.fblogin').click(function() {
        thiz.user.loginToFacebook();
      });
      
      /*
      this.$('.button.gglogin').click(function() {
        thiz.user.loginToGoogle(thiz.networkKeys.googleClientID);
      });
      */
      
      this.$('.button.logout').click(function() {
        thiz.logout();
      });
      
      if (error) {
        this.displayAlert({error: "NETWORK_ERROR"});
      }
      
      this.resizeBoxes();
      
      _.delay(function() {
        $('.ad').css({
          display: '',
          opacity: '',
          visibility: '',
          backgroundPosition: ''
        });
      }, 300);
    },
    
    showAd: function() {
      var wrapper = _.sample(ADS.wrappers);
      var amzn_url = _.sample(ADS.amzn);
      
      var wl = $("<div class='wrap_left'></div>");
      var wt = $("<div class='wrap_top'></div>");
      wt.html(wrapper.label);
      var unit = $("<div class='unit'></div>");
      wl.css({
        'background-image': "url(" + wrapper.wrapper_side + ")"
      });
      
      wt.css({
        'background-image': "url(" + wrapper.wrapper_top + ")"
      });
      
      unit.html('<iframe src="' + amzn_url + '" width="728" height="90" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>');
      
      $('#menu-ad').append(wl);
      $('#menu-ad').append(wt);
      $('#menu-ad').append(unit);
      
      unit.click(function(e) {
        if(e.target == this) {
          window.open("https://www.amazon.com/dp/B00DBYBNEE?_encoding=UTF8&adid=0WC028HAZX3RDJT8X2C6&camp=0&creative=0&linkCode=ur1&primeCampaignId=prime_assoc_ft&tag=baanfell-20");
        }
      });
    }
  });
  
  
  
  
  WB.GamePlayer = GAME.Views.UI.extend({
    
    showScreenSaver: function(puzzle) {
      this.screenSaver = true;
      //this.player.setVisibility(false);
      this.introduceNextPuzzle(puzzle);
    },
    
    restartLevel: function() {
      var user_id = WB.user.get('user_id');
      var level = this.activePuzzle.level;
      GAME.storage.removeItem(user_id + "_levelsave_" + level.level_id);
      if (this.activePuzzle) {
        this.playLevel(level);
      }
      WB.overlay.togglePauseMenu(false);
    },
    
    setActiveLevelData: function(data, level_id) {
      data = data || {};
      this.activeLevelData = {
        retries: data.retries || 0,
        time: data.time || 0,
        turns: data.turns || 0,
        level_id: level_id
      }
    },
    
    playLevel: function(level, puzzleIndex, data) {
      data = data || {};
      if (WB.menu.content.other && WB.menu.content.other.login_days) {
        WB.overlay.setFactor('login_days', {type: 'add', reason: "logged in today", factor: 0.5, expires: GAME.now() + (10 * GAME.MINUTES)});
      }
      $('#overlay .gimbles .tasks .line-section').removeClass('animating');
      
      var levelHistory = WB.user.get('levelHistory');
      levelHistory[level.level_id] = levelHistory[level.level_id] || {completed: 0, retry_record: HIGH_INT, time_record: HIGH_INT, turn_record: HIGH_INT};
      
      var user_id = WB.user.get('user_id');
      
      if(!WB.EDITOR) {
      
        try {
          var levelSave = GAME.storage.getItem(user_id + "_levelsave_" + level.level_id);
          
          if (levelSave) {
            var saveData = JSON.parse(levelSave);
            puzzleIndex = saveData.puzzle_index;
            data = saveData;
          }
        } catch(e) {console.log(e)}
        
      }
      
      var pass = 0;
      if (this.activePuzzle) {
        if (this.screenSaver || this.activePuzzle.completed) {
          if (this.screenSaver) {
            this.activePuzzle.fail();
          }
          this.removeActivePuzzle('pass');
          pass = 1;
        } else {
          this.removeActivePuzzle('shuffle');
          this.activePuzzle.fail();
        }
      }
      
      this.screenSaver = false;
      
      
      this.setActiveLevelData(data, level.level_id);
      
      this.puzzleIndex = (puzzleIndex % level.puzzles.length) || START_PUZZLE_INDEX;
      this.checkPoint = this.puzzleIndex;
      this.level = level;
      var puzzle = this.prepareNextPuzzle();
      this.introduceNextPuzzle(puzzle);
      if (pass)
        puzzle.setScale(0.0001);      
    },
    
    setup: function() {

      this.scene = new GFX.Scene3D({
        el : this.$el,
        updateFn : this.update,
        backgroundColor: WB.COLOR_THEME.backgroundHex
      });
      
      var scene = this.scene;
      
      /*
      this.scene.oldSetCamera = this.scene.setCamera;

      this.scene.setCamera = function(pos, target, up) {
        scene.oldSetCamera(pos, target, up);
        audioManager.orientListener(pos, target, up);
      }*/
      
      this.camera = new WB.GimbleCamera({
        controller: this,
        scene: this.scene
      });
      
      this.themeGroup = new GFX.Group();
      this.scene.add(this.themeGroup);
      
      this.checkPoint = this.puzzleIndex;
      
      this.puzzleGroup = new GFX.Group();
      this.scene.add(this.puzzleGroup);
      this.createPlayer();
      
      this.effectsGroup = new GFX.Group();
      this.scene.add(this.effectsGroup);
      
      this.rampUpSpeed = 1.0;
      
    },
    
    createPlayer: function() {
         
      this.player = new GFX.Group();
        
      /*
      human = new GFX.Humanoid({
        material: WB.BasicMaterial,	
      });
      
      human.setPosition([0, 0.0, 0]);
      this.player.add(human);
      this.human = human;
      */
            
    },
    
    loadAvatar: function(avatar) {
      var result;
      
      var material = WB.BasicMaterial.clone();

       if (avatar.material) {
        material.uniforms.color.value.fromArray(avatar.material.color);
        material.uniforms.specularColor.value.fromArray(avatar.material.specularColor || [0,0,0]);
        material.uniforms.gloss.value = (avatar.material.gloss) || 0;
        material.uniforms.ambient.value = (avatar.material.ambient) || 0.8;
      }

      
      if (avatar.type == 'humanoid' || avatar.type == 'default') {   
        result = new GFX.Humanoid(_.extend(avatar.params || {}, {material: material}));
        
        if (avatar.type == 'default') {
          
          result.armTurnOut = 0.3;
          result.legTurnOut = 0.05;
          result.handDistance = 1.2;
          result.material.uniforms.ambient.value = 0.7;
          
          var hatMaterial = WB.BasicMaterial.clone();
          hatMaterial.uniforms.color.value.setHex(0x618dbc);
          hatMaterial.uniforms.dissolve = material.uniforms.dissolve;
          
          var hatGeometry = GFX.makePyramidGeometry(0.046, 12, 0.039);
          var scale = 1.0;
           GFX.transformGeometry({
            euler: [0.55, 0, 0],
            translate: [0,0.0,0.009],
            scale: [scale, scale, scale],
            geometry: hatGeometry
          });
          
          var hat = new GFX.Object3D({
            geometry: hatGeometry,
            material: hatMaterial
          });

          result.bodyMeshes.head.add(hat);
        }
        
      } else {
        result = new WB.HumanObject(_.extend(avatar.params || {}, {material: material}));        
      }
      
      return result;
    },
    
    changeAvatar: function(avatar) {
      if (!avatar) return;
  
      WB.user.set('activeAvatar', avatar);
      
      
      if (this.human) {
        this.player.remove(this.human);
      }
      
      if (!avatar.preloaded) {
        avatar.preloaded = this.loadAvatar(avatar);
      }
      
      avatar.lift = avatar.lift || 0;
      
      if (this.themeObjects) {
        for (var obj in this.themeObjects) {
          this.themeGroup.remove(this.themeObjects[obj]);
          this.effectsGroup.remove(this.themeObjects[obj]);
          //TODO Clear Object data
          this.themeObjects[obj].destroy();
        }
      }
 
      //Apply defaults
      this.themeColors = WB.COLOR_THEME;
      WB.audioManager.synthType1 = "triangle";
      WB.audioManager.synthType2 = "sine";
      WB.audioManager.synthDetune = 5;
      WB.audioManager.synth2Gain = 0.5;
      
      if (this.$cssThemeLink) {
        this.$cssThemeLink.remove();
      }
      
      if (avatar.theme) {
        this.themeObjects = {};
        
        
        if (avatar.theme.defaultNoise) {
          WB.themes.addNoise(this.themeObjects, this.themeGroup);
        }
        
        if (avatar.theme.colors)
          this.themeColors = avatar.theme.colors;
          
        WB.themes.applyTheme.call(this, avatar);
        
        if (avatar.theme.stylesheet) {
          this.$cssThemeLink = $('<link rel="stylesheet" type="text/css" />');
          this.$cssThemeLink.attr('href', '/stylesheets/avatar_styles/' + avatar.theme.stylesheet + '.css');
          $('head').append(this.$cssThemeLink);
        } 
      }
      
      this.updateTheme();
      this.human = avatar.preloaded;
      this.player.add(this.human);
      
      
      if (avatar.lift) {
        this.human.setPosition([0,0, avatar.lift]);
      }
    },
    
    updateTheme: function() {
      var tc = this.themeColors;
      
      this.scene.setBackgroundColor(tc.background);
      WB.MiniMeMaterial.uniforms.color.value.setColor(tc.colored);
      
      WB.TileMaterial.uniforms.coloredColor.value.setColor(tc.colored);
      WB.TileMaterial.uniforms.ghostColor.value.setColor(tc.ghost);
      WB.TileMaterial.uniforms.ghostAlpha.value = tc.ghostAlpha;
      WB.TileMaterial.uniforms.style.value = tc.style || 0;
      WB.TileMaterial.uniforms.AOEffect.value = tc.AOEffect || 1.0;
     
      WB.TileMaterial.uniforms.nearGradient.value.setColor(tc.nearGradient || tc.colored);
      WB.TileMaterial.uniforms.farGradient.value.setColor(tc.farGradient || tc.colored);
      
      WB.SceneryMaterial.uniforms.style.value = tc.scenery_style || 0;
      if (tc.tileTexture) {
        GFX.textureLoader.load(tc.tileTexture, function(texture) {
          WB.TileMaterial.uniforms.tex.value = texture;
          WB.gamePlayer.activePuzzle.updateTileTexture(texture);
        });
      } else {
        WB.TileMaterial.uniforms.tex.value = undefined;
      }
    },
    
    updateCamera: function(elapsedTime) {
      this.camera.gameUpdate(elapsedTime);
    },
    
    prepareNextPuzzle: function() {
      if (this.flagUpdateAvatar) {
        this.changeAvatar(this.flagUpdateAvatar);
        delete this.flagUpdateAvatar;
        if(this.removingMode == 'fail')
          this.removingMode = 'shuffle';
        this.lastPuzzleTileData = null;
      }
      
      var puzzle = new WB.Puzzle({level: this.level, player: this.player});
      
      puzzle.makePuzzle(this.puzzleIndex, this.lastPuzzleStartData);
      
      puzzle.updateForLastPuzzle(this.lastPuzzleTileData);

      return puzzle;
    },
    
    introduceNextPuzzle: function(puzzle) {
      
      var addToBeginning = this.removingMode == 'pass';
      
      this.puzzleGroup.add(puzzle, addToBeginning);
      puzzle.setPosition([0,0,0]);
      this.activePuzzle = puzzle;
      if (this.screenSaver) return;
      
      WB.state.set('currentPuzzle', puzzle);
     
      var tile = puzzle.tiles[puzzle.startTile];
      var startPos = Vec.add(tile.center, Vec.setLength(tile.normal, PLAYER_RADIUS));
      startPos = Vec.add(startPos, Vec.scale(puzzle.startDir, tile.sideLength * -0.4));
      puzzle.startPos = startPos;
      
      if (this.removingMode != 'fail') {
        this.player.setPosition(startPos);
      }
      
      //this.human.glObject.material.uniforms.dissolve.value = 0;
      this.playerDir = _.clone(puzzle.startDir);
      this.playerIn = Vec.invert(tile.normal);
      this.playerForward = _.clone(this.playerDir);
      this.playerRight = Vec.toAxis(Vec.perpendicular(this.playerDir, tile.normal));
      this.playerCharacterUp = Vec.invert(this.playerIn);
      puzzle.startNormal = Vec.invert(this.playerIn);

      this.player.lookAt(Vec.add(this.player.getPosition(), this.playerCharacterUp), this.playerForward); 
      
      this.playerCharacterDir = undefined;
      
      this.playerTile = tile;

      if (this.removingMode == 'fail') {
        this.activePuzzle.explode(1.0);
      }
      
      this.human.armsUp = 0;
      
      //this.human.setStanding();
      this.human.setArmsOut();
      
      this.direction = 'UP';
      
      this.setupAvatar();
      
      this.updateRemovingPuzzle(0);
    },
    
    setupAvatar: function() {
      this.avatarData = {};
      var avatar = WB.user.get('activeAvatar');
      this.human.setPosition([0,0, avatar.lift || 0]);
      
      //TODO What if not loaded?
      if (!this.human.loaded) return;

      if (avatar.jetpack) {
        this.avatarData.jetpack = this.human.bodyMeshes.body.glObject.children[0];
        this.avatarData.jetpack.visible = false;
        this.avatarData.jetpack.material = WB.jetpackFireMaterial;
      }
      
      if (avatar.tail) {
        this.avatarData.tail = this.human.bodyMeshes.body.glObject.children[0];
      }
    },
 
    removeActivePuzzle: function(mode) {
      if (this.removingPuzzle) {
        this.destroyPuzzle(this.removingPuzzle);
        this.removingPuzzle = undefined;
      }
      
      
      if (this.activePuzzle) {
        this.cleanAvatarData();
        this.activePuzzle.cube.remove(this.player);
        this.removingPuzzle = this.activePuzzle;
        this.player.remove(this.activePuzzle.miniMe);

        if (mode == 'fail') {
          this.lastPuzzleTileData = this.activePuzzle.tiles;
          this.lastPuzzleStartData = {
            startTile: this.activePuzzle.startTile,
            startDir: this.activePuzzle.startDir
          }
        } else {
          this.lastPuzzleTileData = null;
          this.lastPuzzleStartData = null;
        }
      
      } else {
        this.removingPuzzle = 1;
      }
      
      this.removingAmount = 0.0;
      this.removingTick = 0;
      this.removingMode = mode;
      
      this.camera.saveRemovingState();

    
      
      this.activity = WB.GAME_ACTIVITY.REMOVING; 


    },
    
    destroyPuzzle: function(puzzle) {
      puzzle.cube.remove(this.player);
      this.puzzleGroup.remove(puzzle);
      puzzle.clear(true);
    },
    
    handleTileCleared: function() {
      for (var t in this.themeObjects) {
        if (this.themeObjects[t].burst) {
          this.themeObjects[t].burst();
        }
      }
    },
    
    debug: function() {
      var vecs = [this.camera.cameraPos, 
        this.camera.cameraUp, 
        this.player.getPosition(), 
        this.playerCharacterUp, 
        this.playerIn,
        this.playerForward,
        this.playerDir,
        this.playerRight];
      
      for (var v = 0; v < vecs.length; v++) {
        if (!vecs[v]) {
          //console.log(v);
          continue;
        }
        if (isNaN(vecs[v][0])) {
          console.log("ERROR for Vec", v);
          WB.menu.displayAlert({error: "PROGRAM ERROR", message: "Whoops, roTopo has crashed! Try reloading the page."});
          //barf();
        }          
      }
    },
    
    quickSwapNextPuzzle: function() {
      this.puzzleIndex++;
      this.destroyPuzzle(this.activePuzzle);
      var puzzle = this.prepareNextPuzzle();
      this.introduceNextPuzzle(puzzle);
      this.activePuzzle.inspect();
    },
    
    updateRemovingPuzzle: function(elapsedTime) {
      if (this.removingPuzzle) {
        
        //The first time we remove, the old puzzle may not exist, and is designated by a 1. 
        //var removingPuzzleExists = this.removingPuzzle != 1;
        var removingPuzzleExists = 1;
        
        var avatar = WB.user.get('activeAvatar');
        
        if (removingPuzzleExists) {
          this.removingPuzzle.updateForPlayer(elapsedTime, this.player, this.playerForward, this.playerRight, this.playerDir);
        }
        
        if (this.removingMode == 'fail' || this.removingMode == 'shuffle') {
          amount = this.removingAmount;
          this.activePuzzle.explode(Math.pow(1.0 - this.removingAmount, 2.0) + 0.001);
          //this.player.setPosition(this.activePuzzle.startPos, Vec.scale(this.activePuzzle.startNormal, 8 * this.activePuzzle.explodeAmount)));
          var tile = this.activePuzzle.tiles[this.activePuzzle.startTile];
          this.player.setPosition(Vec.displace(this.activePuzzle.startPos, tile.normal, 8 * this.activePuzzle.explodeAmount));
          
          if (removingPuzzleExists && this.removingMode == 'shuffle')
            this.removingPuzzle.setScale(1.0001 - Math.pow(amount, 0.5));
        } else if (this.removingMode == 'pass') {
          var amount = Math.max(this.removingAmount, 0.01);
          
          if (removingPuzzleExists) {
            this.removingPuzzle.explode(this.removingAmount);
          }
          this.activePuzzle.setScale(amount);
        }

        
        this.human.material.uniforms.dissolve.value = 0.0;

        
        
        
        if (this.removingTick > 1) {
          this.removingAmount += elapsedTime / WB.REMOVING_DUR;
        } else {
          this.removingTick += 1;
        }
        
        if (this.removingAmount > 1.0) {
          this.beginPuzzle(removingPuzzleExists);
        }
      }
    },  
    
    beginPuzzle: function(removingPuzzleExists) {
      if (removingPuzzleExists) {
        this.destroyPuzzle(this.removingPuzzle);
      }
      
      var avatar = WB.user.get('activeAvatar');
      if (avatar.multiplier) {
        WB.overlay.setFactor("avatar", {type: "add", reason: avatar.name, factor: avatar.multiplier});
      } else {
        WB.overlay.deleteFactor("avatar");
      }
      
      WB.audioManager.stopEffects();
      this.playerDeathPos = undefined;
      this.activePuzzle.state = WB.PUZZLE_STATE.PLAY;
      this.activePuzzle.explode(0.0);
      this.activePuzzle.setScale(1.0);
      this.activePuzzle.resetTileColors();
      
      this.rampUpSpeed = 0.3;
      
      if(this.activePuzzle.puzzleIndex == 0) {
        this.setActiveLevelData({}, this.activePuzzle.level.level_id);
      }
      
      this.removingPuzzle = undefined;
      this.removingAmount = 0.0;
      this.activity = WB.GAME_ACTIVITY.DEFAULT;
      this.human.material.uniforms.dissolve.value = 0.0;
      
      if (this.activePuzzle) this.activePuzzle.fadeTileColor(this.playerTile);
    },
    
    updateScreenSaver : function(elapsedTime) {
      
      if (this.screenSaverTime == undefined) {
        this.screenSaverTime = 0.4;
      }
      

      this.screenSaverTime += elapsedTime;
      
      var time = 1.75;
      
      if (this.screenSaverTime + elapsedTime > time) {
        var delay = time - this.screenSaverTime;
        this.screenSaverTime = -delay;

        this.screenSaverCounter = this.screenSaverCounter || 0;
        
        if (this.screenSaverCounter % 4 == 0) {
          
          if (this.ssTone == undefined) {
            this.ssTone = -1;
            this.ssLoop = -1;
          }
          
          this.ssTone = ((this.ssTone) + 1) % 4;
          
          if (this.ssTone == 0) {
            this.ssLoop++;
          }
          
          if (this.ssTone == 0 || this.ssTone == 2) {
            //audioManager.playSound('rotopo', {delay: time * 0.95});
            //console.log('playSound');
          }
          
          if (this.ssLoop % 2 != 0) {
            this.sst1 = [0,1,2,7][this.ssTone];
            this.sst2 = [4,5,6,3][this.ssTone];  
          } else {
            this.sst1 = [0,2,0,1][this.ssTone];
            this.sst2 = [0,2,0,1][this.ssTone];  
          }
          
        }
                        
        var t1 = WB.MUSIC.MAJOR[this.sst1];
        var t2 = WB.MUSIC.MAJOR[this.sst2];
        
        if (!GAME.SYSTEM.MOBILE) {
          audioManager.playSynth({
            volume: 10.0 * VOLUME,
            frequency: t1,
            pan: -0.5 + (this.sst1/7),
            delay: delay
          });
          
          audioManager.playSynth({
            volume: 6.0 * VOLUME,
            frequency: t1,
            delay: delay + 0.75 * (time/2.0),
            pan: -0.5 + (this.sst1/7),
          });
          
          
          audioManager.playSynth({
            volume: 7.0 * VOLUME,
            frequency: t2,
            delay: delay + 1.0 * (time/2.0),
            pan: -0.5 + (this.sst2/7),
          });
          
          
          audioManager.playSynth({
            volume: 7.0 * VOLUME,
            frequency: t2,
            delay: delay + 1.5 * (time/2.0),
            pan: -0.5 + (this.sst2/7),
          });
        }
        
        this.screenSaverCounter++;
      }
      
      if (this.ssTone != undefined) {
        this.xxx = this.xxx || 0;
        this.xxx += elapsedTime;
        this.activePuzzle.setEuler([0, this.xxx * Math.PI * 1.5 / 64,0]); 
      }
    },
    
    handleActivePuzzleVictory: function() {
      WB.overlay.savePuzzleCompleted(this.activePuzzle);
      this.camera.saveVictoryState();
      this.updatePlayerDirection();
    },
    
    //Returns false if play is to continue, otherwise returns true if user play is disabled.
    updateActivePuzzle: function(elapsedTime) {
      var puzzleState = this.activePuzzle.state;
      var avatar = WB.user.get('activeAvatar');
      
      if (this.activity == WB.GAME_ACTIVITY.REMOVING) return true;
      
      if (puzzleState == WB.PUZZLE_STATE.INSPECT) {        
        return true;
      }
      
      //Victory animation
      if (puzzleState == WB.PUZZLE_STATE.VICTORY) {
        //update victory
        this.updatePlayerOrientation(elapsedTime, true);
        var pos = this.player.getPosition();
        var dir = Vec.sub(pos, this.activePuzzle.victoryTile.center);
        var dist = Vec.length(dir);
        if (!this.avatarData.frozen && dist > PLAYER_RADIUS * 0.5) {
          this.human.speed = dist * 0.2;
          //this.human.armsUp = Math.clamp(1.0 - (dist/this.activePuzzle.victoryTile.sideLength) * 2.0, 0.0, 1.0);
          var newPos = Vec.moveTo(pos, this.activePuzzle.victoryTile.center, elapsedTime * 0.25);
          this.player.setPosition(newPos);
        }
        
        
        this.human.armsUp = Math.clamp(Math.pow(this.activePuzzle.victoryTime / WB.VICTORY_DUR, 1.4), 0.0, 1.0);

        return true;
      }
      
      if (puzzleState == WB.PUZZLE_STATE.PASS) {
        
        this.checkPoint = this.puzzleIndex;
        
     
        
        /*
        if (this.puzzleIndex >= this.level.puzzles.length) {
          //Save level history
          WB.user.addPair('levelHistory', this.level.level_id, this.level.puzzles.length);
        }*/
                
        
        if (!WB.TEST_SHAPE) 
          this.puzzleIndex++;
        
        if (this.puzzleIndex >= this.level.puzzles.length) {
          //New Level
          WB.overlay.shuffle();
        } else {
          this.removeActivePuzzle('pass');
          var puzzle = this.prepareNextPuzzle();
          this.introduceNextPuzzle(puzzle);
          puzzle.setScale(0.0001);
        }
        return true;
      } else if (puzzleState == WB.PUZZLE_STATE.FAIL) {
        
        /*
        this.puzzleIndex--;
        if (this.puzzleIndex < this.checkPoint) {
          this.puzzleIndex = this.checkPoint;
        }*/
        
        this.removeActivePuzzle('fail');
        var puzzle = this.prepareNextPuzzle();
        this.introduceNextPuzzle(puzzle);
        return true;
      } else if (puzzleState == WB.PUZZLE_STATE.DIE) {
        var t = (this.activePuzzle.dyingTime += elapsedTime / WB.DEATH_DUR);
        this.playerDeathPos = this.playerDeathPos || this.player.getPosition() 
        if (t > 1.0) {
          this.activePuzzle.fail();
        }
        
        this.player.setPosition(Vec.displace(this.playerDeathPos, this.playerIn, t*t*6.0));
        this.player.setPosition(Vec.displace(this.player.getPosition(), this.playerDir, Math.pow(t, 0.5) * 0.3));
        this.human.material.uniforms.dissolve.value = t;
        //this.human.setPosition([0,-(this.removingAmount * this.removingAmount) * 3.0, 0.0]);
        
        //this.human.setPosition([0, 0.06, avatar.lift-(t * t  * 6.0)]);
        return true;
      }
      
      return false;
    },
    
    updatePlayerDirection: function() {
      switch (this.direction) {
        case 'UP':
          Vec.copyInto(this.playerDir, this.playerForward);
        break;
        case 'DOWN':
          this.playerDir = Vec.invert(this.playerForward);
        break;
        case 'LEFT':
          this.playerDir = Vec.invert(this.playerRight);
        break;
        case 'RIGHT':
          Vec.copyInto(this.playerDir, this.playerRight);
        break;
      }
    },
    

    queueNextAvatar: function(avatar) {
      WB.gamePlayer.flagUpdateAvatar = avatar;
      if (!avatar.preloaded) {
        avatar.preloaded = this.loadAvatar(avatar);
      }
    },

    updateForAvatar: function(elapsedTime) {
      WB.updateGameForAvatar.call(this, elapsedTime);
    },

    deployCountermeasures: function() {
      var avatar = WB.user.get('activeAvatar');
      if (avatar.jetpack && !this.avatarData.jetpackUsed) {
        this.deployJetpack();
      }
      if (avatar.bomb) {
        this.deployBomb();
      }
      if (avatar.dash) {
        this.deployDash();
      }
    },
    
    deployDash: function() {
      if(this.avatarData.dashDeployed) return;
      this.avatarData.immune = true;
      this.avatarData.autopilot = true;
      this.avatarData.dashStartPos = Vec.clone(this.player.getPosition());
      this.avatarData.speed = 2.0; 
      this.avatarData.dashDeployed = true;
      this.activePuzzle.markTileCleared(this.playerTile);
      this.avatarData.dashSynth = WB.audioManager.playEffect({type: "DASH"});

      //this.avatarData.dashTime = 0;
    },
    
    updateDash: function(elapsedTime) {
      var distance = Vec.distance(this.avatarData.dashStartPos, this.player.getPosition());
      if (distance > this.activePuzzle.sideLength * 1.05) {
        this.avatarData.immune = false;
        this.avatarData.autopilot = false;
        this.avatarData.speed = 1.0;
              
        if (this.avatarData.dashSynth) {
          this.avatarData.dashSynth.stop();
        }
        
        delete (this.avatarData.dashStartPos);
        if (Vec.rectDistance(this.player.getPosition(), this.playerTile.center) > this.activePuzzle.sideLength * 0.5) { 
          var nextTile = this.activePuzzle.getTile(
            Vec.sub(this.player.getPosition(), Vec.setLength(this.playerIn, 0.03)),
            this.playerIn);
          if (nextTile) {
            this.playerTile = nextTile;
          } else {
            this.activePuzzle.die(); 
            return;
          }
        } 
  
        
        this.activePuzzle.fadeTileColor(this.playerTile);
    
        //this.playerTile.fadeTileColor();
      } else {
        
      }
    },
    
    updateBomb: function(elapsedTime) {
      this.avatarData.bombTime += elapsedTime;
      if (this.avatarData.bombTime > 1.0) {
        this.avatarData.immune = false;
        this.effectsGroup.remove(this.avatarData.objs.bombBlast);
        this.avatarData.objs.bombBlast.destroy();

        this.avatarData.bombTime = undefined;
        if(this.avatarData.bombSynth) {
          this.avatarData.bombSynth.stop();
        }
        
        if(!this.activePuzzle.completed) {
          this.activePuzzle.die();
        }
      } else {
        //Draw bomb
      }
    },
    
    deployBomb: function() {
      if (this.avatarData.bombDeployed) return;
      this.avatarData.immune = true;
      this.avatarData.bombTime = 0;
      this.avatarData.bombDeployed = true;
      this.avatarData.frozen = true;
      var playerPos = this.player.getPosition();
      this.avatarData.objs = this.avatarData.objs || {};

      this.avatarData.objs.bombBlast = new WB.bombBlast({});
      this.avatarData.objs.bombBlast.setPosition(playerPos);
      this.effectsGroup.add(this.avatarData.objs.bombBlast);
      
      var tiles = this.activePuzzle.tiles;
      var s = this.activePuzzle.sideLength;
      
      for (var t = 0; t < tiles.length; t++) {
        if (!tiles[t].activated && Vec.distance(tiles[t].center, playerPos) < 1.5 * s) {
          this.activePuzzle.fadeTileColor(tiles[t]);
        }
      }
      this.avatarData.bombSynth = WB.audioManager.playEffect({type: "ICE_BOMB"});

    },
    
    cleanAvatarData: function() {
      if (this.avatarData && this.avatarData.objs) {
        var objs = this.avatarData.objs;
        
        for (var o in objs) {
          this.effectsGroup.remove(objs[o]);
          objs[o].destroy();
        }
      }
    },
    
    deployJetpack: function() {
     //Zippy
     
      if (this.human.loaded && !this.avatarData.jetpack) {
        this.avatarData.jetpack = this.human.bodyMeshes.body.glObject.children[0];
        this.avatarData.jetpack.visible = false;
        this.avatarData.jetpack.material = WB.jetpackFireMaterial;        
      } else if (!this.human.loaded) {
        return;
      }
      
      this.avatarData.jetpackUsed = true;
      this.avatarData.jetpackTime = 0;
      this.avatarData.jetpack.visible = true;
      this.avatarData.immune = true;
      this.avatarData.velocity = _.clone(this.playerDir);
      this.human.setStanding();
      this.activePuzzle.markTileCleared(this.playerTile);
      
      this.avatarData.objs = this.avatarData.objs || {};
      this.avatarData.objs.jetpackSmoke = new WB.jetpackSmoke({
        object: this.avatarData.jetpack,
        particleDir: Vec.invert(this.playerCharacterUp)
      });
      this.effectsGroup.add(this.avatarData.objs.jetpackSmoke);
      
      this.avatarData.jetpackSynth = WB.audioManager.playEffect({type: "JETPACK"});
    },
    

        
    updateJetpack: function(elapsedTime) { 
      this.avatarData.jetpackTime += elapsedTime;
      var pos = this.human.getPosition();
      var avatar = WB.user.get('activeAvatar');
      
      if (this.avatarData.jetpackTime > 1.95) { //End jetpack
        delete this.avatarData.jetpackTime;
        this.avatarData.immune = false;
        this.avatarData.jetpack.visible = false;
        this.avatarData.speed = 1.0;
        this.avatarData.velocity = undefined;
        pos[2] = avatar.lift;
        this.human.setPosition(pos);
        //WB.audioManager.stopEffect();
        this.human.squat = 0;
        this.human.setPosition([0,0,avatar.lift]);
        this.effectsGroup.remove(this.avatarData.objs.jetpackSmoke);
        this.avatarData.objs.jetpackSmoke.destroy();
        delete this.avatarData.objs.jetpackSmoke;
        if (this.avatarData.jetpackSynth)
          this.avatarData.jetpackSynth.stop();
        
        //Potentially fire ray
        if (Vec.rectDistance(this.player.getPosition(), this.playerTile.center) > this.activePuzzle.sideLength * 0.5) { 
          var nextTile = this.activePuzzle.getTile(
            Vec.sub(this.player.getPosition(), Vec.setLength(this.playerIn, 0.03)),
            this.playerIn);
          if (nextTile) {
            this.playerTile = nextTile;
          } else {
            this.activePuzzle.die(); 
            return;
          }
        } 
        
        this.activePuzzle.fadeTileColor(this.playerTile);
    
      } else {
        var t = this.avatarData.jetpackTime / 2.0;
        this.avatarData.jetpack.scale.z = 1.0 + Math.sin(t * 100.0) * 0.5; 
        this.avatarData.speed = 0.8;
        
        this.avatarData.velocity = Vec.displace(this.avatarData.velocity, this.playerDir, elapsedTime * 5.0);
        var drag = 1.0;
        this.avatarData.velocity = Vec.scale(this.avatarData.velocity, 1.0 - drag*elapsedTime); 
        if (Vec.length(this.avatarData.velocity) > 1.3) {
          this.avatarData.velocity = Vec.setLength(this.avatarData.velocity, 1.3);
        }
        
        var cutoff = 0.25;
        this.avatarData.objs.jetpackSmoke.particleDir = Vec.invert(this.playerCharacterUp);
        
        if (t < cutoff) {
          pos[2] = Math.inOut(t/cutoff, 2.0);
          //this.human.setStanding();
          this.human.squat = Math.min(t, 0.2);
        } else {
          var linear = 1.0 - (t-cutoff)/(1.0-cutoff);
          //pos[2] = 1.0 - Math.inOut(linear, 1.0) * 1.05;
          pos[2] = linear;
        }
        
        //pos[2] = 1.0 - Math.pow(0.5 - this.avatarData.jetpackTime / 2.0, 2.0) * 4.0;
        pos[2] *= 0.6;
        pos[2] += avatar.lift;
        
        
        if (t > 0.8) {
          this.human.squat = (1.0 - t);
        }
        
        this.human.speed = 0.0;
        this.human.setPosition(pos);
      }
    },
    
    updatePlayerOrientation: function(elapsedTime, lookAhead, pos) {
      if (this.avatarData.frozen) {
        return;
      } 
      var pos = pos || this.player.getPosition();
      //this.player.lookAt(Vec.add(pos, this.playerForward), this.playerRight); 
      var calcPlayerCharacterUp = Vec.invert(this.playerIn);
      
      var avatar = WB.user.get('activeAvatar');
      if (avatar.type == "rigid") {
        //this.playerCharacterUp = calcPlayerCharacterUp;
        //this.playerCharacterDir = this.playerDir;
        //this.player.lookAt(Vec.add(pos, this.playerCharacterUp), this.playerCharacterDir); 
        //return;
      }
      
      
      if(lookAhead) {
        var testPos = Vec.add(pos, Vec.setLength(this.playerDir, this.playerTile.sideLength * 0.3));
        var cent = this.playerTile.center;
        var tileCenterDistance = Math.max(Math.abs(testPos[0] - cent[0]),Math.abs(testPos[1] - cent[1]),Math.abs(testPos[2] - cent[2]));

        if (tileCenterDistance > this.playerTile.sideLength * 0.5001) {

          var nextTile;
          var dirString = Vec.axisString(this.playerDir);
          if (this.playerTile.neighbors[dirString]) {
            nextTile = this.playerTile.neighbors[dirString];
          }
          
          if (nextTile && nextTile != this.playerTile) {

            //Player anticipates a wall to climb up
            var dot = Vec.dot(nextTile.normal, Vec.invert(this.playerDir));
            if (dot > 0.9 || ((this.activePuzzle.cameraType == "follow") && dot < -0.9)) {
              calcPlayerCharacterUp = nextTile.normal;
            } 
          }
        }
      }
      
      
      this.playerCharacterUp = this.playerCharacterUp || calcPlayerCharacterUp;
      this.playerCharacterDir = this.playerCharacterDir || this.playerDir;
      
      //this.playerCharacterUp = GFX.slerpVec(this.playerCharacterUp, calcPlayerCharacterUp, Math.clamp(elapsedTime * 4.0, 0.001, 1.0), elapsedTime * 0.5);
      
      
      this.playerCharacterUp = GFX.slerpVec(this.playerCharacterUp, calcPlayerCharacterUp, Math.clamp(elapsedTime * 4.0, 0.001, 1.0), elapsedTime * 0.5, undefined, this.playerCharacterDir);
      this.playerCharacterDir = GFX.slerpVec(this.playerCharacterDir, this.playerDir, Math.clamp(elapsedTime * 7.0, 0.001, 1.0), elapsedTime * 0.7, undefined, this.playerCharacterUp);
     
      if (avatar.type == "rigid") {
        //this.playerCharacterUp = calcPlayerCharacterUp;
        //this.playerCharacterDir = this.playerDir;
        //this.player.lookAt(Vec.add(pos, this.playerCharacterUp), this.playerCharacterDir); 
        //return;
      } else {
      
      }
      
      this.player.lookAt(Vec.add(pos, this.playerCharacterUp), this.playerCharacterDir); 
    },
    
    //Deprecated
    checkTileUnderPos: function(testPos, stepOffFlag, currentTile) {
      //var pos2 = Vec.add(testPos, Vec.setLength(this.playerIn, 2 * PLAYER_RADIUS));
      var nextTile = this.activePuzzle.getTile(testPos, Vec.sub(this.playerTile.testPoint, testPos));
      //console.log('garbage', testPos);
      
      if (!nextTile || (stepOffFlag && nextTile == currentTile)) {
        nextTile = this.activePuzzle.getTile(this.playerTile.testPoint2, Vec.sub(testPos, this.playerTile.testPoint2));
      } 
      
      return nextTile;
    },
    
    gameLoop: function(elapsedTime) {
      
      this.human.speed = 0.0;
      if (this.freeze) return;
      
      if (this.activePuzzle && this.screenSaver) {
        this.updateScreenSaver(elapsedTime);
        return;
      }
      this.debug();
      
      if (this.activity == WB.GAME_ACTIVITY.REMOVING)
        this.updateRemovingPuzzle(elapsedTime);

      if (!this.activePuzzle) {
        return;
      }
      
      this.updateForAvatar(elapsedTime);

      if (this.updateActivePuzzle(elapsedTime)) {
        return;
      }
      
      
      //Update player movement
      this.handleKeysDown(elapsedTime);
      this.updatePlayerDirection();
      this.activeLevelData.time += Math.floor(elapsedTime * 1000);
      
      var pos = this.player.getPosition();
     
      var avatar = WB.user.get('activeAvatar');
      
      this.rampUpSpeed += elapsedTime * 1.2;
      if (this.rampUpSpeed > 1.0) {
        this.rampUpSpeed = 1.0;
      }
      
      //var rus = this.rampUpSpeed;
      var rus = Math.pow(this.rampUpSpeed, 1.6);
      
      
      var avSpeed = ((avatar.speed || 1.0) * (this.avatarData.speed || 1.0) * rus);
     // var avSpeed = (avatar.speed) || 1.0;
      
      //Move player
      if (!this.avatarData.frozen && !this.avatarData.jetpackTime) {
        
        this.human.speed = 0.14 * rus;
        if (this.avatarData.speed) {
          this.human.speed *= this.avatarData.speed;
        }
      }
      
      var speed = elapsedTime * SPEED * (this.activePuzzle.playerSpeed || 1.0) * avSpeed;
      
      var newPos;
      
      if (this.avatarData.velocity) {
        newPos = Vec.add(pos, Vec.scale(this.avatarData.velocity, speed));
      } else {
        newPos = Vec.add(pos, Vec.scale(this.playerDir, speed));
      }
      
      if (this.avatarData.frozen) {
        newPos = _.clone(pos);
      }
      
      //Distance to plane
      var pt = this.playerTile;
      
      var tileDist = Vec.dot(Vec.sub(newPos, pt.center), pt.normal);
  
      var correction = Vec.setLength(this.playerIn, (tileDist - PLAYER_RADIUS));
      if (this.activePuzzle.cameraType == "follow") {
        var correction = Vec.setLength(this.playerIn, (tileDist - PLAYER_RADIUS) * Math.clamp(0.0, elapsedTime, 1.0));
      }
      
      if (Vec.length(correction) > 1.0) {
        this.activePuzzle.die();
        return;
      }
      
      this.player.setPosition(Vec.add(newPos, correction));
      
      
      //Test for puzzle tile intersections
      pos = this.player.getPosition();  
      var tilePosition = Vec.sub(pos, Vec.scale(this.playerIn, PLAYER_RADIUS));


      var cent = this.playerTile.center;
      
      var tileCenterDistance = Vec.scalarProject(Vec.sub(tilePosition, cent), this.playerDir);
      //var tileCenterDistance = Math.max(Math.abs(tilePosition[0] - cent[0]),Math.abs(tilePosition[1] - cent[1]),Math.abs(tilePosition[2] - cent[2]));
      this.updatePlayerOrientation(elapsedTime, true, tilePosition);      
      if (tileCenterDistance < this.playerTile.sideLength * 0.3) {
        return;
      } 
      

      if (tileCenterDistance / this.playerTile.sideLength > 0.501) {
        var nextTile;
        
        
        var dirString = Vec.axisString(this.playerDir);
        if (this.playerTile.neighbors[dirString]) {
          nextTile = this.playerTile.neighbors[dirString];
        } 
        
        if (!nextTile || nextTile.activated) {
          this.deployCountermeasures();
        }
        
        
        if (nextTile) {
          if (!this.avatarData.immune) {
            this.activePuzzle.markTileCleared(this.playerTile);
            this.activePuzzle.fadeTileColor(nextTile);
            
            if (this.activePuzzle.state == WB.PUZZLE_STATE.DIE) {
              return;
            }
          }          

        
          var normal = this.playerTile.normal;
          var newNormal = nextTile.normal;
          
          var theta = Vec.angleBetween(normal, newNormal);
          var cross = Vec.perpendicular(normal, newNormal);
          
          /*
          if (theta > 0.01 && theta < Math.PI - 0.1) {
            var nCross = Vec.perpendicular(normal, newNormal);
            var dCross = Vec.perpendicular( 
              Vec.sub(nextTile.center, this.playerTile.center),
              this.playerDir
            );
            
            if (Vec.angleBetween(dCross, nCross) < 0.01) {
              this.activePuzzle.die();
              //this.changeDirection(WB.INVERTED_DIRECTIONS[this.direction]);
            }
          } else if (theta > Math.PI - 0.1) {
            //if (this.direction == "RIGHT" || this.direction == "LEFT")
            //  this.changeDirection(WB.INVERTED_DIRECTIONS[this.direction]);
            this.activePuzzle.die();
            return;
            //cross = this.playerDir;
          }*/
          
          
          if (theta > 0.01) {
            var oldUp = this.playerForward;
            this.playerIn = Vec.invert(newNormal);//Vec.normalize(GFX.rotateV3AxisAngle(this.playerIn, cross, theta));
            this.playerForward = Vec.toAxis(GFX.rotateV3AxisAngle(this.playerForward, cross, theta));
            var oldRight = this.playerRight;
            this.playerRight = Vec.toAxis(GFX.rotateV3AxisAngle(this.playerRight, cross, theta));
          } 
                    
          
          
          this.playerTile = nextTile;
          
          this.updatePlayerDirection();
          this.activePuzzle.updateTrackVecs(nextTile);
        } else {
          if (!this.avatarData.immune) {
            this.activePuzzle.die();
          }
        }
      }
    },

    
    update : function(elapsedTime) {
      elapsedTime *= GAME_RATE;
      this.gameLoop(elapsedTime);
      
      if (this.activePuzzle)
        this.activePuzzle.updateForPlayer(elapsedTime, this.player, this.playerForward, this.playerRight, this.playerDir);
      this.updateCamera(elapsedTime);
    },
    
    restartGame: function() {
      this.playLevel(this.level);
    },
    
    handleKeysDown: function(elapsedTime) {
      
    },
      
    handleClick: function() {
      this.scene.updateMouseCaster(this.mouseX, this.mouseY);
	    if (!WB.TEST_SHAPE) return true;
      if (this.activePuzzle) {
        var inter = this.scene._rayCaster.intersectObject(this.activePuzzle.cube.glObject);
        if (inter[0] && inter[0].faceIndex != undefined) {
          
          var fi = this.activePuzzle.cube.glObject.geometry.faces[inter[0].faceIndex].originalIndex;
          var ti = Math.floor(fi / 2);
            
          console.log(this.activePuzzle.tiles[ti])
          console.log("tile: ", ti, "normal: ", this.activePuzzle.tiles[ti].normal);
          //console.log(fi, this.activePuzzle.tiles[ti].facea, this.activePuzzle.tiles[ti].faceb);
          
          /* Extrude
          TEST_LEVEL.puzzles[0].extrudes.push([Math.floor(fi / 2)]);
          var n = this.playerTile.number;
          this.activePuzzle.makePuzzle(0);
          this.playerTile = this.activePuzzle.tiles[n];
          */
        }
      }        
    },
    
    hasFocus: function() {
      return (WB.state.get('focus') == 'gamePlayer');
    },
    
    changeDirection: function(direction) {
      if (!this.hasFocus()) return;

      if (WB.overlay.settings.controls == "inverted") {
        direction = WB.INVERTED_DIRECTIONS[direction];
      }
      
      this.lastStop = undefined;
      
      if (this.activity == WB.GAME_ACTIVITY.REMOVING) return;
      if (this.avatarData && (this.avatarData.autopilot || this.avatarData.frozen)) return;
      
      this.activeLevelData.turns++;
      this.direction = direction;
    },
    
    handleTouchMove: function(e) {
      if (!this.hasFocus()) return true;
      e = e.originalEvent;
      //console.log(e.originalEvent);
      var touch = e;
            
      if (e.changedTouches)
        touch = e.changedTouches[0];
      

      if (this.touchStart) {
        var diff = [
          touch.clientX - this.touchStart[0], 
          touch.clientY - this.touchStart[1], 
        ]
        e.preventDefault();
        
        var amt = 50;
        
        if (diff[0] > amt) {
          this.changeDirection("RIGHT");
          this.touchStart = undefined;
        } else if (diff[0] < -amt) {
          this.changeDirection("LEFT");
          this.touchStart = undefined;
        } else if (diff[1] < -amt) {
          this.changeDirection("UP");
          this.touchStart = undefined;
        } else if (diff[1] > amt) {
          this.changeDirection("DOWN");
          this.touchStart = undefined;
        }
      }
    },
    
    pause: function() {
      this.scene.pause();
    },
    
    unpause: function() {
      this.scene.unpause();
    },
    
    hide: function() {
      this.scene.hide();
    },
    
    show: function() {
      this.scene.show();
    },

    
    handleTouchStop: function(e) {
      
      //Initiate sounds on ios9
      if (!WB.IOS_SOUND_ON) {
        WB.IOS_SOUND_ON = 1;
        WB.audioManager.playSynth({
          frequency: 200,
          volume : 0.0001,
        });
      }
      
      if (!this.hasFocus()) return true;

      
      if (e.target.className == "viewport" || e.target.className == "fade") {
        e.preventDefault();
        if (this.lastStop) {
          var doubleTapTime = GAME.now() - this.lastStop;
          var focus = WB.state.get('focus');
          if (doubleTapTime < 200) {
            
            if (focus == 'gamePlayer' || focus == 'pauseMenu') {
              WB.overlay.togglePauseMenu();
            }
          }
        }
        
        this.lastStop = GAME.now();
      }
      
      this.touchStart = undefined;
      return true;
    },

    handleTouchStart: function(e) {
      if (!this.hasFocus()) return true;
      //if (!e.changedTouches) return;
      e = e.originalEvent;
      var touch = e;
      if (e.changedTouches)
        touch = e.changedTouches[0];
      
      this.touchStart = [touch.clientX, touch.clientY];      
    },
    
    editingKeypress: function(key) {
      if (WB.APP_MODE == "release") {
        return;
      }
      
      switch(key) {
        case 'p' :
          this.activePuzzle.pass();
        break;
        
        case 'v' :
          this.activePuzzle.playVictory(this.playerTile);
        break;
        
        /*
        case 'i' :
          this.activePuzzle.inspect();
          $('#overlay').hide();
        break;
        */
        
        case 'z' :

        break;
      }
    },

    handleKeypress: function(key) {
      var fcs = WB.state.get('focus');

      switch (key) {
        case 'left':
        case 'a':
          this.changeDirection("LEFT");
        break;
        case 'down':
        case 's':
          this.changeDirection("DOWN");
        break;
        case 'right':
        case 'd':
          this.changeDirection("RIGHT");
        break;
        case 'up':
        case 'w':
          this.changeDirection("UP");
        break;
        case 'tab':
          if (fcs == 'gamePlayer' || fcs == 'pauseMenu') {
            WB.overlay.shuffle();
            return false;
          }
        break;
        case 'enter':
          
          var $el = $(document.activeElement);
          if ($el.is(":visible")) {
            $el.trigger('click');
          }
        
        break;
        case 'backspace':
          if (fcs == 'gamePlayer' || fcs == 'pauseMenu') {
            //this.restartLevel();
            WB.menu.displayAlert({type: "CONFIRM_RESTART"});
            return false;
          }
        case 'space':

          /* promo
          if (this.scene.paused)
            this.unpause();
          else
            this.pause();
          
          return;
          */
          if (fcs == 'gamePlayer' || fcs == 'pauseMenu') {
            WB.overlay.togglePauseMenu();
            
                  
            if (WB.TEST_SHAPE) {
              $('.pause-button').hide();
            }
          
            return false;
          }
    
        //this.freeze = !!!this.freeze;
        break;
        default :
          this.editingKeypress(key);
      } 
      
      return true;
    },
  });
}