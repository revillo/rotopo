var Overlay = function(WB) {
  'use strict'
  var HIGH_INT = 99999999;
  var SORGHUM = {
    name: "sorghum", 
    point_price: 0, 
    directory: "avatars",
    description: "Over decades that seem but a moment in time, lines of scarlet figures shuttled among the sorghum stalks to weave a vast human tapestry.<br> -Mo Yan",
    video: "1fq3q6OAR3U",
    "difficulty" : "easy",		
    item_id: -1, 
    img:"/images/shop/avatars/default.png"        
  }
  
  var HTML_STAR = "&#9733;"
  
  
  var AMZN = {
    shop : [
    {
      //rubiks
      code : '<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ac&ref=tf_til&ad_type=product_link&tracking_id=baanfell-20&marketplace=amazon&region=US&placement=B00I19QZX8&asins=B00I19QZX8&linkId=3ea5c9c0016e5e0aba0bf896bbe524a0&show_border=false&link_opens_in_new_window=true&price_color=00bbff&title_color=141414&bg_color=ffffff"></iframe>'
    }, {
      //lego
      code : '<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ac&ref=tf_til&ad_type=product_link&tracking_id=baanfell-20&marketplace=amazon&region=US&placement=B00NW2Q6ZG&asins=B00NW2Q6ZG&linkId=b1d5552fb52eaafb99c32b5521a4b5b9&show_border=false&link_opens_in_new_window=true&price_color=00bbff&title_color=141414&bg_color=ffffff"></iframe>'
    }, {
      //owl
      code : '<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ac&ref=tf_til&ad_type=product_link&tracking_id=baanfell-20&marketplace=amazon&region=US&placement=B00Q70D7OI&asins=B00Q70D7OI&linkId=89ff4499faa3107ff5eb97f0bd7bec31&show_border=false&link_opens_in_new_window=true&price_color=00bbff&title_color=141414&bg_color=ffffff"></iframe>'
    }, {
      //magic cubes
      code : '<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ac&ref=tf_til&ad_type=product_link&tracking_id=baanfell-20&marketplace=amazon&region=US&placement=B01G4QW150&asins=B01G4QW150&linkId=730dc237c128dde5c3c18b695e889eba&show_border=false&link_opens_in_new_window=true&price_color=00bbff&title_color=141414&bg_color=ffffff"></iframe>'
    }, {
      //wafers
      code : '<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ac&ref=tf_til&ad_type=product_link&tracking_id=baanfell-20&marketplace=amazon&region=US&placement=B000HDOPYM&asins=B000HDOPYM&linkId=103d03b3edcbe6dbb62d12b078982d39&show_border=false&link_opens_in_new_window=true&price_color=00bbff&title_color=141414&bg_color=ffffff"></iframe>'
    }, {
      //ice cubes
      code : '<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ac&ref=tf_til&ad_type=product_link&tracking_id=baanfell-20&marketplace=amazon&region=US&placement=B00D3MGYR0&asins=B00D3MGYR0&linkId=bfd314d325a6627383ac85abbc638e9e&show_border=false&link_opens_in_new_window=true&price_color=00bbff&title_color=141414&bg_color=ffffff"></iframe>'
    },{
      //qbert
      code : '<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ac&ref=tf_til&ad_type=product_link&tracking_id=baanfell-20&marketplace=amazon&region=US&placement=B0167ZBBSG&asins=B0167ZBBSG&linkId=196d2207d0c7a53882fab6df35784812&show_border=false&link_opens_in_new_window=true&price_color=00bbff&title_color=141414&bg_color=ffffff"></iframe>'
    },{
      //learning cubes
      code : '<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ac&ref=tf_til&ad_type=product_link&tracking_id=baanfell-20&marketplace=amazon&region=US&placement=B000P1PVMQ&asins=B000P1PVMQ&linkId=e8c9820f4b67af1a5564bca53a558c2c&show_border=false&link_opens_in_new_window=true&price_color=00bbff&title_color=141414&bg_color=ffffff"></iframe>'
    }, {
      //companion
      code : '<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ac&ref=tf_til&ad_type=product_link&tracking_id=baanfell-20&marketplace=amazon&region=US&placement=B005QBQBXS&asins=B005QBQBXS&linkId=23a97762d4166f294d354f951adc2312&show_border=false&link_opens_in_new_window=true&price_color=00bbff&title_color=141414&bg_color=ffffff"></iframe>'
    }, {
      //clock
      code : '<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ac&ref=tf_til&ad_type=product_link&tracking_id=baanfell-20&marketplace=amazon&region=US&placement=B01C01PZUQ&asins=B01C01PZUQ&linkId=dfcad50e74013d5b575ef2848d38a180&show_border=false&link_opens_in_new_window=true&price_color=00bbff&title_color=141414&bg_color=ffffff"></iframe>'
    }, {
      //cubebot
      code : '<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ac&ref=tf_til&ad_type=product_link&tracking_id=baanfell-20&marketplace=amazon&region=US&placement=B009MP2ALG&asins=B009MP2ALG&linkId=2de547fe2a6dc3d0b602511a10b7c079&show_border=false&link_opens_in_new_window=true&price_color=00bbff&title_color=141414&bg_color=ffffff"></iframe>'
    }
    
    ]
  }
  
  
  WB.LevelMenu = GAME.Views.View.extend({
   
    setup: function() {
      var thiz = this;
      this.$el.on('click', '.level', function() {
        var index = $(this).attr('index');
        thiz.parent.shuffleList = thiz.select(thiz.parent.activeSelector);
        thiz.parent.shuffleList = _.reject(thiz.parent.shuffleList, function(level) {
          return level.level_id == index;
        });
        
        thiz.parent.playLevel(index);
      });
      
      this.$el.on('click', '.selector', function() {
        thiz.parent.shuffleList = thiz.select($(this).attr('tag'));
      });

    },
    
    hide: function() {
      this.$el.hide();
    },
    
    render: function() {
      var thiz = this;
      this.$el.html(GAME.templates["level-menu"]());
      this.$el.show();
      this.select(this.parent.activeSelector);
      this.fixCSS();
    },
    
    fixCSS: function() {
      var height = this.$el.height() - this.$('.selectors').height() - 5;
      this.$('.levels').css('height', height);
    },
    
    select: function(tag) {
      var levels = WB.user.get('levels');
      var levelHistory = WB.user.get('levelHistory');
      this.parent.activeSelector = tag;
      var result = [];
      this.$('.selector').removeClass('selected');
      this.$('.selector[tag="' + tag + '"]').addClass('selected');
      
      switch (tag) {
        case "easy":
        case "intermediate":
        case "advanced":
        case "insane":
          for (var level_id in levels) {
            if (levels[level_id].difficulty == tag) {
              result.push(levels[level_id]);
            }
          }
        break;
        case "solved":
          for (var level_id in levels) {
            if (levelHistory[level_id].completed >= levels[level_id].puzzles.length) {
              result.push(levels[level_id]);
            }
          }
        break;
        case "unsolved":
          for (var level_id in levels) {
            if (!levelHistory[level_id] || levelHistory[level_id].completed < levels[level_id].puzzles.length) {
              result.push(levels[level_id]);
            }
          }
        break;
        case "all":
          result = _.values(levels);
        break;
      }
      
      result.sort(function(a,b) { return(a.name).localeCompare(b.name)});
      
      this.$('.levels').html(GAME.templates["levels"]({levels: result}));
      
      if (!WB.EDITOR) {
        if (_.size(levels) >= WB.menu.gameInfo.numLevels) {
          this.$('.button.shop').hide();
        }
      }
      return result;
    }
  });
  
  WB.ShoppingCart = GAME.Models.Model.extend({
    items : [],
    
    addItem: function(item) {
      
      if (this.hasItem(item)) return;
      this.items.push(item);
      
      if (item.item_id == '999') {
        this.hasLevelPass = true;
        for (var i = 0; i < this.items.length; i++) {
          if (this.items[i].contents && this.items[i].contents.level_ids) {
            this.items.splice(i, 1);
            i-=1;
          }
        }
      }
      this.updateOverlay();
    },
    
    updateOverlay: function() {
      if (!this.isEmpty()) {
        $('#cart-count').html(this.items.length);
        $('#line-cart').show();
        $('.gimbles .back').hide();
      } else {
        $('#line-cart').hide();
        $('.gimbles .back').show();
      }
    },
    
    hasItem: function(item) {
      if (item.contents && item.contents.level_ids && this.hasLevelPass) {
        return true;
      }

      for (var i = 0; i < this.items.length; i++) {
        if (this.items[i].item_id == item.item_id) {
          return true;
        }        
      }
      
      return false;
    },
    
    isEmpty: function() {
      return this.items.length == 0;
    },
    
    getFee: function() {
      var cost = this.getItemCost();
      
      if (cost < 100) {
        return 30;
      } else return 0;
    },
    
    checkPurchases: function() {
      var purchases = WB.user.get('purchases');
      for (var i = 0; i < this.items.length; i++) {
        if (purchases[this.items[i].item_id]) {
          this.items.splice(i,1);
          i -= 1;
        }          
      }
      this.updateOverlay();
    },
    
    removeItemID: function(itemID) {
      this.items = _.reject(this.items, function(i){
        return i.item_id == itemID;
      });
      
      if (itemID == "999") {
        this.hasLevelPass = false;
      }
      this.updateOverlay();
    },
    
    getTotalCost : function() {
      return this.getItemCost() + this.getFee();
    },
    
    getItemCost: function() {
      var cost = 0;
      for (var i = 0; i < this.items.length; i++) {
        cost += this.items[i].cash_price;
      }
      return cost;
    },
    
    packageData: function() {
      return {
        items: this.items,
        totalPrice: this.getTotalCost(),
        fee: this.getFee()
      }
    }
  });    
  
  WB.Overlay = GAME.Views.View.extend({
    clientVersion: WB.CLIENT_VERSION,

    setup: function() {
      var thiz = this;
      this.activeSelector = 'all';
      this.clientVersion = WB.CLIENT_VERSION;
      this.multiplier = 1.0;
      this.shoppingCart = new WB.ShoppingCart();
      Handlebars.registerPartial('game-settings', function() {
        return GAME.templates['settings-list']();
      }());
     
      Handlebars.registerHelper('time-left', function(factor) {
        if (factor && factor.expires) {
          
          
          return "(" + GAME.Format.timeFromMS(Math.max(0, factor.expires - GAME.now()), 0) + ")";
        }
      });
      
      Handlebars.registerHelper('level-stats', function(level) {
        var levelHistory = WB.user.get('levelHistory')[level.level_id];
        
        if (!levelHistory) {
          levelHistory = {
            completed: 0,
            retry_record: HIGH_INT,
            time_record: HIGH_INT,
            turn_record: HIGH_INT
          }
        }
        
        var retry = levelHistory.retry_record;
        
        if (retry >= HIGH_INT) {
          retry = "--";
        } else if (retry == 0) {
          retry = "PERFECT";
        }
        
        var time = levelHistory.time_record;
        
        if (time >= HIGH_INT) {
          time = "-:-";
        } else {
          time = GAME.Format.timeFromMS(time);
        }
        
        var turns = levelHistory.turn_record;
        
        if (turns >= HIGH_INT) {
          turns = "--";
        } 
        
        var lhs = {
          completed: levelHistory.completed + "/" + level.puzzles.length,
          retry_record: retry,
          time_record: time,
          turn_record: turns
        }
        
        return GAME.templates['level-stats'](lhs); 
      });
      
      Handlebars.registerHelper('ifPurchased', function(item, block) {
        
        //TODO
        //return block.inverse(item);

        
        var purchases = WB.user.get('purchases');
        if (item.directory != "avatars") {
          if (purchases[item.item_id] || purchases['999']) {
            if (item.item_id == '999') {
              return "";
            } else {
              var level_id = item.contents.level_ids[0];
              return block.fn({level_id: level_id});
            }
          } else {
            return block.inverse(item);
          }
        }
        
        //Else for avatars
        if (item.directory == "avatars" && (purchases[item.item_id] || item.item_id < 0)) {
          
          if (WB.gamePlayer.flagUpdateAvatar) {
            if (item.name == WB.gamePlayer.flagUpdateAvatar.name) {
              return '<div class="button">up next</div>';
            }
          } else {
            if (item.name == WB.user.get('activeAvatar').name) {
              return "";
            }
          }
          
          return block.fn(item);
        } else {
          return block.inverse(item);
        }
        
        
        //return !purchases[item.item_id];
      });
      
      Handlebars.registerHelper('solved', function(item, block) {
        var levelHistory = WB.user.get('levelHistory');
        
        if (levelHistory[item.level_id] && levelHistory[item.level_id].completed >= item.puzzles.length) {
          if (levelHistory[item.level_id].retry_record == 0) {
            //return "&radic;&radic;"
            //return  "&#128504;&#128504;" check-marks
            return "&#9733;&#9733;"
          }
          
          return "&#9733;"
        } else {
          return "";
        }
        
        //return !purchases[item.item_id];
      });
      
      Handlebars.registerHelper('decimal', function(factor) {
        return factor.toFixed(2);
      });
      
      Handlebars.registerHelper('cashPurchase', function(item, block) {
        if (thiz.shoppingCart.hasItem(item)) {
          return block.inverse(item);
          //return "<div class = 'purchase-cash'><div class = 'button cart-checkout'>in cart.</div></div>";
        } else {
          if (item.cash_price) {
            return block.fn(item);
          } else {
            return "";
          }
        }
      });
      
      Handlebars.registerHelper('cash', function(price, block) {
        var dollars = Number(price) / 100;
        return '$' + dollars.toFixed(2);
      });
      
      this.shuffleList = _.values(WB.user.get('levels'));
      
      this.factors = {
        "winloss" : {reason: "win - loss", factor: 1.0} 
      };
      
      this.draw();
      
      this.$el.on('click', '.button.menu-return', function() {
        WB.menu.reloadMenu();
      });
      
      $('#view').on('click', '#settings .option', function() {
        var type = $(this).parent().attr('name');
        thiz.modifySetting(type, $(this).text());
        thiz.saveSettings();
        $(this).parent().find('.option').removeClass('selected');
        $(this).addClass('selected');
      });
      
      this.$el.on('click', '.cart-checkout', function() {
        thiz.showCheckoutView();
      });
      
      this.$el.on('click', '.button.badges', function() {
        WB.menu.showBadges();
      });
      
      this.$el.on('click', '.level-entry .preview', function() {
        WB.menu.displayAlert({type: "LEVEL_OVERVIEW", level : WB.user.get('levels')[$(this).attr('index')]});
      });
      
      this.shopReminderShown = 0;
      this.showShopReminder = 0;
      
      WB.user.onChange('points', function(points) {
        var currentPoints = Number(thiz.$('#gimbles').text());
        thiz.pointAnim.setElement(thiz.$('#gimbles'));
        thiz.pointAnim.animateNumber(currentPoints, points, 2000);
        
        var userID = WB.user.get('user_id');
        if (userID < 0) {
          GAME.storage.setItem("points_" + userID, points);
        }
        
        /*
        var purchases = WB.user.get('purchases');
  
        if (thiz.showShopReminder && !thiz.shopReminderShown && points > 2000 && _.isEmpty(purchases)){
          thiz.shopReminderShown = 1;
          WB.menu.displayAlert({type: "SHOP_REMINDER"});
        }
        */
        
        //thiz.$('#gimbles').text(points);
      });
      
      WB.state.onChange('currentPuzzle', function(puzzle) {
        thiz.updateLevelDisplay(puzzle);
      });
      
      this.$el.on('click', '.button.shuffle', function() {
        thiz.shuffle();
      });
      
      this.$el.on('click', '.button.restart', function() {
        WB.gamePlayer.restartLevel();
      });
      
      this.$el.on('click', '.button.shop', function() {
        thiz.togglePauseMenu(true);
        thiz.loadShop();
      });
      
      this.$el.on('click', '.pause-button', function() {
        thiz.togglePauseMenu();
      });
           
      this.$el.on('click', '#settings.condensed .header', function() {
        if ($('.box.settings').length) {
          
        } else {
          WB.menu.displayAlert({type: "SETTINGS"});
        }
      });
      
      this.$el.on('click', '.leveldisplay', function() {
        thiz.togglePauseMenu();
      });
      
      this.$el.on('click', '.use-avatar', function() {
        var item_id = $(this).attr('item_id');
        var name = $(this).attr('name');
        
        var avatars = WB.user.get('avatars');
        if (avatars[name]) {
          WB.gamePlayer.queueNextAvatar(avatars[name]);
          thiz.saveSettings();
        }
        thiz.drawShopItems();
      });
      

      
      this.rewardTextAnim = new GAME.HTMLAnim.TypeAnimator({
        el: this.$rewardText
      });

      this.pointAnim = new GAME.HTMLAnim.NumberScroller({
        el: this.$("#gimbles")
      });
      
      this.multiplierAnim = new GAME.HTMLAnim.NumberScroller({
        el: $('#multiplier .display .number'),
        places: 2,
        //prefix: "&times;"
      });
            
      this.$el.on('click', '.play-level', function() {
        if (thiz.leaveShop()) {
          thiz.playLevel(Number($(this).attr('level_id')));
        }
      });        
            
      this.$el.on('click', '.purchase-points', function() {
        thiz.clickPointPurchase($(this));   
      });
      
      this.$el.on('click', '.purchase-cash', function() {
        thiz.clickCashPurchase($(this));   
      });
      
      this.$el.on('click', '.button.back', function() {
        if (history)
          thiz.historyBack();
        else
          thiz.leaveShop();
      });
      
      this.$el.on('click', '.shop-section.levels .selectors .button', function() {
        thiz.levelItemsTag = $(this).attr('tag');
        $('.shop-section.levels .selectors .button').removeClass('selected');
        $(this).addClass('selected');
        thiz.drawShopItems();
        $('.levels .shop-list').scrollLeft(0);
      });
      
      this.$el.on('click', '.shop-section.avatars .selectors .button', function() {
        thiz.avatarItemsTag = $(this).attr('tag');
        $('.shop-section.avatars .selectors .button').removeClass('selected');
        $(this).addClass('selected');
        thiz.drawShopItems();
        $('.avatars .shop-list').scrollLeft(0);
      });
      
    
      $('#view').on('click', '.read-manual', function() {
        WB.menu.displayAlert({type: 'MANUAL'});
      });
      
      $('#view').on('click', '.contact-us', function() {
        var $alert = WB.menu.displayAlert({type: "CONTACT"});
        var clicked = 0;
        $alert.on('click', '.submit', function() {
          if (clicked) return;
          clicked = 1;
          $alert.find('.submit').html("One Moment");
          $alert.find('.ok').hide();

          
          var confirmContact = function(response) {
              $alert.find('.form').remove();
              $alert.find('.submit').remove();
              $alert.find('.ok').html('ok');
              $alert.find('.ok').show();
            if (response.success) {
              $alert.find('.info').html("Thank you. We have received your message.");
            } else {
              $alert.find('.info').html("Sorry. There was an error processing your message");
            }
          }
          
          thiz.postAPI('/api/contact_us', 
          {
            email: $alert.find('.email').val(),
            subject: $alert.find('.subject').val(),
            message: $alert.find('.message').val()
          }, 
          confirmContact); 
        });
      });
      
      this.avatarItemsTag = 'all';
      this.levelItemsTag = 'all';
      
         
      this.updateSettings(this.settings);
    },
    
    considerShopPrompt: function() {
      var purchases = WB.user.get('purchases');
      if (!_.isEmpty(purchases) || WB.SHOP_REMINDER_SHOWN) {
        return;
      }
      
      if (GAME.SYSTEM.MOBILE) {
        return;
      }
      
      try {
        var coreLevelsBeaten = 0;
        var levelHistory = WB.user.get('levelHistory');
        var levels = WB.user.get('levels');
        
        for (var level_id in levelHistory) {
          var progress = levelHistory[level_id].completed;
          if (levels[level_id].directory == "core") {
            if (progress >= levels[level_id].puzzles.length) {
              coreLevelsBeaten++; 
            }
          }
        }
        
        if (coreLevelsBeaten >= 3) {
          WB.menu.displayAlert({type: "TUTORIAL", number: 0});
          WB.SHOP_REMINDER_SHOWN = true;
        }
      } catch (error) {
        console.log(error);
      }
    },
    
    clickPointPurchase: function($div) {
      if ($div.hasClass('disabled')) return;
      
      if(!WB.menu.signedIn) {
        WB.menu.showLoginMenu();
        return;
      }
      
      if ($div.hasClass('button')) {
        $div.html('processing...');
      } else {
        $div.find('.pointpurchase').html('processing...');
        $div.find('.pointprice').hide();
      }
      
      $div.addClass('disabled');
      var item_id = $div.attr('item_id');
      this.handlePurchaseWithPoints(item_id);
    },
    
    clickCashPurchase: function($div) {
      if(!WB.menu.signedIn) {
        WB.menu.showLoginMenu();
        return;
      }
      
      //var $shopItem = $(this).closest('div[class^="shop-item"]');
      //var item_id = $shopItem.attr('item_id');
      var item_id = $div.attr('item_id');
      
      if (!item_id) return;
      
      if ($div.hasClass('button')) {
        $div.html('in cart!');
      }
      $div.addClass('disabled');
      var thiz = this;
      
      $div.click(function() {
        thiz.removeItemDescription();
        thiz.showCheckoutView();
      });

      var item = this.shopItemsByID[item_id];
      
      if (!item || !item.cash_price) return;
      
      this.shoppingCart.addItem(item);
      this.drawShopItems();
      
      //thiz.showCheckoutView(item);
    },
    
    setupBraintree: function(callback) {
      var thiz = this;
      
      braintree.setup(this.braintreeClientToken, "dropin", {
        container: 'braintree-area',
        onPaymentMethodReceived: callback,
        onReady: function(obj) {
          $('#checkout-layer .submit').show();
          WB.menu.resizeBoxes()
          thiz.braintreeReadyObj = obj;
        },
        onError: function(obj) {console.log("bt error", obj)}
      });
      
      this.braintreeSetup = true;
    },
    
    refreshCheckoutView: function() {
      if (this.shoppingCart.isEmpty()) {
        this.removeCheckoutView();
        return;
      }
      
      $('.cart-review-boxes').html(GAME.templates['checkout-review'](this.shoppingCart.packageData()));
      var price = this.shoppingCart.getTotalCost() / 100;
      $('.checkout-form .submit').val("Pay $" + price.toFixed(2));
      var thiz = this;
      $('#checkout-layer .remove').click(function() {
        var item_id = $(this).attr("item_id");
        thiz.shoppingCart.removeItemID(item_id);
        thiz.refreshCheckoutView();
        thiz.drawShopItems();
      });
    },
    
    stopAdBonus: function() {
      clearInterval(this.adBonusInterval);
      this.adBonusStart = undefined;;
    },

    adBonusTick: function() {
      if (!this.adBonusStart) {
        return;
      }
      var time = GAME.now() - this.adBonusStart;
      var dur = 15 * 1000;
      if (time >= dur) {
        this.stopAdBonus();
        this.setFactor('ad_bonus', {type: 'add', factor: 0.5, reason: "watched ad", expires: GAME.now() + (15 * 60 * 1000)});
        $('.ad-bonus-counter').html("received!");

      } else {
      
        $('.ad-bonus-counter').html("in " +GAME.Format.timeFromMS(Math.max(dur - time, 0), 0));
      }
    },
    
    startAdBonus: function() {
      this.adBonusStart = GAME.now();
      this.adBonusInterval = setInterval(this.adBonusTick, 1000);
    },
    
    ytVideoReady: function() {
    },
    
    ytVideoStateChange: function(event) {
      if (event.data == YT.PlayerState.PLAYING) {
        this.startAdBonus();
      }
    },
    
    adBonusOnPlay: function() {      
      this.ytPlayer = new YT.Player('yt-player', {
        events: {
          'onReady': this.ytVideoReady,
          'onStateChange': this.ytVideoStateChange
        }
      });
    },
    
    loadYoutubeAPI: function() {
      if (!window.YT) {
        
        window.onYouTubeIframeAPIReady = this.adBonusOnPlay
        
        
        $.getScript(
          "https://www.youtube.com/iframe_api"
        );
        
      } else {
        this.adBonusOnPlay();
      }
    },
    
    showItemDescription: function(item) {
      $('#checkout-layer').html(GAME.templates['item-description'](item));
      WB.audioManager.setSequenceVolume(0.1);
      
      if (item.video) {
        this.loadYoutubeAPI();
      }
      
      $('.fade').show();
      this.showingItem = item;
      $('#checkout-layer .back').click(this.removeItemDescription);
      WB.menu.resizeBoxes();
      var thiz = this;
      
      if ($(window).width() < $('.description').width()) {
        $('.box.description').css({
          width: '32em',
          marginLeft: '-17em',
        });
        $('.box.description .text').css({
          marginLeft: '1em',
        });
      }
      
      $('.description .pointpurchase').click(function() {
        thiz.clickPointPurchase($(this));
      });
      
      $('.description .cashpurchase').click(function() {
        thiz.clickCashPurchase($(this));
      });
      
      if(item.item_id == "999") {
        
        var refund = 0;
        var purchases = WB.user.get('purchases');
        
        if (purchases[item.item_id]) {
          $('.box.description .description').append($("<br><span> Thank you for playing and supporting roTópo.</span>"));
          return;
        }
        
        for (var p in purchases) {
          var item = this.shopItemsByID[p];
          if (item.contents && item.contents.level_ids) {
            refund += item.point_price;
          }
        }
        
        $('.box.description .description').append($("<br><span> You will also be refunded the total cost (<b>+" + refund + "</b>) in gimbles of your previoulsy purchased levels.<span>"));
      }
      
    },
    
    removeItemDescription: function() {
      $('#checkout-layer').html("");
      $('.fade').hide();
      this.showingItem = 0;
      WB.audioManager.setSequenceVolume(1.0);
    }, 
    
    showCheckoutView: function() {
      if (this.showingCheckout) return;
      var packageData = this.shoppingCart.packageData();
      this.showingCheckout = 1;
      $('#checkout-layer').html(GAME.templates['checkout-box'](packageData));
      $('.checkout-area').prepend($(GAME.templates['checkout-review'](packageData)));
      var thiz = this;
      
      //checkout remove item
      $('#checkout-layer .remove').click(function() {
        var item_id = $(this).attr("item_id");
        thiz.shoppingCart.removeItemID(item_id);
        thiz.refreshCheckoutView();
        thiz.drawShopItems();
      });
      
      $('.fade').show();
      
      this.setupBraintreeForm();
      
      $('#checkout-layer .back').click(this.removeCheckoutView);
      
      if ($(window).width() < $('.checkout-area').width()) {
        $('.box.cart-checkout').css({
          'position' : 'relative',
          left: 0,
          top: '2em'
        });
        
        $('.box.cart-review').css({
          width: '25em',
          'max-height' : '25em'
        });
        
        $('.box.cart-review .item-list').css({
          'max-height' : '15em'
        });
        
        $('.checkout-area').css({
          'margin-left' : "-14.5em"
        });
      }
    
    },
    
    removeCheckoutView: function() {
      var thiz = this;
      this.showingCheckout = 0;
      if (thiz.braintreeReadyObj) {
        thiz.braintreeReadyObj.teardown(function(){
          thiz.braintreeReadyObj = undefined;
          $('#checkout-layer').html("");
          $('.fade').hide(); 
        });
      } else {
        $('#checkout-layer').html("");
        $('.fade').hide(); 
      }
      this.stopAdBonus();
    },
    
    alertNoBraintree: function() {
      WB.menu.displayAlert({error: "BRAINTREE_OFFLINE"});
      this.getBraintreeTimeout = 0;
    },  
    
    setupBraintreeForm: function() {
      var thiz = this;

      if (!window.braintree && !this.getBraintreeTimeout) {
        $.getScript(
          "https://js.braintreegateway.com/v2/braintree.js",
          this.setupBraintreeForm
        );
        
        this.getBraintreeTimeout = setTimeout(this.alertNoBraintree, 3000);
        return;
      } else {
        clearTimeout(this.getBraintreeTimeout);
      }
      
      var callback = function(braintreeData) {
        thiz.handlePurchaseWithCash(braintreeData);
      }
      
      if (this.braintreeClientToken) {
        thiz.setupBraintree(callback);
        return;
      }

      
      this.getAPI('/api/get_braintree_client_token', {}, function(response) {
        if (response.success) {
          thiz.braintreeClientToken = response.token;
          thiz.setupBraintree(callback);
        }
      });
    },
    
    updateLevelDisplay: function(puzzle) {
      if (!puzzle) return;
      var levelHistory = WB.user.get('levelHistory');
      this.$('.leveldisplay').html(puzzle.level.name + "//" + puzzle.name + " (" + (puzzle.puzzleIndex+1) + "/" + puzzle.level.puzzles.length + ")"); 
      if(levelHistory[puzzle.level.level_id] && levelHistory[puzzle.level.level_id].completed > puzzle.puzzleIndex) {
        this.$('.leveldisplay').append($('<span>&#9733;</span>'));
      }
    },
    
    updateSettings: function(settings) {
      for (var setting in settings) {
        this.modifySetting(setting, settings[setting]);
      }
    },
    
    modifySetting: function(setting, option) {
      if (setting == 'antialiasing') {
        if (option == 'low') {
          WB.gamePlayer.scene.setCanvasScale(1.0);
        } else if (option == 'med') {
          WB.gamePlayer.scene.setCanvasScale(1.25);
        } else if (option == 'hi') {
          WB.gamePlayer.scene.setCanvasScale(1.5);
        } else if (option == 'retro') {
          WB.gamePlayer.scene.setCanvasScale(0.5);
        }
      }
      
      if (setting == 'music') {
        if (option == 'on') {
          WB.audioManager.enableSounds();
        } else {
          WB.audioManager.disableSounds();
        }
      }
      
      if (setting == 'fullscreen') {
        if (option == 'on') {
          WB.gamePlayer.goFullScreen();
        } else if (option == 'off') {
          WB.gamePlayer.exitFullScreen();
        }
      }

      this.settings[setting] = option;
    },
    
    deleteFactor: function(name) {
      delete this.factors[name];
      this.updateMultiplier();
    },
    
    setFactor: function(name, value) {
      this.factors[name] = value;
      this.updateMultiplier();
    },
    
    handlePurchaseWithPoints: function(item_id) {
      if (!item_id || this.awaitingPurchaseConfirmation) {
        
      } else {
        this.awaitingPurchaseConfirmation = item_id;
        this.postAPI("/api/purchase_items", {item_ids: [item_id], method: 'POINTS'}, this.confirmPurchase);
      }
    },
    
    handlePurchaseWithCash: function(data) {
      if (this.purchaseUnderway) {
        return;
      }
      
      var items = this.shoppingCart.items;


      var thiz = this;
      if (!items.length) {
       
      } else {
                    
        $('#checkout-layer .submit').hide();
        $('#checkout-layer .back').hide();
        $('#checkout-layer .cart-checkout .header').html('Processing...');

        this.purchaseUnderway = true; 

        thiz.postAPI("/api/purchase_items", {item_ids: _.pluck(items, 'item_id'), method: 'CASH', data: data, amount_payed: this.shoppingCart.getTotalCost()}, function(response) {
          thiz.purchaseUnderway = false;
          thiz.confirmPurchase(response);
          $('#checkout-layer .back').show();
            
          if (response.success) {
            $('#checkout-layer .cart-checkout .header').html('SUCCESS!');
            $('#checkout-layer .back').html('ok');
            thiz.braintreeClientToken = undefined;
            thiz.braintreeReadyObj.teardown(function(){
              thiz.braintreeReadyObj = undefined;
            });
          } else {
            $('#checkout-layer .cart-checkout .header').html('PAY');
            $('#checkout-layer .submit').show();
          }
        });
      }
    },
    
    /*
    handlePurchaseWithCash: function(item, token) {
      if (!item) {

      } else {
        this.postAPI("/api/purchase_items", {item_ids: [item.item_id], method: 'CASH', token: token}, this.confirmPurchase);
      }
    },*/
    
    confirmPurchase: function(response) {
      if (response.success) {
        WB.audioManager.playSound('thank_you');

        if (this.awaitingPurchaseConfirmation)
          this.recentlyPurchased[this.awaitingPurchaseConfirmation] = 1;

        if (response.content) {
          WB.menu.initUserData(response.content, true);
          this.remakeShuffleList();
          this.drawShopItems();
          this.shoppingCart.checkPurchases();
        }
      } else {
        WB.menu.displayAlert(response);
        this.drawShopItems();
      }
      this.awaitingPurchaseConfirmation = undefined;

      if (this.showingItem) {
        this.showItemDescription(this.showingItem);
      }
    },
    
    leaveShop: function() {
      if (this.purchaseUnderway) {
        return false;
      }

      
      this.stopShopMusic();
      
      this.removeCheckoutView();
      this.removeItemDescription();
      
      this.inShop = false;
      this.draw();
      
      //WB.gamePlayer.unpause();
      WB.gamePlayer.show();
      
      
      if (WB.gamePlayer.removingPuzzle) {
        WB.gamePlayer.removingPuzzle.show();
      }
      
      if (WB.gamePlayer.activePuzzle) {
        WB.gamePlayer.activePuzzle.show();
        WB.gamePlayer.scene.renderScene();

      }
      
      if (WB.gamePlayer.effectsGroup)
        WB.gamePlayer.effectsGroup.setVisibility(true);
      
      WB.state.popFocus();
      this.pauseMenuOpen = false;
      this.togglePauseMenu(true);
      return true;
    },
    
    playLevel: function(level_id) {
      var levels = WB.user.get('levels');
      WB.gamePlayer.playLevel(levels[level_id]);
      
      //Close menu
      this.togglePauseMenu(false);
    },
    
    updateFactorList: function() {
      $('#multiplier .factor-list').html(GAME.templates["factor-list"]({factors: this.factors}));
    },
    
    togglePauseMenu: function(bool) {
      
      if (bool != undefined) {
        if (!!this.pauseMenuOpen == !!bool) {
          return;
        }
      }      
      
            
      var $settings = $("#settings");
      var $factorList = $('#multiplier .factor-list');
      
      
      //Close menu
      if (this.pauseMenuOpen) {
        //$levelList.hide();
        if (this.levelMenu)
          this.levelMenu.hide();
        
        $settings.hide();
        $factorList.hide();
        this.pauseMenuOpen = false;
        WB.gamePlayer.unpause();
        WB.state.setFocus('gamePlayer');
        this.$('.fade').hide();
        this.$('.pause-button').hide();
        this.$('.tasks').addClass("condensed");
        clearInterval(this.updateFactorListInterval);
        return;
      }
      this.drawOptions(this.$el);

      
      WB.state.setFocus('pauseMenu');
      $('#overlay').show();

      $settings.show();
      $factorList.show();
      this.$('.fade').fadeIn(100);
      this.$('.pause-button').fadeIn(100);
     
      this.updateTasks("show");
      
      $factorList.html(GAME.templates["factor-list"]({factors: this.factors}));
      
      this.updateFactorListInterval = setInterval(this.updateFactorList, 1000);
      WB.gamePlayer.pause();
      this.pauseMenuOpen = true;
      this.levelMenu.render();
      
      if ($(window).width() < (WB.state.get('window').fontSize * 350) + $(".line.levels").width()) {
        $('#settings').addClass('condensed');
        $('.pause-button').css({
          top: "18em",
          fontSize: "0.7em"
        });
        $('#level-menu').css({
          fontSize: "1em",
          bottom: "4em"
        });
      } else {
        $settings.removeClass('condensed');
        $('.pause-button').css({
          top : "",
          fontSize: ""
        });
        $('#level-menu').css({
          fontSize: "",
          bottom: ""
        });
      }
    },
    
    loadShop: function(callback) {
      this.loadShopCallback = callback;
      this.getAPI('/api/get_shop_items', {}, this.drawShop);
    },
    
    updateTasks: function(mode) {
      var activeLevelData = WB.gamePlayer.activeLevelData;
      
      if (activeLevelData) {
        if (mode == "show")
          this.$('.tasks').removeClass("condensed");
        
        this.$('.current.retries').html(activeLevelData.retries);
        this.$('.current.time').html(GAME.Format.timeFromMS(activeLevelData.time));
        this.$('.current.turns').html(activeLevelData.turns);
        
        var levelHistory = WB.user.get('levelHistory');
        var recordTime = "/-:-";
        var recordRetries = "/-";
        var recordTurns = "/-";
        
        if (levelHistory[activeLevelData.level_id]) {
          recordTime = levelHistory[activeLevelData.level_id].time_record;
          recordRetries = levelHistory[activeLevelData.level_id].retry_record;
          recordTurns = levelHistory[activeLevelData.level_id].turn_record;
          
          this.$('.current.time').toggleClass('ahead', activeLevelData.time <= recordTime); 
          this.$('.current.retries').toggleClass('ahead', activeLevelData.retries <= recordRetries);
          this.$('.current.turns').toggleClass('ahead', activeLevelData.turns <= recordTurns);
          
          if (recordTime >= HIGH_INT) {
            recordTime = "/-:-";
          } else {
            recordTime = "/" + GAME.Format.timeFromMS(recordTime);
          }

          this.$('.record-retries').removeClass('bold');

          if (recordRetries == 0 || (mode == "finish" && activeLevelData.retries == 0)) {
            recordRetries = "PERFECT";
            this.$('.record-retries').addClass('bold');
            this.$('.current.retries').html("");
          } else if (recordRetries >= HIGH_INT) {
            recordRetries = "/-";
          } else {
            recordRetries = "/" + recordRetries;
          }
        }
        
        if (recordTurns >= HIGH_INT) {
          recordTurns = "/-";
        } else {
          recordTurns = "/" + recordTurns;
        }
        
        this.$('.record-retries').html(recordRetries);
        this.$('.record-time').html(recordTime);
        this.$('.record-turns').html(recordTurns);
      }
    },
    
    
    markPurchased: function() {
      var purchases = WB.user.get('purchases');
      var levelHistory = WB.user.get('levelHistory');
      
      for (var p in purchases) {
        $('.shop-item[item_id="'+p+'"]').addClass('purchased');
        
        /*
        if (!this.recentlyPurchased[p])
          $('.shop-item[item_id="'+p+'"]').addClass('levelhide');
        
        
        var si = this.shopItemsByID[p];
        if (si && si.contents.level_ids) {
          var level_id = si.contents.level_ids[0];
          var lh = levelHistory[level_id];
          if (lh && lh.completed > 0) {
            $('.shop-item[item_id="'+p+'"]').addClass('levelhide');
          }
        }*/
      }
      
      if (purchases['999']) {
        $('.shop-item.levels').addClass('purchased');
        //$('.shop-item.levels').addClass('levelhide');
        $('.shop-item[item_id="999"]').removeClass('levelhide');
      }
      
      $('.shop-item[item_id="-1"]').addClass('purchased');
      
      
      var $purchased = $('.shop-item.levels.purchased');
      for (var i = 0; i < $purchased.length; i++) {
        var $p = $($purchased.get(i));
        var item_id = Number($p.attr('item_id'));
        var item = this.shopItemsByID[item_id];
        if (item && item.contents && item.contents.level_ids) {
          var level_id = item.contents.level_ids[0];
          var lh = levelHistory[level_id];
          if (lh && lh.completed > 0) {
            $p.addClass('levelhide');
          }
        }
      }
      
    },
    
    
    drawShopItems: function() {
      if (!this.inShop) {
        return;
      }
      var level_packs = this.shopItems.level_packs;
      var avatars = this.shopItems.avatars;

      var display_packs = [];
      var display_avatars = [];
      
      if (this.levelItemsTag != 'all') {
        for (var l in level_packs) {
          if(level_packs[l].difficulty == this.levelItemsTag) {
            display_packs.push(level_packs[l]);
          }
        }
      } else {
        display_packs = _.values(level_packs);
      }
      
      var purchases = WB.user.get('purchases');
      
      //All levels purchased
      if (purchases['999']) {
        //display_packs = [];
      }

      display_packs = _.sortBy(display_packs, function(item) {
        return (item.point_price || (item.cash_price * 0.01));
      });
      
      
      if (this.avatarItemsTag != 'all') {
        for (var a in avatars) {
          var purchased = purchases[avatars[a].item_id];
          
          if (avatars[a].hidden && !purchased) {
            continue;
          } 
          
          if(purchased) {
            if (this.avatarItemsTag == 'purchased')
              display_avatars.push(avatars[a]);
          } else if (this.avatarItemsTag == 'locked') {
            display_avatars.push(avatars[a]);
          }
        }
      } else {
        
        for (var a in avatars) {
          var purchased = purchases[avatars[a].item_id];
          
          if (avatars[a].hidden && !purchased) {
            continue;
          } 
          
          
          display_avatars.push(avatars[a]);
        }
        //display_avatars = _.values(avatars);
      }
      
      if (this.avatarItemsTag != 'locked') {
        display_avatars.push(SORGHUM);
      }
      
      display_avatars = _.sortBy(display_avatars, function(item) {
        if (item.point_price != undefined) return item.point_price 
        else return item.cash_price;
        //return (item.point_price || (item.cash_price ));
      });      
      
      this.$('.levels .shop-list').html(GAME.templates["level-items"]({level_packs: display_packs}));
      this.$('.avatars .shop-list').html(GAME.templates["avatar-items"]({avatars: display_avatars}));
      this.markPurchased();
      
      $('.shop-section.levels .selectors .button[tag^="' + this.levelItemsTag + '"]').addClass('selected');
      $('.shop-section.avatars .selectors .button[tag^="' + this.avatarItemsTag + '"]').addClass('selected');
      
      var thiz = this;
      
      //this.placeShopAds();
      
      $('.preview').click(function() {
        var itemID = $(this).closest('div[class^="shop-item"]').attr('item_id');
        var item = thiz.shopItemsByID[itemID];
        if (item)
          thiz.showItemDescription(item);
        else if (itemID == -1) {
          thiz.showItemDescription(SORGHUM);
        }
      });
      
      $('.shop-item').on('touchend', function() {
        if ($(this).hasClass('active') || $(this).hasClass('dragging') || $(this).hasClass('purchased') ) {
          return true;
        } else {
          $('.shop-item').removeClass('active');
          $(this).addClass('active');
          return false;
        }
      });
      
      $('.shop-item').on('touchstart', function() {
        $(this).removeClass('dragging');
        $(this).addClass('touchscreen');
      });
      
      $('.shop-item').on('touchmove', function() {
        $(this).addClass('dragging');
      });
      
      
      $('.shop-item').on('mouseover', function() {
        $('.shop-item').removeClass('active');
        $(this).addClass('active');
      });
      
      $('.shop-item').on('mouseleave', function() {
        if ($(this).hasClass('touchscreen')) {
          return;
        }
        
        $(this).removeClass('active');
      });
    },
    
    placeShopAds: function() {
      var $levels = $('.shop-item.levels');
      var count = 0;
      
      var ads = _.shuffle(AMZN.shop);
      var adi = 0;
      
      for (var l = 0; l < $levels.length; l++) {
        var $level = $($levels[l]);
        if(!$level.hasClass('levelhide')) {
          count++;
          
          if (count % 8 == 0) {
            var $img = $("<div class='shop-item-ad'></div>");
            var ad = ads[(adi++) % ads.length];
            $img.html(ad.code);
            $img.insertAfter($level);
          }
          
        }
      }
      
    },
    
    drawShop: function(shopItems) {
      if (!window.braintree) {
        $.getScript(
          "https://js.braintreegateway.com/v2/braintree.js"
        );
      }
      
      if (!WB.audioManager.hasSound('thank_you')) {
        WB.audioManager.loadSounds({
          thank_you: {src: "/assets/sounds/thank_you", volume: 33 * WB.VOLUME},
        });
      }
      
      this.inShop = true;
      shopItems = shopItems.items;
      
      this.shopItems = shopItems;
      this.recentlyPurchased = {};
      
      this.shopItemsByID = {};
      
      for (var av in shopItems.avatars) {
        this.shopItemsByID[shopItems.avatars[av].item_id] = shopItems.avatars[av];
      }
      
      for (var lp in shopItems.level_packs) {
        this.shopItemsByID[shopItems.level_packs[lp].item_id] = shopItems.level_packs[lp];
      }
      
      this.pauseMenuOpen = false;
      
      this.$el.html(GAME.templates["overlay-shop"]());
      this.drawShopItems();

      this.$('#gimbles').text(WB.user.get('points'));
  
  
      $('.shop-list').mousewheel(function(event, delta) {
        var temp = this.scrollLeft;
        this.scrollLeft -= (delta * 80);
        
        if (this.scrollLeft != temp) {
          event.preventDefault();
        }
      });
      
      WB.state.pushFocus('shop');
      WB.gamePlayer.pause();
      //WB.gamePlayer.hide();
      
      if (WB.gamePlayer.activePuzzle) {
        WB.gamePlayer.activePuzzle.hide();
      }
      
      if (WB.gamePlayer.removingPuzzle) {
        WB.gamePlayer.removingPuzzle.hide();
      }
      
      if (WB.gamePlayer.effectsGroup)
        WB.gamePlayer.effectsGroup.setVisibility(false);
      
      WB.gamePlayer.scene.renderScene();
      
      this.shoppingCart.updateOverlay();
      this.pushHistory({state: 'shop'}, 'roTopo Shop', "/shop");
      
      this.playShopMusic();

      if (this.loadShopCallback) {
        this.loadShopCallback();
      }
      
      /*
      $('.shop-ad').click(function(e) {
        if(e.target == this) {
          //window.open('https://www.amazon.com/?&tag=baanfell-20&camp=0&creative=0&linkCode=ur1&adid=1CP2JAPS316GHPH4666Z&');
          //window.open('http://vgppl.com');
        }
      });*/
      
      this.resize();
    },
    
    stopShopMusic: function() {
      WB.audioManager.stopSequence();
    },
    
    playShopMusic: function() {
      WB.audioManager.playSequence(
      {"info":{"duration" : 36},"notes":[{"n":56,"st":0,"et":36},{"n":57,"st":12,"et":96},{"n":61,"st":96,"et":144},{"n":57,"st":144,"et":192},{"n":56,"st":192,"et":216},{"n":57,"st":216,"et":240},{"n":56,"st":240,"et":264},{"n":61,"st":288,"et":336},{"n":57,"st":432,"et":480},{"n":59,"st":480,"et":528},{"n":62,"st":528,"et":576},{"n":61,"st":576,"et":624},{"n":57,"st":648,"et":672},{"n":54,"st":672,"et":768},{"n":56,"st":768,"et":804},{"n":57,"st":780,"et":864},{"n":61,"st":864,"et":912},{"n":57,"st":912,"et":960},{"n":56,"st":960,"et":984},{"n":57,"st":1008,"et":1032},{"n":56,"st":1032,"et":1056},{"n":61,"st":1056,"et":1104},{"n":57,"st":1200,"et":1248},{"n":59,"st":1248,"et":1296},{"n":62,"st":1296,"et":1344},{"n":61,"st":1344,"et":1392},{"n":57,"st":1392,"et":1440},{"n":56,"st":1440,"et":1488},{"n":59,"st":1488,"et":1536},{"n":56,"st":1536,"et":1572},{"n":57,"st":1548,"et":1632},{"n":61,"st":1632,"et":1680},{"n":57,"st":1680,"et":1728},{"n":56,"st":1728,"et":1752},{"n":57,"st":1752,"et":1776},{"n":56,"st":1776,"et":1800},{"n":61,"st":1824,"et":1872},{"n":57,"st":1968,"et":2016},{"n":59,"st":2016,"et":2064},{"n":62,"st":2064,"et":2112},{"n":61,"st":2112,"et":2160},{"n":57,"st":2184,"et":2208},{"n":54,"st":2208,"et":2268},{"n":52,"st":2292,"et":2304},{"n":54,"st":2304,"et":2352},{"n":49,"st":2352,"et":2448},{"n":52,"st":2448,"et":2544},{"n":54,"st":2544,"et":2592},{"n":49,"st":2592,"et":2640},{"n":47,"st":2640,"et":2688},{"n":45,"st":2688,"et":3312},{"n":50,"st":2688,"et":3312},{"n":57,"st":2688,"et":2832},{"n":59,"st":2832,"et":2976},{"n":61,"st":2976,"et":3072},{"n":62,"st":3072,"et":3120},{"n":64,"st":3120,"et":3192},{"n":64,"st":3216,"et":3312},{"n":62,"st":3312,"et":3360},{"n":64,"st":3360,"et":3408},{"n":66,"st":3408,"et":3456},{"n":59,"st":3456,"et":3600},{"n":64,"st":3456,"et":3600},{"n":57,"st":3600,"et":3696},{"n":62,"st":3600,"et":3696},{"n":61,"st":3696,"et":3744},{"n":62,"st":3744,"et":3792},{"n":64,"st":3792,"et":3840},{"n":56,"st":3840,"et":3984},{"n":62,"st":3840,"et":3984},{"n":57,"st":3984,"et":4056},{"n":61,"st":3984,"et":4080},{"n":57,"st":4080,"et":4128},{"n":59,"st":4128,"et":4176},{"n":56,"st":4176,"et":4224},{"n":50,"st":4224,"et":4752},{"n":57,"st":4224,"et":4368},{"n":59,"st":4368,"et":4512},{"n":61,"st":4512,"et":4608},{"n":62,"st":4608,"et":4656},{"n":64,"st":4656,"et":4752},{"n":61,"st":4752,"et":4992},{"n":54,"st":4992,"et":5040},{"n":49,"st":5040,"et":5136},{"n":52,"st":5136,"et":5232},{"n":54,"st":5232,"et":5256},{"n":52,"st":5256,"et":5280},{"n":49,"st":5280,"et":5328},{"n":59,"st":5328,"et":5376},{"n":57,"st":5376,"et":5616}]},
        {
          volume: WB.VOLUME * 7.5,
          loop: 1,
          delay: 1.5,
          rate: 1.2
        }
      );
    },
    
    savePuzzleFailed: function(puzzle) {
      this.factors.winloss.factor = Math.clamp(this.factors.winloss.factor - 0.05, 1.0, 3.0);
      this.updateMultiplier();
      var level_id = puzzle.level.level_id;
      var puzzle_index = puzzle.puzzleIndex;
      var thiz = this;
      
      var user_id = WB.user.get('user_id');
      
      GAME.storage.setItem(user_id + "_levelsave_" + level_id, JSON.stringify({
        puzzle_index: puzzle_index,
        retries: WB.gamePlayer.activeLevelData.retries,
        time : WB.gamePlayer.activeLevelData.time,
        turns : WB.gamePlayer.activeLevelData.turns
      }));
      
      if (level_id > 0 && WB.menu.signedIn) {
        var data = {level_id: level_id, puzzle_index: puzzle_index, multiplier: this.multiplier, winloss: this.factors.winloss.factor}
        data.retry_record = WB.gamePlayer.activeLevelData.retries;
        data.time_record = WB.gamePlayer.activeLevelData.time;
        data.turn_record = WB.gamePlayer.activeLevelData.turns;
        this.postAPI('/api/save', {type: "fail", data: data}, 
          function(pz) { 
            return function (data) {
              thiz.confirmSave(data, pz) 
            }
          }(puzzle));
      }
    },
    
    savePuzzleCompleted: function(puzzle) {
      var o = _hs;
      this.showShopReminder = 1;
      var reward = puzzle.reward || 0;
      var addition = Math.floor(reward * this.multiplier);
      var level_id = puzzle.level.level_id;
      var puzzle_index = puzzle.puzzleIndex;
      var isLastPuzzle = puzzle_index == puzzle.level.puzzles.length-1;
      this.factors.winloss.factor = Math.clamp(this.factors.winloss.factor + 0.2, 1.0, 3.0);
      var levelHistory = WB.user.get('levelHistory');

      var user_id = WB.user.get('user_id');
      
      if (puzzle_index == puzzle.level.puzzles.length-1) {
        GAME.storage.removeItem(user_id + "_levelsave_" + puzzle.level.level_id);
      } else {
        GAME.storage.setItem(user_id + "_levelsave_" + level_id, JSON.stringify({
          puzzle_index: puzzle_index + 1,
          retries: WB.gamePlayer.activeLevelData.retries,
          time : WB.gamePlayer.activeLevelData.time,
          turns : WB.gamePlayer.activeLevelData.turns
        }));
      }
     

      if (level_id > 0 && WB.menu.signedIn) {
        this.savingPuzzle = puzzle;
        var data = {
          level_id: level_id, 
          puzzle_index: puzzle_index, 
          multiplier: this.multiplier, 
          winloss: this.factors.winloss.factor,
        }
        
        data['ts'] = o(puzzle['ts']);
        
        data.retry_record = WB.gamePlayer.activeLevelData.retries;
        data.time_record = WB.gamePlayer.activeLevelData.time;
        data.turn_record = WB.gamePlayer.activeLevelData.turns;
        
        this.postAPI('/api/save', {type: "pass", data: data}, function(pz) { 
            return function (data) {
              thiz.confirmSave(data, pz) 
            }
          }(puzzle));
      } else {
        puzzle.saveFlag = true;
        WB.user.increase('points', addition);
      }
      
      if (level_id < 0) return;
      
      this.$rewardBar = $(".reward-bar");
      this.$rewardText = $(".reward-text");
      
      //Reward anim
      this.$rewardBar.css({
        maxWidth: 0,
        opacity: "0em",
        paddingLeft: "0em"
      });
      
      this.$rewardBar.show();
      
      var thiz = this;
      this.$rewardBar.html(HTML.tag("+" + addition, 'b') + HTML.tag("(" + reward + "&times;" + this.multiplier.toFixed(2) + ")", "smaller"));
      this.$rewardText.html("");
      
      this.rewardTextAnim.setElement(this.$rewardText[0]);
      
      
      var rewardText = "puzzle completed";
      var level = WB.user.get('levels')[level_id];
      
      var levelData = WB.gamePlayer.activeLevelData;
      var hist = levelHistory[level_id];

      if (isLastPuzzle) {
        rewardText = "level completed";
        //var levelData = WB.gamePlayer.activeLevelData;
        this.updateTasks("finish");
        var newRecord = false;
        var overwrite = false;
        
        if (hist.completed <= puzzle_index) {
          newRecord = true;
          overwrite = true;
        }
        
        if (overwrite || levelData.retries < hist.retry_record) {
          hist.retry_record = levelData.retries;
          var $el = this.$('.gimbles .retry-record');
          this.animateTask($el);
          newRecord = true;
        }
        
        if (overwrite || levelData.time < hist.time_record) {
          hist.time_record = levelData.time;
          var $el = this.$('.gimbles .time-record');
          this.animateTask($el);
          newRecord = true;
        }
        
        if (overwrite || levelData.turns < hist.turn_record) {
          hist.turn_record = levelData.turns;
          var $el = this.$('.gimbles .turn-record');
          this.animateTask($el);
          newRecord = true;
        }
        
        if (newRecord)
          WB.audioManager.playSound('new_record', {delay: 3.0});
        
      }
      
      if (!puzzle.alreadyBeaten) {
        hist.completed = puzzle_index+1;

        rewardText = "new puzzle completed!";
        if (isLastPuzzle) {
          rewardText = "new level completed!";
          this.animateStar();
        }

        if (isLastPuzzle) {
          var thiz = this;
          _.delay(function() {
            thiz.considerShopPrompt();
            }, 5000);
        }
      }    
      

      this.$rewardBar.animate({
        maxWidth: "10em",
        paddingLeft: "6.9em",
        opacity: 1
      }, 600, function() {
          thiz.$rewardText.show();
          thiz.rewardTextAnim.animateText(rewardText, 'type');
          thiz.updateMultiplier();
        }
      );
      
      
      this.rewardFadeTimeout = _.delay(function() {
        thiz.$rewardBar.fadeOut(); thiz.$rewardText.fadeOut()
      }, 6000);
      
      this.updateLevelDisplay(puzzle);
      
      if (user_id < 0) {
        GAME.storage.setItem(user_id + "_levelHistory", JSON.stringify(levelHistory));
      }

    },
    
    saveSettings: function() {
      var avatarName = WB.user.get('activeAvatar').name;
      if (WB.gamePlayer.flagUpdateAvatar) {
        avatarName = WB.gamePlayer.flagUpdateAvatar.name;
      }
      var data = {data: {settings: this.settings, avatar: avatarName}};
      
      var user_id = WB.user.get('user_id');
      
      GAME.storage.setItem(user_id + '_settings', JSON.stringify(this.settings));
      GAME.storage.setItem(user_id + '_avatar', avatarName);
      
      WB.menu.storedSettings = this.settings;
      WB.menu.storedAvatarName = avatarName;
      
      //this.postAPI('/api/save_settings', data, this.lg);
    },
    
    animateTask: function($el) {
      var textAnim = new GAME.HTMLAnim.TypeAnimator({
        el: $el
      });
      
      
      $el.addClass('animating');
      textAnim.animateText($el.html());
    },
    
    updateMultiplier: function() {
      var result = 1.0;
      for (var f in this.factors) {
        if (!this.factors[f]) continue;
        
        if (this.factors[f].expires) {
          if (GAME.now() > this.factors[f].expires) {
            this.factors[f] = undefined;
            continue;
          }
        }
        
        if (this.factors[f].type == 'add') {
          result += this.factors[f].factor;
          this.factors[f].symbol = "+";
        } else {
          result *= this.factors[f].factor;
          this.factors[f].symbol = "&times;";
        }
      }
      
      var oldMultiplier = this.multiplier;
      this.multiplier = result;
      
      if (this.multiplierAnim) {
        this.multiplierAnim.setElement($("#multiplier .display .number"));
        this.multiplierAnim.animateNumber(oldMultiplier, this.multiplier, 1000);
      } else {
        $('#multiplier .display .number').html('&times;' + this.multiplier.toFixed(2)); 
      }
      
      if (this.pauseMenuOpen) { 
        $('#multiplier .factor-list').html(GAME.templates["factor-list"]({factors: this.factors}));
      }
      
      return result;
    },
    
    confirmSave: function(data, puzzle) {
      if (data.success) {
        if (data.points) {
          
          WB.user.set('points', data.points);
        }
        
        if (puzzle)
          puzzle.saveFlag = true;
        
        
      } else {
        WB.menu.displayAlert(data);
        
        if (puzzle)
          puzzle.saveFlag = true;
      }
    },
    
    animateStar: function() {
      var $star = $('<div class = "level-star">');
      this.$el.append($star);
      
      $star.html(HTML_STAR);
      
      $star.css({
        position: 'fixed',
        top: '48%',
        right: '50%',
        transform : 'scale(20,20)',
        opacity: 0.0,
      });
      
      $star.animate(
      {
        opacity: 1.0,
      }, 
      {
        step: function(now, tween) {
          var amt = Math.lerp(20, 5, now);
          $(this).css('transform' , 'scale(' + amt + ',' + amt + ')');
        },
        duration: 1500,
        easing: 'linear',
        complete: function(){
          var start = GAME.now();
          $star.animate({
            top: '92%',
            right: '0%'      
          }, 
          {
            step: function(now2) {
              var amt = Math.lerp(5, 0.5, (GAME.now() - start)/700);
              $(this).css('transform' , 'scale(' + amt + ',' + amt + ')');
            },
            duration: 700,
            easing: 'linear',
            complete: function() {
              $star.remove();
            }
          })
        }
      });
      

    },
    
    handlePopHistory: function(state) {
      if (!state) {
        if (this.purchaseUnderway) {
          this.pushHistory({state: 'shop'}, 'roTopo Shop', "/shop");
        }
        this.leaveShop();
      }
    },
    
    remakeShuffleList: function() {
      this.shuffleList = this.levelMenu.select(this.activeSelector);
      if (WB.gamePlayer.activePuzzle) {
        var currentLevel = WB.gamePlayer.activePuzzle.level;
        this.shuffleList = _.reject(this.shuffleList, function(level) {
          return level.level_id == currentLevel.level_id;
        });
      } else {
        console
      }
    },
    
    shuffle: function(mode) {
      if(!this.shuffleList || this.shuffleList.length == 0) {
        this.shuffleList = this.levelMenu.select(this.activeSelector);
      }
      
      //var level = _.sample(this.shuffleList);
      
      var levelChoices = [];
      var min = 100000;
      var levelHistory = WB.user.get('levelHistory');
      
      for (var l = 0; l < this.shuffleList.length; l++) {
        var level = this.shuffleList[l];
        
        var fraction = -1;
        if (levelHistory[level.level_id])
          fraction = levelHistory[level.level_id].completed / level.puzzles.length;
        if (fraction < min) {
          min = fraction;
          levelChoices = [level];
        } else if (fraction == min) {
          levelChoices.push(level);
        }
      }
      
      var level = _.sample(levelChoices);
      
      if (level) {
        this.playLevel(level.level_id);
        this.shuffleList = _.without(this.shuffleList, level);
      }
    },
    
    draw: function() {
      this.$el.html(GAME.templates["overlay"]());
      this.$('#gimbles').text(WB.user.get('points'));
      this.updateMultiplier();
      this.updateLevelDisplay(WB.state.get('currentPuzzle'));
      this.levelMenu = new WB.LevelMenu({el: $("#level-menu"), parent: this}); 
      this.resize();
    },
    
    drawOptions: function($el) {
      $('.setting .option').removeClass('selected');
      for (var setting in this.settings) {
        $el.find('.setting .options[name~=' + setting + ']').find('.' + this.settings[setting]).addClass('selected');
      }
    },
    
    resize: function() {
      if (this.levelMenu) {
        this.levelMenu.fixCSS();
      }
      if ($('.line.gimbles').width() + $('#multiplier').width() > $(window).width()) {
        $('#multiplier').css("font-size", "0.5em");
      } else {
        $('#multiplier').css("font-size", "1em");
      }
      
      var adscale = ($('.shop-list').height() / 216) * 0.9;
      
      $('.shop-item-ad').css({
        transform: "scale(" + adscale + "," + adscale + ")"
      });
      
      if ($(window).width() < $('.line.gimbles').width() + 468) {
        $(".shop-ad").hide();
      } else {
        $(".shop-ad").show();
      }
           
      
    }
  });
  
  var _hs = function(a) {
    var hs = 0;
    if (a.length == 0) return hs;
    for (var i = 0; i < a.length; i++) {
      var char = a.charCodeAt(i);
      hs = ((hs<<5)-hs)+char;
      hs = hs & hs;
    }
    return hs;
  }  
} 