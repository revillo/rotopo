var Puzzle = function(WB) {
  'use strict'
  
  var GFX = GAME.GFX;
  
  WB.PUZZLE_STATE = Enum([
    'ENTER',   //0
    'PLAY',    //1
    'DIE',     //2
    'FAIL',    //3
    'VICTORY', //4
    'PASS',     //5
    'INSPECT'  //6
  ]);
  
  WB.LIGHT_VEC = new THREE.Vector4(0.1, 1.0, 0.1, 0.0);
  WB.PLAYER_VEC = new THREE.Vector4();
  
  WB.MUSIC = {
    //NOTES : [1.0, 27/24, 30/24, 36/24, 40/24, 2],
    MAJOR : [261.63, 293.66, 329.63, 349.23, 392, 440, 493.88, 523.25],
    MINOR : [220, 246.94, 261.63, 293.66, 329.63, 349.23, 392, 440],
    //PENTATONIC: [261.63, 293.66, 349.23, 392, 440, 523.25, 698.46, 783.99],
  }
  
  var UNIT_BOX_GEO = new THREE.BoxGeometry(1,1,1);
  
  var TILE_STATE = {
    coloredHex: 0xee6755, 
    ghostHex: 0x008800, 
    eraseHex: 0x000000,
    justBorderHex: 0x0000ff,
    errorHex: 0xff8800, 
    stepHex: 0x00ff00, 
    leaveHex: 0x00c000, 
    failHex: 0xbe7268,
    finishHex: 0x5588ff,
  }
  
  WB.TILE_STATE = TILE_STATE;
  
  
  //Default
  
  var COLOR_THEME = {
    background: [0.812, 0.839, 0.807], 
    colored: [0.933, 0.403, 0.333],
    ghost: [0.85, 0.8, 0.8],
    ghostAlpha: 0.75,
    style: 0,
    //nearGradient: [0.702, 0.294, 0.341],
    nearGradient: [0.63, 0.23, 0.31],
    //farGradient: [1, 0.6, 0.282]
    farGradient: [0.99, 0.53, 0.25]
  }
  
  
  
  WB.COLOR_THEME = COLOR_THEME;
  
  var TileMaterial = new THREE.ShaderMaterial({
    uniforms: {
      time: {type: 'f', value: 0},
      playerPos: {type: 'v3', value: new THREE.Vector3(0,0,0)},
      playerDir: {type: 'v3', value: new THREE.Vector3(0,0,0)}, 
      playerForward: {type: 'v3', value: new THREE.Vector3(0,0,0)}, 
      playerRight: {type: 'v3', value: new THREE.Vector3(0,0,0)}, 
      explodeAmount: {type: 'f', value: 0.0},
      ghostAlpha: {type: 'f', value:0.95},
      aliasScale : {type: 'f', value: 1.0},
      cameraDist : {type: 'f', value: 1.0},
      drawWindow : {type: 'i', value: 0},
      tex : {type: 't', value: undefined},
      coloredColor: {type: 'c', value: new THREE.Color()},
      ghostColor: {type: 'c', value: new THREE.Color()},
      nearGradient : {type: 'c', value: new THREE.Color(0x000000)},
      farGradient : {type: 'c', value: new THREE.Color(0xffffff)},
      state: {type: 'i', value: 0},
      style: {type: 'i', value: 0},
      AOEffect : {type: 'f', value: 1.0},
      scale : {type: 'f', value: 1.0},
      eyeLight : {type: 'v3', value: new THREE.Vector3(0,0,0)},
      eyePlayer: {type: 'v3', value: new THREE.Vector3(0,0,0)}
    },
    
    vertexShader: $('#vertexShader').text(),
    fragmentShader: $('#fragmentShader').text(),
    vertexColors: THREE.FaceColors,
    shading: THREE.FlatShading,
    side: THREE.DoubleSide,
    transparent: true,
  });
  
  WB.TileMaterial = TileMaterial;
  TileMaterial.sharedAsset = true;
  
  WB.SceneryMaterial = new THREE.ShaderMaterial({
    uniforms: {
      time: {type: 'f', value: 0},
      aliasScale : {type: 'f', value: 1.0},
      explodeAmount: {type: 'f', value: 0.0},
      nearGradient : {type: 'c', value: new THREE.Color(0x000000)},
      farGradient : {type: 'c', value: new THREE.Color(0xffffff)},
      cameraDist : {type: 'f', value: 1.0},
      playerPos : {type: 'v3', value: new THREE.Vector3()},
      style: {type: 'i', value: 0},
      state: {type: 'i', value: 0},
      drawWindow : {type: 'i', value: 0},
      opacity: {type: 'f', value: 1.0},
      eyeLight : {type: 'v3', value: new THREE.Vector3(0,0,0)},
      eyePlayer: {type: 'v3', value: new THREE.Vector3(0,0,0)}
    },
    
    vertexShader: $('#sceneryVertexShader').text(),
    fragmentShader: $('#sceneryFragmentShader').text(),
    vertexColors:  THREE.FaceColors,
    //vertexColors:  THREE.FaceColors,
    transparent: true,
  });  
  
  var tempMat4 = new THREE.Matrix4();
  
  GFX.addTileToGeometry = function(geo, center, normal, s, direction, faceData) {
    var vs = geo.vertices;
    var fs = geo.faces;
    var tempNormal = new THREE.Vector3();
    var tempDir = new THREE.Vector3();
    tempDir.fromArray(direction);
    tempNormal.fromArray(normal);
    
    var vn = vs.length;
     
     /*
    if (Math.abs(tempNormal.y) > 0.5) {
    
      tempMat4.lookAt(GFX.ZERO, tempNormal, GFX.xAxis);
      
    } else {
    
      tempMat4.lookAt(GFX.ZERO, tempNormal, GFX.yAxis);
    
    }*/
    
    
    tempMat4.lookAt(GFX.ZERO, tempNormal, tempDir);

    
    tempMat4.elements[12] = center[0];
    tempMat4.elements[13] = center[1];
    tempMat4.elements[14] = center[2];
    
    vs.push(new THREE.Vector3(-s * 0.5, -s * 0.5, 0));
    vs[vs.length-1].applyMatrix4(tempMat4);
    vs.push(new THREE.Vector3(-s * 0.5, s * 0.5, 0));
    vs[vs.length-1].applyMatrix4(tempMat4);
    vs.push(new THREE.Vector3(s * 0.5, -s * 0.5, 0));
    vs[vs.length-1].applyMatrix4(tempMat4);
    vs.push(new THREE.Vector3(s * 0.5, s * 0.5, 0));
    vs[vs.length-1].applyMatrix4(tempMat4);
    
    if (false && faceData.invertFlag) {
      faceData.invertFlag = 0;
      tempNormal.negate();
      
      fs.push(new THREE.Face3(vn+0, vn+1, vn+0, tempNormal));
      fs.push(new THREE.Face3(vn+1, vn+3, vn+2, tempNormal));  
    } else {
      fs.push(new THREE.Face3(vn+0, vn+1, vn+2, tempNormal));
      fs.push(new THREE.Face3(vn+1, vn+3, vn+2, tempNormal));
    }
    
    fs[fs.length-1]._myDir = direction;
    fs[fs.length-2]._myDir = direction;
   
    if (faceData.movecam) {
      fs[fs.length-1]._movecam = 1;
      fs[fs.length-2]._movecam = 1;
    }    
  }
  
  GFX.createGeometryForPath = function(path, s) {
    var list = path.split(' ');
    
    var geo = new THREE.Geometry();
    var vs = geo.vertices;
    var fs = geo.faces;
    var center = [0, 0, 0];
    var normal = [0, 1, 0];
    var direction = [0, 0, -1];

    var history = [];
    var faceData = {
      movecam: 0
    } 
    
    if (list.length && list[0] == "<cam>") {
      faceData.movecam = 1;
    }
    
    var commented = false;
    
    
    GFX.addTileToGeometry(geo, center, normal, s, direction, faceData);

    for (var t = 0; t < list.length; t++) {
      var d = list[t];
      
      if (d[0] == '{') {
        commented = true;
        continue;
      }
      
      if (d[0] == '}') {
        commented = false;
        continue;
      }
      
      if (commented) continue;
      
      if (d[0] == '_') continue;
      
      var skipFlag = 0;
      var invertFlag = 0;
      
      if (d[0] == 'z') {
        skipFlag = 1;
        d = d.substr(1);
      }
      
      if (d[0] == 'n') {
        faceData.invertFlag = 1;
        d = d.substr(1);
      }
      
      
      if (d == '<cam>') {
        faceData.movecam = 1;
        continue;
      }
      
      if (d == '</cam>') {
        faceData.movecam = 0;
        continue;
      }
      
      if (d[0] == "x") {
        var last = history.pop();
        direction = last.direction;
        center = last.center;
        normal = last.normal;
        continue;
      } else {
        history.push({center: center, normal: normal, direction: direction});
      }
      
      if (d[0] != 'f' && d[0] != 'r' && d[0] != 'l' && d[0] != 'b') {
        continue;
      }
      
      if (d[1] == 's') {
        
        if (d[0] == 'f') {
          
        }else if (d[0] == 'l') {
          direction = Vec.toAxis(Vec.perpendicular(normal, direction));
        }else if (d[0] == 'r') {
          direction = Vec.toAxis(Vec.perpendicular(direction, normal));
        }else if (d[0] == 'b') {
          direction = Vec.invert(direction);
        }
        
        center = Vec.add(Vec.setLength(direction, s), center);
        
        //Turning up
      } else {
        var rise = Vec.setLength(normal, s*0.5);
        var newDirection = _.clone(normal);
        
        if (d[1] == 'd') {
          rise = Vec.invert(rise);
          newDirection = Vec.invert(normal);
        }

        if (d[0] == 'f') {
          center = Vec.add(center, Vec.add(Vec.setLength(direction, s*0.5), rise));
          normal = Vec.invert(direction);
        }else if (d[0] == 'l') {
          normal = Vec.toAxis(Vec.perpendicular(direction, normal)); //right
          center = Vec.add(center, Vec.add(Vec.setLength(normal, -s*0.5), rise));
        }else if (d[0] == 'r') {
          normal = Vec.toAxis(Vec.perpendicular(normal, direction)); //left
          center = Vec.add(center, Vec.add(Vec.setLength(normal, -s*0.5), rise));
        }else if (d[0] == 'b') {
          center = Vec.add(center, Vec.add(Vec.setLength(direction, -s*0.5), rise));
          normal = direction;
        }
        
        if (d[1] == 'd') {
          normal = Vec.invert(normal);
        }
        
        direction = newDirection;
      
      } 
     
      if(faceData.invertFlag) {
        normal = Vec.invert(normal);
        faceData.invertFlag = 0;
      }
      
      
      if (!skipFlag) {
        GFX.addTileToGeometry(geo, center, normal, s, direction, faceData);
      }
    }
    
    geo.mergeVertices();
    
    return geo;
  }
  
  WB.Puzzle = GFX.Group.extend({
    
    loadGeometry: function(file, callback) {
      var cb = function(collada) {
        var geo = collada.scene.children[0].children[0].geometry;
        callback(geo);
      }
      GFX.colladaLoader.load(file, cb);
    },
    
    makeNotes: function(puzzleIndex) {
      if (WB.TEST_SHAPE || WB.gamePlayer.screenSaver) {
        Math.srand(5);
      } else {
        var avatar = WB.user.get('activeAvatar');
        
        if (avatar.theme && avatar.theme.musicSeed) 
          Math.srand(avatar.theme.musicSeed * 10000 + this.level.level_id * 100 + puzzleIndex);
        else
          Math.srand(this.level.level_id * 100 + puzzleIndex);
      }
      
      
      this.baseNotes = _.clone(Math.randomReal(0,1) < 0.5 ? _.clone(WB.MUSIC.MAJOR) : _.clone(WB.MUSIC.MINOR));

      var notes = _.randomShuffle(_.clone(this.baseNotes));
      //var notes = _.randomShuffle(_.clone(WB.MUSIC.PENTATONIC));
      
      //this.noteShift = 0.5 + Math.randomInt(0, 4) * 0.15;
      //this.noteShift = [0.5, 0.667, 0.75, 1.0][Math.randomInt(0, 3)];
      this.noteShift = [0.75, 1.0, 1.333, 1.5][Math.randomInt(0, 3)];
      //this.noteShift = 1;
      var fourth = _.randomShuffle(notes.slice(3, 7));
      
      notes = notes.concat(notes);
      notes.length = 12;
      notes = notes.concat(fourth);
      
      this.notes = notes;
    },
    
    inspect: function() {
      this.state = WB.PUZZLE_STATE.INSPECT;
      this.resetTileColors();
      this.inspectTime = 0;
      //this.setRotationalVelocity([Math.PI * 2 * 0.2, Math.PI * 2 * 0.2, 0]);
      WB.gamePlayer.human.setVisibility(false);
      this.inspectDistance = 5.5 * this.sideLength;
    },
    
    isValidMove: function(tile1, tile2, dir) {
      var normal = tile1.normal;
      var newNormal = tile2.normal;
      var theta = Vec.angleBetween(normal, newNormal);
      
      if (theta < 0.01) {
        return true;
      } else if (theta > Math.PI - 0.1) {
        return false;
      } else if (theta > 0.01 && theta < Math.PI - 0.1) {
        var nCross = Vec.perpendicular(normal, newNormal);
        var dCross = Vec.perpendicular( 
          Vec.sub(tile2.center, tile1.center),
          dir
        );
                
        if (Vec.angleBetween(dCross, nCross) < 0.01) {
          return false;
        } else {
          return true;
        }
      }
    },
    
    makePuzzle: function(puzzleIndex, startData) {
      this['ts'] = WB.user.get('user_id') + '+' + this.level.level_id + '+' + puzzleIndex;

      
      if (this.player)
        this.add(this.player);
      
      this.avatar = WB.user.get('activeAvatar');
      this.state = WB.PUZZLE_STATE.ENTER;
      
      puzzleIndex = puzzleIndex % this.level.puzzles.length;      
      this.puzzleIndex = puzzleIndex;
      var data = this.level.puzzles[puzzleIndex];
            
            
      var levelHistory = WB.user.get('levelHistory');
      
      
      if (WB.overlay) {
        if (levelHistory[this.level.level_id] && levelHistory[this.level.level_id].completed > this.puzzleIndex) {
          this.alreadyBeaten = 1;
          delete WB.overlay.deleteFactor('new_puzzle');
        } else {
          WB.overlay.setFactor('new_puzzle', {type: 'add', factor: 1, reason: "new puzzle"});
        }
      }
      
      this.name = data.name;
      this.reward = data.reward;
      this.makeNotes(puzzleIndex);
      
      this.playerSpeed = 1.0;
      
      //Set some defaults
      this.cameraDist = WB.CAMERA_DIST;
      this.drawWindow = "auto";
      this.cutWindow = 0;
      this.windowCutTimer = 0;
      
      //var data = shapeFunc();
      _.extend(this, data.opts);
      
      this.originalStartTile = data.opts.startTile;
      this.originalStartDir = data.opts.startDir;
      
      if (this.alreadyBeaten && startData) {
        _.extend(this, startData);
      } else if (this.alreadyBeaten && data.altStarts) {
        var altStarts = _.union(data.altStarts, {st: this.startTile, dir: this.startDir});
        var start = _.sample(altStarts);
        this.startTile = start.st;
        this.startDir = start.dir;
      }
      
      if (this.drawWindow == "always") {
        this.cutWindow = 1;
      } else if (this.drawWindow == "never") {
        this.cutWindow = 0;
      }
      

      this.cameraDir = this.startDir;
      
      var geo;
      this.sideLength = data.s;
      
      if (data.path) {
        geo = GFX.createGeometryForPath(data.path, data.s);
      } else if (data.box) {
        var box = [];
        for (var b = 0; b < data.box.length; b++) {
          if (_.isString(data.box[b]) && data.box[b][0] == "s") {
            box[b] = data.s * (Number(data.box[b].substr(1)) || 1); 
          } else {
            box[b] = data.box[b];
          }
        }
        geo = new THREE.BoxGeometry(box[0], box[1], box[2], box[3], box[4], box[5]);
      } else if (data.cubes) {
        geo = new THREE.Geometry();
        
        for (var c = 0; c < data.cubes.length; c++) {
          var co = data.cubes[c];
          GFX.tempMat4.makeTranslation(co[0], co[1], co[2]);
          geo.merge(UNIT_BOX_GEO, GFX.tempMat4, 0);
        }

        geo.mergeVertices();
        geo.computeFaceNormals();
        GFX.transformGeometry({
          geometry: geo,
          scale: data.s
        });
      }
      
      if (data.extrudes) {
        for (var e = 0; e < data.extrudes.length; e++) {
          if (data.extrudes[e] == 'j') {
            GFX.removeJoinedTiles(geo);
          } else 
            GFX.extrudeQuads(geo, data.extrudes[e], data.s);
        }
      }
      
      if (data.removes) {
        for (var r = 0; r < data.removes.length; r++) {
         
          var tile = data.removes[r];
          
          if (tile == 'c') {
            geo.faces = _.compact(geo.faces);
            continue;
          }
          
          geo.faces[tile * 2] = undefined;
          geo.faces[tile * 2 + 1] = undefined;
        }
        
        geo.faces = _.compact(geo.faces);
      }
      
      
      this.processGeometry(geo);
    },
    
    processGeometry: function(cubeGeo) {
      cubeGeo.center();
         
      
      var tiles = [];
      var tempColor = new THREE.Color();
      
      cubeGeo.faceVertexUvs = [[]];
      var moveDirs = [];
      var vs = cubeGeo.vertices;
      
      var tileCenters = {};
          
      //Testing
      var seenPairs = {};
      var fourCorners = {};
      
      var hash = function(v) {
        if (v[0] < v[1]) {
          return v[0] + "+" + v[1];
        }
        return v[1] + "+" + v[0];
      }
           
      var direction = function(center, vs) {
        var va = cubeGeo.vertices[vs[0]];
        var vb = cubeGeo.vertices[vs[1]];
        
        var axis = Vec.toAxis([
          (va.x + vb.x)*0.5-center.x, 
          (va.y + vb.y)*0.5-center.y, 
          (va.z + vb.z)*0.5-center.z
        ]);
        
        return Vec.axisString(axis);
      }
      
      
     if (WB.JOIN_TILES)
      GFX.removeJoinedTiles(cubeGeo);
 

      for (var f = 0; f < cubeGeo.faces.length; f+=2) {
        //var c = [Math.random(), Math.random(), Math.random()];
        
        var center = new THREE.Vector3();
        var facea = cubeGeo.faces[f];
        var faceb = cubeGeo.faces[f+1];
        center.add(vs[facea.b]);
        center.add(vs[facea.c]);
        center.multiplyScalar(0.5);
        var sideLength = vs[facea.a].distanceTo(vs[facea.b]);
        cubeGeo.faces[f].color.setHex(WB.TILE_STATE.coloredHex);
        cubeGeo.faces[f+1].color.setHex(WB.TILE_STATE.coloredHex);
        
        /*
        var moveDir = new THREE.Vector3(
        
        
        for (var md = 0; md < 6; md++) {
          moveDirs.push();
        }*/
        
        cubeGeo.faceVertexUvs[0][f] = [
          new THREE.Vector2(0,0),
          new THREE.Vector2(0,1),
          new THREE.Vector2(1,0)
        ];
        cubeGeo.faceVertexUvs[0][f+1] = [
          new THREE.Vector2(0,1),
          new THREE.Vector2(1,1),
          new THREE.Vector2(1,0)
        ];
        
        cubeGeo.uvsNeedUpdate = true;
        facea.originalIndex = f;
        faceb.originalIndex = f+1;
        
        tiles.push({
          facea: facea, 
          faceb: faceb,
          normal: GFX.V3toArray(facea.normal),
          activated: false,
          number: tiles.length,
          center: GFX.V3toArray(center),    
          sideLength: sideLength,
          neighbors : {},
          neighborsLists: {},
          cameraData : {}
        });
             
       //Testing
        var tile = _.last(tiles);
        
        if (tile.facea._movecam) {
          tile.cameraData.movecam = tile.facea._myDir;
        }
        
        var pairs = [[facea.a, facea.b], [facea.a, facea.c], [faceb.a, faceb.b], [faceb.b, faceb.c]];
        for (var p = 0; p < 4; p++) {
          var key = hash(pairs[p]);
          var dir = direction(center, pairs[p]);
                    
          if (seenPairs[key]) {
            var seenPair = seenPairs[key];
            var otherTile = seenPair.tile;
            
            //if (Vec.dot(otherTile.normal), tile.normal < -0.9) 
            
            if (seenPair.otherTile) {
              fourCorners[key] = fourCorners[key] || [
                {tile : seenPair.tile, dir : seenPairs[key].dir},
                {tile  : seenPair.otherTile, dir: seenPairs[key].otherDir}
              ];
                
              fourCorners[key].push({tile: tile, dir: dir});
              continue;
            }
            
            if (this.isValidMove(tile, otherTile, Vec.STRING_AXIS[dir])) {
              otherTile.neighbors[seenPairs[key].dir] = tile;
              tile.neighbors[dir] = otherTile;
            } else {
              otherTile.neighbors[seenPairs[key].dir] = 0;
              tile.neighbors[dir] = 0;
            }
            seenPairs[key].otherDir = dir;
            seenPairs[key].otherTile = tile;
          } else {
            seenPairs[key] = {tile: tile, dir: dir};
          }
        }
        
      }
      

      for (var fc in fourCorners) {
        var tileInfos = fourCorners[fc];
        
        for (var t = 0; t < tileInfos.length; t++) {
          var tileInfo = tileInfos[t];
          var maxAngle = -1000;
          var pair;
          var list = [];

          for (var t2 = 0; t2 < tileInfos.length; t2++) {
            if (t2 == t) continue;
            var otherInfo = tileInfos[t2];
            var tile = tileInfo.tile;
            var otherTile = otherInfo.tile;
            var angle = Vec.dot(tile.normal, Vec.normalize(Vec.sub(otherTile.center, tile.center)));
            
            
            list.push({tile: otherTile, angle: angle, isValid: this.isValidMove(tile, otherTile, Vec.STRING_AXIS[tileInfo.dir])});
            /*
            if (maxAngle < angle) {
              pair = t2;
              maxAngle = angle;
            } 
            */
          }
          
          list.sort(function(a, b) {
            if (a.angle < b.angle) {
                return 1;
              }
              if (a.angle > b.angle) {
                return -1;
              }
              return 0;
          });
          
          tileInfo.tile.neighborsLists[tileInfo.dir] = list;
          if (list[0] && list[0].isValid)
            tileInfo.tile.neighbors[tileInfo.dir] = list[0].tile;
          else {
            tileInfo.tile.neighbors[tileInfo.dir] = 0;
          }
        }
      }
     
      this.tiles = tiles;
      
      function AOHelper(tile, v1, v2) {
        var axis = Vec.toAxis(Vec.sub(Vec.lerp(GFX.V3toArray(vs[v1]), GFX.V3toArray(vs[v2]), 0.5), tile.center));
        var nextTile = tile.neighbors[Vec.axisString(axis)];
        if (!nextTile) return 0;
        var dist = Vec.distance(
          Vec.displace(nextTile.center, nextTile.normal, tile.sideLength * 0.5),
          Vec.displace(tile.center, tile.normal, tile.sideLength * 0.5)
        );
        
        if (dist < tile.sideLength * 0.1) {
          return 2;
        } else if (dist < tile.sideLength * 1.1) {
          return 0;
        } else {
          return 1;
        }
      }
      
      
      //Neighbors for AO
      for (var t = 0; t < this.tiles.length; t++) {
        
        var tile = this.tiles[t];
        
        var lowX = AOHelper(tile, tile.facea.a, tile.facea.b);
        var lowY = AOHelper(tile, tile.facea.a, tile.facea.c);
        var hiX  = AOHelper(tile, tile.faceb.b, tile.faceb.c);
        var hiY  = AOHelper(tile, tile.faceb.a, tile.faceb.b);
        
        tempColor.setHex(TILE_STATE.coloredHex);
        var b = tempColor.b = ( (lowX * 27) + (lowY * 9) + (hiX * 3) + hiY) / 255;  
        tile.coloredHex = tempColor.getHex();
        
        tempColor.setHex(TILE_STATE.ghostHex);
        tempColor.b = b;
        tile.ghostHex = tempColor.getHex();
        
        tempColor.setHex(TILE_STATE.leaveHex);
        tempColor.b = b;
        tile.leaveHex = tempColor.getHex();;
        
        tempColor.setHex(TILE_STATE.stepHex);
        tempColor.b = b;
        tile.stepHex = tempColor.getHex();
        
        tempColor.setHex(TILE_STATE.justBorderHex);
        tempColor.b = b;
        tile.justBorderHex = tempColor.getHex();
        
        
        tile.aoinfo = {
          lowX: lowX,
          lowY: lowY,
          hiX: hiX,
          hiY: hiY
        }
        
        tile.facea.color.setHex(tile.coloredHex);
        tile.faceb.color.setHex(tile.coloredHex);        
      }
      
      if (this.tiles.length <= this.startTile) {
        console.log('Warning: Starting tile off');
        this.startTile = this.originalStartTile;
        this.startDir = this.originalStartDir;
      } else {
        var dot = Vec.dot(this.tiles[this.startTile].normal, this.startDir);
        if (dot > 0.9 || dot < -0.9) {
          console.log('Warning: Starting tile off');
          this.startTile = this.originalStartTile;
          this.startDir = this.originalStartDir;
        }
      }
        
       
      //Make miniMe
      this.miniMe = new GFX.Object3D({
        geometry: cubeGeo,
        material: WB.MiniMeMaterial
      });
            
      if (this.alreadyBeaten) {
        WB.MiniMeMaterial.uniforms.opacity.value = 0.5;
      } else {
        WB.MiniMeMaterial.uniforms.opacity.value = 1.0;
      }
      
      this.miniMe.setScale(0.01);
      this.miniMe.setVisibility(false);
      
      if (this.cube) {
        this.remove(this.cube);
      }
      
      this.originalShader = TileMaterial;
      
      if (TileMaterial.uniforms.tex.value)
        TileMaterial.uniforms.tex.value.needsUpdate = true;
      
      this.cube = new GFX.Object3D({
        geometry: cubeGeo,
        material: TileMaterial.clone(),
      });
      
      
      
      if (this.ghostAlpha != undefined) {
        this.cube.glObject.material.uniforms.ghostAlpha.value = this.ghostAlpha;
      }
      
      this.add(this.cube);
      
      this.prepareForAvatar();


      this.activeTile = -1;
      this.activatedTiles = 0;
      this.cameraUp = this.tiles[this.startTile].normal;
      var lastTile = _.last(this.tiles);

      if (WB.TEST_SHAPE && lastTile.faceb._myDir) {
        this.arrowMarker = new GFX.Object3D({
          geometry: GFX.makePyramidGeometry(this.sideLength * 0.1, 6, this.sideLength * 0.4),
          materialColor: 0x666666
        });
        
        
        
        this.add(this.arrowMarker);
        this.arrowMarker.setPosition(lastTile.center);

        this.arrowMarker.faceDirection(lastTile.faceb._myDir, lastTile.normal);
      }
      
      this.updateTileTexture();
    },
    
    //Move an object along the puzzle (position, direction, up, animUp)
    moveAlong: function(state, elapsedTime) {
      if (this.state != WB.PUZZLE_STATE.PLAY) return;
     
      
      var dirString = Vec.axisString(state.direction);
      
      if (Vec.dot(state.direction, Vec.sub(state.position, state.tile.center)) > state.tile.sideLength * 0.5) {
        var nextTile = state.tile.neighbors[dirString];
        if (nextTile) {
          var normal = state.tile.normal;
          var newNormal = nextTile.normal;
          var theta = Vec.angleBetween(normal, newNormal);
          
          if (theta > 0.01) {
            var cross = Vec.perpendicular(normal, newNormal);
            var oldUp = state.up;
            
            state.up = newNormal;//Vec.normalize(GFX.rotateV3AxisAngle(this.playerIn, cross, theta));
            state.direction = Vec.toAxis(GFX.rotateV3AxisAngle(state.direction, cross, theta));
          }
          state.tile = nextTile;
          
        } else {
          
          state.direction = Vec.invert(state.direction);
          /*
          state.invertFlag = !!!state.invertFlag;
          if (state.invertFlag)
            state.up = Vec.invert(state.tile.normal);
          else
            state.up = state.tile.normal;
          
          state.direction = Vec.invert(state.direction);
          */
        }
      }
            
      state.animUp = GFX.slerpVec(state.animUp, state.up, 0, Math.clamp(elapsedTime * 4.0,0.0,1.0));
      var slowDown = Vec.angleBetween(state.up, state.animUp);
      if (slowDown > 0.01) {
        return;
      }
      state.position = Vec.displace(state.position, state.direction, state.speed * elapsedTime);
    },
    
    prepareForAvatar: function() {
      this.avatarData = {};

      var avatar = WB.user.get('activeAvatar');
      WB.themes.applyAvatar.call(this);
      
    },
    
    hide: function() {
      this.setVisibility(false, {remember: true});
    },
    
    show: function() {
      this.setVisibility(true, {fromMemory: true});
      for (var t = 0; t < this.tiles.length; t++) {
        if (this.tiles[t].scenery) {
          this.tiles[t].scenery.setVisibility(!this.tiles[t].activated);
        }
      }
    },
    
    cleanUpForAvatar: function() {
      WB.themes.cleanUpHazards.call(this);
    },
    
    sortTiles: function() {
      /*
      if (WB.EDITOR) {
        return;
      }*/
      
      var camLocal = this.getLocalPosition(WB.gamePlayer.camera.getPosition());
      
      if (this.sceneryGroup) {
        this.sceneryGroup.sortChildren(camLocal);
      } else {
        
        this.count = (this.count || -1)+1;
        if (this.count % 20 != 0) {
          return;
        }
        
        
        var ts = this.tiles;
        var geo = this.cube.glObject.geometry;
        
        if (!geo._bufferGeometry) return;
          
        var tn = ts.length;
        //var camPos = WB.gamePlayer.camera.getPosition();
        this.sortIndices = this.sortIndices || _.range(0, tn);
        this.swapFaces = this.swapFaces || _.range(0, geo.faces.length);
        
        for (var t = 0; t < tn; t++) {
          ts[t].camDist = Vec.distancesq(ts[t].center, camLocal);
        }
        
        this.sortIndices.sort(function(a, b) {
          if (ts[a].camDist < ts[b].camDist) {
            return 1;
          }
          if (ts[a].camDist > ts[b].camDist) {
            return -1;
          }
          return 0;
        });
        
        var flag = false;
        for (var t = 0; t < tn; t++) {
          if (this.sortIndices[t] != geo.faces[t*2].originalIndex / 2) {
            flag = true;
          }
        }
        
        
        if (!flag) {
          return;
        }
        
        for (var t = 0; t < tn; t++) {     
          geo.faces[t*2] = this.tiles[this.sortIndices[t]].facea;
          geo.faces[t*2+1] = this.tiles[this.sortIndices[t]].faceb;
        }
        
        GFX.remakeGeometry(geo);
      }
    },
  
    updateForPlayer: function(elapsedTime, player, forwardDir, rightDir, playerDir) {
      
      if(this.fadingSceneries) {
        var removeFlag = 0;
        for (var s = 0; s < this.fadingSceneries.length; s++) {
          this.fadingSceneries[s].glObject.material.uniforms.opacity.value -= elapsedTime * 7.5;
          
          if (this.fadingSceneries[s].glObject.material.uniforms.opacity.value < 0.0) {
            this.fadingSceneries[s].setVisibility(false);
            this.fadingSceneries[s] = undefined;
            removeFlag = 1;
          }
        }
        
        if(removeFlag) {
          this.fadingSceneries = _.compact(this.fadingSceneries);
        }
      }
      
      var pos = Vec.scale(player.getPosition(), this.getScale());
      var avatar = WB.user.get('activeAvatar');
      if (this.explodeAmount > 0) {
        pos = Vec.add(pos, Vec.setLength(this.tiles[this.startTile].normal, 8 * this.explodeAmount));
      }

      if (this.tileSceneries && this.tileSceneries[0]) {
        this.tileSceneries[0].material.uniforms.playerPos.value.fromArray(pos);
        this.tileSceneries[0].material.uniforms.state.value = this.state;
      }
      
      var tileMaterialUnis = this.cube.glObject.material.uniforms;
      
      tileMaterialUnis.playerPos.value.fromArray(pos);
      if(!forwardDir) {
        forwardDir = [0,0,-1];
      }
      
      tileMaterialUnis.scale.value = this.getScale();
      tileMaterialUnis.playerForward.value.fromArray(forwardDir || [0,0,-1]);
      tileMaterialUnis.playerDir.value.fromArray(playerDir || [0, 0, -1]);
      tileMaterialUnis.playerRight.value.fromArray(rightDir || [1, 0, 0]);      
      tileMaterialUnis.aliasScale.value = Math.max(1.0, (WB.gamePlayer.scene.canvasScale || 1.0));
      tileMaterialUnis.state.value = this.state;
      
      
      WB.LIGHT_VEC.set(0.1, 1.0, 0.1, 0.0);
      WB.LIGHT_VEC.applyMatrix4(WB.gamePlayer.scene._camera.matrixWorldInverse);
      tileMaterialUnis.eyeLight.value.set(WB.LIGHT_VEC.x, WB.LIGHT_VEC.y, WB.LIGHT_VEC.z);
      tileMaterialUnis.eyeLight.value.normalize();
      
      WB.PLAYER_VEC.set(pos[0], pos[1], pos[2], 1.0);
      WB.PLAYER_VEC.applyMatrix4(WB.gamePlayer.scene._camera.matrixWorldInverse);
      tileMaterialUnis.eyePlayer.value.set(WB.PLAYER_VEC.x, WB.PLAYER_VEC.y, WB.PLAYER_VEC.z); 
     
      if (this.sceneryMaterial) {
        this.sceneryMaterial.uniforms.eyeLight.value.copy(tileMaterialUnis.eyeLight.value);
        this.sceneryMaterial.uniforms.eyePlayer.value.copy(tileMaterialUnis.eyePlayer.value);
      }
      
      var windo = WB.state.get('window');
      var cameraDist = this.cameraDist;
      if (windo && windo.ar < 1.0) {
        cameraDist /= windo.ar;
      }
      
      tileMaterialUnis.cameraDist.value = cameraDist;
      
      if (this.state == WB.PUZZLE_STATE.INSPECT) {
        tileMaterialUnis.cameraDist.value = this.inspectDistance;
      }
      
      WB.gamePlayer.human.material.uniforms.aliasScale.value = WB.gamePlayer.scene.canvasScale || 1.0;
      this.cube.glObject.material.uniforms.time.value += elapsedTime;
      WB.themes.updateHazards.call(this, elapsedTime, player, avatar);
    },
    
    playFailSound: function() {
      WB.audioManager.playSynth({
        volume : 8.0 * WB.VOLUME, 
        frequency: this.baseNotes[3] * this.noteShift,
      });
      
      WB.audioManager.playSynth({
        volume : 8.0 * WB.VOLUME, 
        frequency: this.baseNotes[2] * this.noteShift,
        delay: 0.2,
      });
      
      WB.audioManager.playSynth({
        volume : 8.0 * WB.VOLUME, 
        frequency: this.baseNotes[0] * this.noteShift,
        delay: 0.3,
      });
    },
    
    fail : function() {
      this.state = WB.PUZZLE_STATE.FAIL;
      this.cleanUpForAvatar();
    },
    
    die: function() {
      //console.log('fail');
      
      /*
      if (this.failed) return;
      this.failed = true;
      this.failTime = 0;
      audioManager.playSound('error');
      
      for (var t = 0; t < this.tiles.length; t++) {
        if (!this.tiles[t].activated)
          this.changeTileColor(this.tiles[t], WB.TILE_STATE.failHex);
      }*/      
      
      if (WB.gamePlayer.activeLevelData) {
        WB.gamePlayer.activeLevelData.retries++;
        WB.overlay.savePuzzleFailed(this);
      }
      
      if (this.state == WB.PUZZLE_STATE.FAIL || this.state == WB.PUZZLE_STATE.DIE) return;
      this.dyingTime = 0;
      this.state = WB.PUZZLE_STATE.DIE;
 
      this.playFailSound();
      this.colorForFail();
    },
    
    colorForFail: function() {
      for (var t = 0; t < this.tiles.length; t++) {
        //if (!this.tiles[t].activated)
          //this.changeTileColor(this.tiles[t], WB.TILE_STATE.failHex);
      }      
    },
    
    pass: function() {
      //audioManager.playSound('pass');
      
      WB.audioManager.playSynth({
        volume : 8.0 * WB.VOLUME, 
        frequency: this.baseNotes[0] * this.noteShift,
      });
      
      WB.audioManager.playSynth({
        volume : 8.0 * WB.VOLUME, 
        frequency: this.baseNotes[2] * this.noteShift,
        delay: 0.2,
      });
      
      WB.audioManager.playSynth({
        volume : 8.0 * WB.VOLUME, 
        frequency: this.baseNotes[4] * this.noteShift,
        delay: 0.3,
      });
      
      
      if (this.puzzleIndex == this.level.puzzles.length-1) {
        var delay = 0.6;
        WB.audioManager.playSynth({
          volume : 8.0 * WB.VOLUME, 
          frequency: this.baseNotes[0] * this.noteShift,
          delay: delay
        });
        
        WB.audioManager.playSynth({
          volume : 8.0 * WB.VOLUME, 
          frequency: this.baseNotes[2] * this.noteShift,
          delay: delay + 0.2,
        });
        
        WB.audioManager.playSynth({
          volume : 8.0 * WB.VOLUME, 
          frequency: this.baseNotes[7] * this.noteShift,
          delay: delay + 0.3,
        });
      }
           
      
      this.state = WB.PUZZLE_STATE.PASS;
    },
    
    playVictory: function(tile) {
      this.state = WB.PUZZLE_STATE.VICTORY;
      this.completed = true;
      this.victoryTime = 0;
      this.victoryTile = tile;
      var thiz = this;
      
      for (var t = 0; t < this.tiles.length; t++) {
        this.changeTileColor(this.tiles[t], this.tiles[t].ghostHex);
      }
      
      WB.audioManager.playSynth({
        volume : 10.0 * WB.VOLUME, 
        frequency: this.baseNotes[6] * this.noteShift,
        prepare: function(synth, start) {
          synth.duration = 3.5;
          synth.osc.frequency.setTargetAtTime(thiz.baseNotes[1] * thiz.noteShift * 2, start + 1.5, 0.05);
          //synth.osc.frequency.linearRampToValueAtTime(thiz.baseNotes[1] * thiz.noteShift * 2, start + 1.5);
          //synth.osc2.frequency.linearRampToValueAtTime(thiz.baseNotes[1] * thiz.noteShift * 2, start + 1.5);
          synth.osc2.frequency.setTargetAtTime(thiz.baseNotes[1] * thiz.noteShift * 2, start + 1.5, 0.05);
          synth.humpTime = 1.5; 
        },
        delay: 1.3,
      })
      
      this.miniMeTime = 0;
      
      WB.gamePlayer.player.add(this.miniMe);
      
      this.cleanUpForAvatar();
      
      WB.gamePlayer.handleActivePuzzleVictory();
    },
    
    updateVictory: function(elapsedTime) {
      this.victoryTime += elapsedTime;
      var t = (this.victoryTime / WB.VICTORY_DUR);
      
      if (this.saveFlag && this.victoryTime > WB.VICTORY_DUR + WB.POSE_DUR) {
        this.miniMe.setVisibility(false);
        WB.gamePlayer.player.remove(this.miniMe);
        this.pass();
        return;
      }
      
      var vt = t;
      
      if (this.victoryTime > 1.3) {
        
        var factor = 1.0;
        if (this.victoryTime > 1.3 + 1.5) {
          factor *= 3.0;
        }
        
        this.miniMeTime += elapsedTime * factor;
        
        //var vt = Math.pow((this.miniMeTime / 4.5), 0.6);
        
        
        this.miniMe.setVisibility(true);
        
        var vt = this.miniMeTime / 4.5;
        this.miniMe.setEuler([Math.PI * 0.5, -vt * 3.0 * Math.PI, 0]);

        if (vt > 1.0) {
          return;
        }
        
        this.miniMe.setScale(0.006 + 0.04 * vt);
        var heightScale = 1.0 + (vt) * 0.6;
        this.miniMe.setPosition([0, 0, heightScale * WB.gamePlayer.human.height + (WB.gamePlayer.human.getPosition())[2]]);
      }
    },  
    
    update: function(elapsedTime) {
      this.sortTiles();
      
      if(this.state == WB.PUZZLE_STATE.INSPECT) {
        this.inspectTime = (this.inspectTime || 0) + elapsedTime;
        this.rotateAxisAngle(Vec.normalize([1,1,0]), Math.PI * (3/8) * this.inspectTime);
        
        if (this.inspectTime > 8 / 3) {
          WB.gamePlayer.quickSwapNextPuzzle();
        }
        
        return;  
      }
      
      if (this.state == WB.PUZZLE_STATE.VICTORY) {
        this.updateVictory(elapsedTime);
      }
      
      //Window
      if (this.windowCutTimer > 0.0) {
        this.windowCutTimer -= elapsedTime;
      } else if (!WB.gamePlayer.screenSaver && this.drawWindow == "auto" && this.state == WB.PUZZLE_STATE.PLAY) {
        var playerPos = WB.gamePlayer.player.getPosition();
        playerPos = Vec.displace(playerPos, WB.gamePlayer.playerCharacterUp, WB.gamePlayer.human.height * 0.5);
        var cameraPos = WB.gamePlayer.camera.getPosition();
        var result = this.getTile(playerPos, Vec.sub(cameraPos, playerPos), true);
        if (result) {
          this.cutWindow = 1;
          this.windowCutTimer = 1.0;
        } else {
          this.cutWindow = 0;
        }
      } 
      
      this.cube.glObject.material.uniforms.drawWindow.value = this.cutWindow;
      if(this.sceneryMaterial) this.sceneryMaterial.uniforms.drawWindow.value = this.cutWindow;
      
    },
    
    explode: function(amount) {
      this.explodeAmount = amount;
      
      if (amount > 0) {
        this.cube.glObject.material.uniforms.playerPos.value.set(0,0,0);
      }
      
      this.cube.glObject.material.uniforms.explodeAmount.value = amount;
      WB.themes.explodeHazards.call(this, amount);
      
      for (var t = 0; t < this.tiles.length; t++) {
        var tile = this.tiles[t];
        var td = tile.scenery;
        if (td) {
          td.setPosition(Vec.add(td.originalPosition, Vec.scale(tile.normal, 8.0 * amount)));
        }
      }
    },  
    
    markTileCleared: function(tile) {
       this.changeTileColor(tile, tile.leaveHex);
       tile.cleared = true;
       
       if (this.lastGhosted)
        this.changeTileColor(this.lastGhosted, this.lastGhosted.ghostHex);
       
       this.lastGhosted = tile;
       
      /*
      if (tile.scenery) 
        this.changeTileColor(tile, WB.TILE_STATE.eraseHex)
      else
        this.changeTileColor(tile, WB.TILE_STATE.ghostHex);
      */
    },
    
    //So we only show new tiles
    updateForLastPuzzle: function(tileData) {
      if (!tileData) return;
       
       if (this.tiles.length != tileData.length) return;
       
      for (var t = 0; t < this.tiles.length; t++) {
        if (!tileData[t].activated) {
          this.changeTileColor(this.tiles[t], WB.TILE_STATE.eraseHex);
          if (this.tiles[t].scenery) {
            this.tiles[t].scenery.setVisibility(false);
          }
        }
      }        
    },  
    
    resetTileColors: function() {
      for (var t = 0; t < this.tiles.length; t++) {
        if (this.tiles[t].scenery) {
          this.changeTileColor(this.tiles[t], WB.TILE_STATE.eraseHex)
          this.tiles[t].scenery.setVisibility(true);
        } else
          this.changeTileColor(this.tiles[t], this.tiles[t].coloredHex);
      }   
    },
    
    changeTileColor: function(tile, hex) {
      tile.facea.color.setHex(hex);
      tile.faceb.color.setHex(hex);
      this.cube.glObject.geometry.colorsNeedUpdate = true;
    },
    
    updateTileTexture: function(texture) {
      
      if (texture) {
        this.cube.glObject.material.uniforms.tex.value = texture;
      }
      
      if (this.cube.glObject.material.uniforms.tex.value) {
        this.cube.glObject.material.uniforms.tex.value.needsUpdate = true;
      }
    },
    
    fadeTileColor: function(tile) {      
      tile.activated = !tile.activated;
      if (tile.activated) {
        this.activatedTiles++;
        
        if (this.activatedTiles == this.tiles.length) {
          this.playVictory(tile);
        } 
        
        var eyePos = GFX.ArraytoV3(WB.gamePlayer.player.getPosition());
        WB.gamePlayer.scene._camera.worldToLocal(eyePos);
        WB.audioManager.playSynth({
          volume : 9.0 * WB.VOLUME, 
          frequency: this.notes[this.activatedTiles % this.notes.length] * this.noteShift,
          //position: GFX.V3toArray(eyePos)
        });
        
        if (tile.scenery) {
          //tile.scenery.setVisibility(false);
          this.fadingSceneries = this.fadingSceneries || [];
          tile.scenery.glObject.material = this.sceneryMaterial.clone();
          this.fadingSceneries.push(tile.scenery);
        } 
        
        WB.handleTileStepForAvatar.call(this, tile);
        
        this.changeTileColor(tile, tile.stepHex);
        
        this.cube.glObject.material.uniforms.time.value = 0.0;
        WB.gamePlayer.handleTileCleared();
        this.updateNeighborTiles(tile);
        
      } else {
        this.die();
        this.changeTileColor(tile, WB.TILE_STATE.errorHex);
        tile.activated = !tile.activated;
      }
      
      this.cube.glObject.geometry.colorsNeedUpdate = true;
    },
    
    updateNeighborTiles: function(tile) {
      
      /*
      if (this.cube.glObject.material.uniforms.ghostAlpha.value > Number.EPSILON) {
        return;
      }*/
      var updateNeighbor = function(neighbor) {
        for (var ndir in neighbor.neighbors) {
          //if (neighbor.neighbors[ndir] == tile) {
            var list = neighbor.neighborsLists[ndir] || [];
            for (var l = 0; l < list.length; l++) {
              if(!list[l].tile.activated) {
                
                if (list[l].isValid) {
                  neighbor.neighbors[ndir] = list[l].tile;
                }
                break;
              }
            }
          //}
        }
      }
      
      
      for (var dir in tile.neighbors) {
        var myList = tile.neighborsLists[dir];
        if (myList) {
          for (var ml = 0; ml < myList.length; ml++) {
            updateNeighbor(myList[ml].tile);
          }
        } else {
          updateNeighbor(tile.neighbors[dir]);
        }
        
      }
    },
    
    updateTrackVecs: function(tile) {
      if (tile.cameraData.movecam) {
        this.cameraDir = WB.gamePlayer.playerForward;
        this.cameraUp = tile.normal;
      }
    },
    
    getTile: function(origin, direction, bothSides) {
      GAME.GFX.tempRaycaster.ray.origin.fromArray(origin);
      GAME.GFX.tempRaycaster.ray.direction.fromArray(Vec.normalize(direction));
      GAME.GFX.tempRaycaster.near = 0.0;
      
      GAME.GFX.tempRaycaster.far = Vec.length(direction);
      
      if(bothSides)
        this.cube.glObject.material.side = THREE.DoubleSide;
      
      var intersections = GAME.GFX.tempRaycaster.intersectObject(this.cube.glObject, false);
      
      //this.cube.glObject.material.side = THREE.FrontSide;
      

      var inter = intersections[0];
      var dirdist = Vec.length(direction);
      if (inter && inter.distance <= (dirdist * 1.35) && inter.faceIndex >= 0) {
        
        
        var tile = Math.floor(this.cube.glObject.geometry.faces[inter.faceIndex].originalIndex / 2.0);
        //console.log('intersect', tile);

        return this.tiles[tile];
      } else {
        return null;
      }
    },
    
  });
  

}