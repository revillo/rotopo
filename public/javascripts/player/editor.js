var Editor = function(WB) {
  WB.TEST_SHAPE = 1.0;
  WB.REMOVING_DUR = 1.0;
  WB.VICTORY_DUR = 1.0;
  WB.POSE_DUR = 1.0;
  
  WB.EDITOR = 1;
  
  WB.TEST_LEVEL = {
    "name" : "testpuzzle",
    "level_id" : -1,
    "directory" : "testdir",
    
    "puzzles" : [    
      
    {"name":"snowflake","s":0.4,"box":["s","s7","s",1,7,1],
    "extrudes":[[3],[3],[3], [10], [10], [10], [8], [49], [1], [37], [5], [35], [12], [47]],
    "opts":{"startTile":19,"ghostAlpha":0,"targetScale":0.5,"startDir":[0,1,0],"cameraDist":3}},
    
   {"name":"winder","s":0.5,
  "box" : ["s", "s3", "s", 1, 3, 1],
  "extrudes": [[13], [13], [20], [20], [27], [27], [37], [37], [42], [42], [11], [11]],
  "opts":{"startTile":13, "ghostAlpha": 0.0, "targetScale":0.5,"startDir":[0,1,0],"cameraDist":2}}


    ]
  }
  
  var $textbox = $("<textarea>");
  $('body').append($textbox);
  $textbox.css({
    position: 'absolute',
    top: '10em',
    width: '20em',
    height: '20em',
    right: 0
  });
  
  $textbox.focus(function() {
    WB.overlay.togglePauseMenu(true);
    WB.state.set('focus', 'editor');  
  });
  
  
  $textbox.blur(function() {
    //WB.state.set('focus', 'editor');    
    try {
      var puzzle = JSON.parse($textbox.val());
      WB.TEST_LEVEL.puzzles[0] = puzzle;
      WB.gamePlayer.player.remove(WB.gamePlayer.activePuzzle.miniMe);
      WB.gamePlayer.activePuzzle.fail();
    } catch (error) {
      console.log(error);
      alert("Typo or error: ", error);
    }    
  });
  
  $textbox.text(JSON.stringify(WB.TEST_LEVEL.puzzles[0]));
}