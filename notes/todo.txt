#####################################################################
How to set up rotopo:

sudo apt-get upgrade
sudo apt-get install git
git clone https://revillo@bitbucket.org/revillo/rotopo.git
git checkout release

wget -qO- https://deb.nodesource.com/setup_4.x | sudo bash -
sudo apt-get install --yes nodejs

(IF NODE ISN'T FOUND) => sudo ln -s /usr/bin/nodejs /usr/bin/node
echo export NODE_ENV=production >> ~/.bash_profile
source ~/.bash_profile

cd rotopo
sudo nano config.json (enter config file stuff)
mkdir logs
iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 3003

npm install pm2 -g

PM2_KILL_TIMEOUT=4000 => /etc/environment

pm2 start bin/www -i 0 --name="rotopo"

//Update
git pull origin release
pm2 gracefulReload rotopo

#####################################################################

MYSQL:

sudo apt-get dist-upgrade
sudo apt-get upgrade
sudo apt-get install mysql-server  [--fix-missing --fix-broken]

Master
sudo nano /etc/mysql/my.cnf

under [mysqld] add:
log-bin=mysql-bin
server-id=1

mysql -uroot -p
>UNLOCK TABLES;

Slave
sudo nano /etc/mysql/my.cnf
under [mysqld] add:
server-id=1





TODO:

Move victory cam on frozen avatar data

Consider case where user adds level + level pass to cart
turn on 2 factor auth for main gmail
Fade out sounds on pause
Reggie - That thing // Make sure multiplier complies with backend specs
No need to update mysql level history if puzzle already completed

//Don't crash when user hits "go back" while waiting from server response in shop

//Add date/time and braintree transaction id to purchase table
//Investigate graceful shutdown
//Use rotopo.messager gmail account for errors 
//Bugs - Minime stays on character on level switch 
//Store last puzzle played in cookie - resume where you left off 
//  - can be added on to last save api call / new level 
//First time beat puzzle - Add'l reward? Change minime color? 
//Wait for response to come back before changing to next puzzle?
//flicker on last frame of remove? Backsides too :(
//Disable double tap zoom on mobile
//Maybe fade tile edges a little?
//Elinimate frame at start of removing
//Hitboxes for bullet detection (and maybe urchin)
//figure out scenery transparency
//Mess with minimus theme



//Mess with ambient occlusion filter?

Change server error handling on front end before submitting to production. (Use alerter)
Handle braintree not loaded when trying to checkout - load again?


Change menu text after trial-then-login

Throttle repeated requests from same user_id?

Restart services on server reboot

Protect against invalid requests
  Logging:
    Incorrect FacebookData (t)
  Saving:
    Invalid multipliers (t)
    Invalid level/puzzle index (t) 
  Purchasing:
    Invalid purchase
      Already purchased (t)

  
Analytics
  
Run Node.js in production - don't display errors
Test everything like a maniac


BKNFR

Raise limit on rss feed post count

